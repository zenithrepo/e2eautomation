package com.zenithss.backend.beans;

import java.util.ArrayList;
import java.util.List;

public class SuiteReport {

	TestRecord testRecord = new TestRecord();	
	List<TestRecordData> suiteRecordHistoryList = new ArrayList<TestRecordData>();
	ArrayList<SuiteStatistics> statusStatisticsList = new ArrayList<SuiteStatistics>();
	
	public List<TestRecordData> getSuiteRecordHistoryList() {
		return suiteRecordHistoryList;
	}
	public void setSuiteRecordHistoryList(List<TestRecordData> suiteRecordHistoryList) {
		this.suiteRecordHistoryList = suiteRecordHistoryList;
	}	
	public ArrayList<SuiteStatistics> getStatusStatisticsList() {
		return statusStatisticsList;
	}
	public void setStatusStatisticsList(ArrayList<SuiteStatistics> statusStatisticsList) {
		this.statusStatisticsList = statusStatisticsList;
	}
	public TestRecord getTestRecord() {
		return testRecord;
	}
	public void setTestRecord(TestRecord testRecord) {
		this.testRecord = testRecord;
	}

}
