package com.zenith.automation.batch;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.saxpath.Operator;

import com.jcraft.jsch.Channel;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.zenith.automation.framework.Constants;
import com.zenith.automation.framework.Operation;
import com.zenith.automation.utility.PropLoader;
import com.zenith.automation.utility.ScpUtility;
 
 
public class BatchCollection {
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	
	
	public boolean copyFilesToLocal(String hostname, String absoluteFilePath, String username, String password, String tmpDir) {
		boolean filesCopied = false;

		ScpUtility scpUtil = new ScpUtility();
		Session session = scpUtil.getScpSession(hostname, username, password);
		ChannelSftp channel = scpUtil.getChannel(session);
		filesCopied = scpUtil.downloadFiles(session, channel, absoluteFilePath, tmpDir);
		return filesCopied;
	}
	
	public List<String> listFilesForFolder(final File folder, String local) {
		List<String> filenames = new LinkedList<String>();
	    for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry, local);
	        } else {
	            if(fileEntry.getName().contains(".csv"))
	                filenames.add(local+fileEntry.getName());
	        }
	    }
	    return filenames;
	}
	
	public List<String> listAllFiles(String local){
		final File folder = new File(local);
		return listFilesForFolder(folder, local);
	}
	
	
	public List<TdrStructure> getDecodedTDR(List<String> allFiles, String sessionId){
		ArrayList<TdrStructure> llTdrs = new ArrayList<TdrStructure>();
		String line = null;
		for(String file : allFiles) {
			BufferedReader br = null;
			try  {
				br = new BufferedReader(new FileReader(file));
	            while ((line = br.readLine()) != null) {
	            	TdrStructure tdrStrct = new TdrStructure();
	            	tdrStrct.getTDR(line);
	            	if(tdrStrct.getImei().equals(sessionId)){
	            		llTdrs.add(tdrStrct);
	            	}
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }finally {
	        	if(br != null) {
	        		try {
						br.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }

		}
		return llTdrs;
	}
	
	public LinkedList<TdrStructure> sortTdr(List<String> allFiles){// Sort based on consumption date
		return null;
	}
	
	public TdrStructure getEachTDR(InputStream io, String sessionId){
		TdrStructure tdrStrct = null;
		String line = null;
		boolean isFirstRecord = true;
		try  {
			DataInputStream in = new DataInputStream(io); 
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
            while ((line = br.readLine()) != null) {
            	/*
            	if(isFirstRecord) {//First record will always be header, ignoring header
            		isFirstRecord = false;
            		continue;
            	}
            	*/
            	if(tdrStrct.getImei().equals(sessionId)){
                	tdrStrct = new TdrStructure();
                	tdrStrct.getTDR(line);
            	}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
		return tdrStrct;
	}
	
	public HashMap<String, Operation> convertAssertToObj(ArrayList<String> parameters, String sValue){
		HashMap<String, Operation> parsedObj = new HashMap<String, Operation>();
		for(String parameter : parameters) {
			//Supported parameters ChargeAmt==90,ChargeAmtTot>=23,ChargeAmtBal<85,ChargeAmt=-90,ChargeAmt=+90
			Operation operation = new Operation();
			String operator = getOperator(parameter);
			operation.setOperator(getOperatorType(operator));
			int operatorLen = operator.length();
			String key = parameter.substring(0, parameter.indexOf(operator));
			String value = parameter.substring(parameter.indexOf(operator)+operatorLen, parameter.length());
			operation.setKey(key);
			operation.setValue(value);
			parsedObj.put(key, operation);
		}
		return parsedObj;
	}
	
	
	
	public int getOperatorType(String operator) {
		int oprType = Constants.OPR_EQUAL;// defualt operator will be considered as equal.
		if(operator.equals("=") || operator.equals("==")) {
			oprType = Constants.OPR_EQUAL;
		}else if(operator.equals("!=") || operator.equals("=!")) {
			oprType = Constants.OPR_NOT_EQUAL;
		}else if(operator.equals("<")) {
			oprType = Constants.OPR_LESS_THAN;
		}else if(operator.equals("<=") || operator.equals("=<")) {
			oprType = Constants.OPR_LT_EQUAL;
		}else if(operator.equals(">") ) {
			oprType = Constants.OPR_GREATER_THAN;
		}else if(operator.equals(">=") || operator.equals("=>")) {
			oprType = Constants.OPR_GT_EQUAL;
		}else if(operator.equals("=+") ) {
			oprType = Constants.OPR_SUM_DIFF;
		}else if(operator.equals("=-") ) {
			oprType = Constants.OPR_SUB_DIFF;
		}else {
			System.out.println("getOperatorType() : unhandled operator:"+operator);
		}
		return oprType;
	}
	
    public String getOperator(String expression) {
    	String operator = "";
        if(expression.indexOf("=") > -1) {//Operator contains -1
        	int idx = expression.indexOf("=");
        	operator = "=";
        	if(isOperator(expression.charAt(idx+1))) {//next char
        		operator = operator + expression.charAt(idx+1);
        	}
        	if(isOperator(expression.charAt(idx-1))) {//Previous char
        		operator = expression.charAt(idx-1) + operator ;
        	}
        }else if((expression.indexOf(">") > -1)) {
        	int idx = expression.indexOf(">");
        	operator = ">";
        	if(isOperator(expression.charAt(idx+1))) {//next char
        		operator = operator + expression.charAt(idx+1);
        	}
        	if(isOperator(expression.charAt(idx-1))) {//Previous char
        		operator = expression.charAt(idx-1) + operator ;
        	}
        }else if((expression.indexOf("<") > -1)){
        	int idx = expression.indexOf("<");
        	operator = "<";
        	if(isOperator(expression.charAt(idx+1))) {//next char
        		operator = operator + expression.charAt(idx+1);
        	}
        	if(isOperator(expression.charAt(idx-1))) {//Previous char
        		operator = expression.charAt(idx-1) + operator ;
        	}
        }else {
        	System.out.println("Unhandled expression.."+expression);
        }
        return operator;
    }
    
    public boolean isOperator(char opr) {
    	boolean isOpr = false;
    	if(opr == '=' || opr == '!' || opr == '+' || opr == '-' || opr == '<' || opr == '>' ) {
    		isOpr = true;
    	}
    	return isOpr;
    }
    
	public HashMap<String,String> convertAlToMap(ArrayList<String> al, String sValue){
		HashMap<String, String> hashMap = new HashMap<String, String>();
		for(String keyValue : al) {
			String key = keyValue.split("=")[0];
			String value;
			if(sValue != null) {
				value = keyValue.split("=")[1];
			}else {
				value = null;
			}
			hashMap.put(key, value);
		}
		return hashMap;
	}
	public List<EdrPpuStructure> getDecodedPpuEdr(List<String> allFiles, String ban, String subsNum){
		ArrayList<EdrPpuStructure> llPpu = new ArrayList<EdrPpuStructure>();
		String line = null;
		for(String file : allFiles) {
			try  {
				BufferedReader br = new BufferedReader(new FileReader(file));
	            while ((line = br.readLine()) != null) {
	            	EdrPpuStructure ppuStrct = new EdrPpuStructure();
	            	ppuStrct = ppuStrct.getPpuEdr(line);
	            	if(ppuStrct.getBan().equals(ban) && ppuStrct.getPhone_number().equals(subsNum)){
	            		llPpu.add(ppuStrct);
	            	}
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		return llPpu;
	}
	
	public List<EdrBlockStructure> getDecodedBlockEdr(List<String> allFiles, String ban){
		ArrayList<EdrBlockStructure> llBlock = new ArrayList<EdrBlockStructure>();
		String line = null;
		for(String file : allFiles) {
			try  {
				BufferedReader br = new BufferedReader(new FileReader(file));
	            while ((line = br.readLine()) != null) {
	            	EdrBlockStructure blockStrct = new EdrBlockStructure();
	            	blockStrct = blockStrct.getBlockEdr(line);
	            	if(blockStrct.getBan().equals(ban)){
	            		llBlock.add(blockStrct);
	            	}
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		return llBlock;
	}

	public void updateMapCounter(HashMap<String, String> accumulateMap, String key, String value) {
		BatchCollection bCol = new BatchCollection();
		if(bCol.isInteger(value)) {//Integer
			String valueFromMap = accumulateMap.get(key);
			if(valueFromMap == null || valueFromMap == "") {
				accumulateMap.put(key, value);
			}else {
				Long parseValue = Long.parseLong(valueFromMap);
				Long inValue = Long.parseLong(value);
				accumulateMap.put(key, Long.toString(parseValue + inValue));
			}
		}else if(bCol.isDouble(value)){//Double value
			String valueFromMap = accumulateMap.get(key);
			if(valueFromMap == null || valueFromMap == "") {
				accumulateMap.put(key, value);
			}else {
				Double parseValue = Double.parseDouble(valueFromMap);
				Double inValue = Double.parseDouble(value);
				accumulateMap.put(key, Double.toString(parseValue + inValue));
			}
		}else {//String
			accumulateMap.put(key, value);
		}
	}	

	public boolean isInteger( String input ) {
		  try {
		    Integer.parseInt( input );
		    return true;
		   }catch( Exception e ) {
		    return false;
		   }
	}
	public boolean isDouble(String input) {
		try {
		    Double.parseDouble( input );
		    return true;
		   }catch( Exception e ) {
		    return false;
		   }
	}

}
