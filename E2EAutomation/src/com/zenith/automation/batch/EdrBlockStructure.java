package com.zenith.automation.batch;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.zenith.automation.framework.Constants;

public class EdrBlockStructure {
	private Date consumption_date;
	private String	access_login;
	private int	brand_id;
	private String	contract_id;
	private String	contract_item_external_id;
	private String	external_sub_id;
	private String	ban;
	private String	time_zone;
	private String	edr_event;
	private int	bill_year_num;
	private int	bill_month_num;
	private int	bill_cycle;
	private String	service_type;
	private String	type;
	private String	level;
	private String	application_source;
	private String	zone;
	private String	requestor_id;
	private double	blocking_threshold;
	private String	rat;
	private String	account_level_ind;
	public Date getConsumption_date() {
		return consumption_date;
	}
	public void setConsumption_date(Date consumption_date) {
		this.consumption_date = consumption_date;
	}
	public String getAccess_login() {
		return access_login;
	}
	public void setAccess_login(String access_login) {
		this.access_login = access_login;
	}
	public int getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}
	public String getContract_id() {
		return contract_id;
	}
	public void setContract_id(String contract_id) {
		this.contract_id = contract_id;
	}
	public String getContract_item_external_id() {
		return contract_item_external_id;
	}
	public void setContract_item_external_id(String contract_item_external_id) {
		this.contract_item_external_id = contract_item_external_id;
	}
	public String getExternal_sub_id() {
		return external_sub_id;
	}
	public void setExternal_sub_id(String external_sub_id) {
		this.external_sub_id = external_sub_id;
	}
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getEdr_event() {
		return edr_event;
	}
	public void setEdr_event(String edr_event) {
		this.edr_event = edr_event;
	}
	public int getBill_year_num() {
		return bill_year_num;
	}
	public void setBill_year_num(int bill_year_num) {
		this.bill_year_num = bill_year_num;
	}
	public int getBill_month_num() {
		return bill_month_num;
	}
	public void setBill_month_num(int bill_month_num) {
		this.bill_month_num = bill_month_num;
	}
	public int getBill_cycle() {
		return bill_cycle;
	}
	public void setBill_cycle(int bill_cycle) {
		this.bill_cycle = bill_cycle;
	}
	public String getService_type() {
		return service_type;
	}
	public void setService_type(String service_type) {
		this.service_type = service_type;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getApplication_source() {
		return application_source;
	}
	public void setApplication_source(String application_source) {
		this.application_source = application_source;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getRequestor_id() {
		return requestor_id;
	}
	public void setRequestor_id(String requestor_id) {
		this.requestor_id = requestor_id;
	}
	public double getBlocking_threshold() {
		return blocking_threshold;
	}
	public void setBlocking_threshold(double blocking_threshold) {
		this.blocking_threshold = blocking_threshold;
	}
	public String getRat() {
		return rat;
	}
	public void setRat(String rat) {
		this.rat = rat;
	}
	public String getAccount_level_ind() {
		return account_level_ind;
	}
	public void setAccount_level_ind(String account_level_ind) {
		this.account_level_ind = account_level_ind;
	}
	@Override
	public String toString() {
		return "EdrBlockStructure [consumption_date=" + consumption_date + ", access_login=" + access_login
				+ ", brand_id=" + brand_id + ", contract_id=" + contract_id + ", contract_item_external_id="
				+ contract_item_external_id + ", external_sub_id=" + external_sub_id + ", ban=" + ban + ", time_zone="
				+ time_zone + ", edr_event=" + edr_event + ", bill_year_num=" + bill_year_num + ", bill_month_num="
				+ bill_month_num + ", bill_cycle=" + bill_cycle + ", service_type=" + service_type + ", type=" + type
				+ ", level=" + level + ", application_source=" + application_source + ", zone=" + zone
				+ ", requestor_id=" + requestor_id + ", blocking_threshold=" + blocking_threshold + ", rat=" + rat
				+ ", account_level_ind=" + account_level_ind + "]";
	}
	
	public EdrBlockStructure getBlockEdr(String row) {
		String sRow[] = row.split(",", -1);
		int iCtr = -1;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2018-03-21 22:17:50
		Date dConDate = null;
		EdrBlockStructure blockEdr = new EdrBlockStructure();
		String sConDt = sRow[++iCtr];
		try {
			dConDate = dateFormat.parse(sConDt);
		}catch(Exception e) {
			e.printStackTrace();
		}
		blockEdr.setConsumption_date(dConDate);
		blockEdr.setAccess_login(sRow[++iCtr]);
		blockEdr.setBrand_id((int)getNumber(sRow[++iCtr]));
		blockEdr.setContract_id(sRow[++iCtr]);
		blockEdr.setContract_item_external_id(sRow[++iCtr]);
		blockEdr.setExternal_sub_id(sRow[++iCtr]);
		blockEdr.setBan(sRow[++iCtr]);
		blockEdr.setTime_zone(sRow[++iCtr]);
		blockEdr.setEdr_event(sRow[++iCtr]);
		blockEdr.setBill_year_num((int)getNumber(sRow[++iCtr]));
		blockEdr.setBill_month_num((int)getNumber(sRow[++iCtr]));
		blockEdr.setBill_cycle((int)getNumber(sRow[++iCtr]));
		blockEdr.setService_type(sRow[++iCtr]);
		blockEdr.setType(sRow[++iCtr]);
		blockEdr.setLevel(sRow[++iCtr]);
		blockEdr.setApplication_source(sRow[++iCtr]);
		blockEdr.setZone(sRow[++iCtr]);
		blockEdr.setRequestor_id(sRow[++iCtr]);
		blockEdr.setBlocking_threshold(getDouble(sRow[++iCtr]));
		blockEdr.setRat(sRow[++iCtr]);
		blockEdr.setAccount_level_ind(sRow[++iCtr]);
		return blockEdr;
	}
	
	public HashMap<String, String> getEdrBlockInMap(){
		HashMap<String, String> blockMap = new HashMap<String, String>();
		blockMap.put(Constants.CONSUMPTION_DATE, this.getConsumption_date().toString());
		blockMap.put(Constants.BRAND_ID, ""+this.getBrand_id() );
		blockMap.put(Constants.CONTRACT_ID, this.getContract_id() );
		blockMap.put(Constants.CONTRACT_ITEM_EXTERNAL_ID, this.getContract_item_external_id() );
		blockMap.put(Constants.EXTERNAL_SUB_ID, this.getExternal_sub_id() );
		blockMap.put(Constants.BAN, this.getBan() );
		blockMap.put(Constants.TIME_ZONE, this.getTime_zone() );
		blockMap.put(Constants.EDR_EVENT, this.getEdr_event() );
		blockMap.put(Constants.BILL_YEAR_NUM, ""+this.getBill_year_num() );
		blockMap.put(Constants.BILL_MONTH_NUM, ""+this.getBill_month_num() );
		blockMap.put(Constants.BILL_CYCLE, ""+this.getBill_cycle() );
		blockMap.put(Constants.SERVICE_TYPE, this.getService_type() );
		blockMap.put(Constants.EDR_TYPE, this.getType() );
		blockMap.put(Constants.LEVEL, this.getLevel() );
		blockMap.put(Constants.APPLICATION_SOURCE, this.getApplication_source() );
		blockMap.put(Constants.ZONE, this.getZone() );
		blockMap.put(Constants.REQUESTOR_ID, this.getRequestor_id() );
		blockMap.put(Constants.BLOCKING_THRESHOLD,""+ this.getBlocking_threshold() );
		blockMap.put(Constants.RAT, ""+this.getRat() );
		blockMap.put(Constants.ACCOUNT_LEVEL_IND, this.getAccount_level_ind() );
		return blockMap;

	}
	public long getNumber(String value){
		long lResult = 0;
		if(value != null  && value.trim() != "") {
			try {
			 lResult = Long.parseLong(value);
			}catch(NumberFormatException e) {
				
			}
		}
		return lResult;
	}
	public double getDouble(String value) {
		if(value != null  && value.trim() != "") {
			return Double.parseDouble(value);
		}else {
			return 0;
		}
	}	

}
