package com.zenith.automation.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.framework.TestExecutorHandler;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;

public class EdrPpuParser {
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	String local = propLoader.getValue("TEST_RESULT");

	public TestExecutorHandler comparePpuEdr(TestExecutorHandler testHandler, String local) {
		TestAssert testAssert = (TestAssert)testHandler;
		boolean result = true;
		BatchCollection batchColl = new BatchCollection();
		ArrayList<String> alAssertType = testAssert.getAssertType();
		HashMap<String, String> expectedResult = batchColl.convertAlToMap(alAssertType, "");//map key with with values present in assert
		HashMap<String, String> accumulatedResult = batchColl.convertAlToMap(new ArrayList<String>(expectedResult.keySet()), null);//map key with null

		List<String> allFiles = batchColl.listAllFiles(local);
		ArrayList<EdrPpuStructure> allPpu = (ArrayList<EdrPpuStructure>) batchColl.getDecodedPpuEdr(allFiles,testAssert.getBan(),testAssert.getIdentifier());

		String filePath = propLoader.getValue("TEST_RESULT");
		filePath = filePath+"\\"+testHandler.getSuiteName()+"\\"+testHandler.getTestCaseName()+"\\";
		CommonUtility.verifyDirectory(filePath);

		accumulateAssertColumn(allPpu, accumulatedResult, filePath, testAssert.getBan()+"_"+testAssert.getIdentifier());
		testHandler.expectedResultMap = expectedResult;
		testHandler.actualResultMap = accumulatedResult;
		System.out.println("=======>>>>Asserted Map:"+expectedResult);
		System.out.println("=======>>>>Computed Map:"+accumulatedResult);

		return testHandler;
	}
	
	public void accumulateAssertColumn(List<EdrPpuStructure> allPpuEdrs, HashMap<String, String> accumulateMap,String filePath, String filename) {
		BatchCollection bCol = new BatchCollection();
		ExcelGenerator excelGenerator = ExcelGenerator.getExcelGenerator();//Create Generator obj
		HSSFWorkbook workbook = excelGenerator.createWorkbook();//Create excel
		HSSFSheet sheet = excelGenerator.createSheet(workbook);//Create sheet
		excelGenerator.addEdrPpuHeader(sheet);//Add Header in excel
		int iRowCtr = 0;
		ArrayList<String> alAssertColums = new ArrayList<String>(accumulateMap.keySet());
		for(EdrPpuStructure tdr : allPpuEdrs) {
			HashMap<String, String> hashMap = tdr.getEdrPpuInMap();
			excelGenerator.addEdrPpuRow(hashMap, sheet, ++iRowCtr);//Add row in sheet
			for(String column : alAssertColums) {
				bCol.updateMapCounter(accumulateMap, column, hashMap.get(column));
			}
		}
		if(allPpuEdrs.size() > 0) {
			excelGenerator.generateFile(workbook, filePath+filename+".xls");//Generate file
			System.out.println("Merged PPU EDR :"+filePath+filename+".xls");
		}else {
			System.out.println("No PPU EDR matched...");
		}

	}

}
