package com.zenith.automation.batch;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.zenith.automation.framework.Constants;

public class EdrPpuStructure {
	private Date consumption_date;
	private Date system_date;
	private int	brand_id;
	private String	ban;
	private String	phone_number;
	private String	subscriber_id;
	private String	time_zone;
	private String	edr_event;
	private int	bill_year_num;
	private int	bill_month_num;
	private int	bill_cycle;
	private String	account_type;
	private String	account_sub_type;
	private String	language;
	private String	soc_cd;
	private String	offer_id;
	private String	package_id;
	private String	offer_recurrence_type;
	private long	volume_kb;
	private double	amount;
	private double	wcc_charge_amount;
	private double	wcc_amount_before;
	private double	wcc_amount_after;
	private double	threshold_crossed;
	private String	ppu_zone;
	private String	wcc_zone;
	private String	notification_code;
	private long	charging_id;
	private String	imsi;
	private String	force;
	public Date getConsumption_date() {
		return consumption_date;
	}
	public void setConsumption_date(Date consumption_date) {
		this.consumption_date = consumption_date;
	}
	public Date getSystem_date() {
		return system_date;
	}
	public void setSystem_date(Date system_date) {
		this.system_date = system_date;
	}
	public int getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	public String getSubscriber_id() {
		return subscriber_id;
	}
	public void setSubscriber_id(String subscriber_id) {
		this.subscriber_id = subscriber_id;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public String getEdr_event() {
		return edr_event;
	}
	public void setEdr_event(String edr_event) {
		this.edr_event = edr_event;
	}
	public int getBill_year_num() {
		return bill_year_num;
	}
	public void setBill_year_num(int bill_year_num) {
		this.bill_year_num = bill_year_num;
	}
	public int getBill_month_num() {
		return bill_month_num;
	}
	public void setBill_month_num(int bill_month_num) {
		this.bill_month_num = bill_month_num;
	}
	public int getBill_cycle() {
		return bill_cycle;
	}
	public void setBill_cycle(int bill_cycle) {
		this.bill_cycle = bill_cycle;
	}
	public String getAccount_type() {
		return account_type;
	}
	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}
	public String getAccount_sub_type() {
		return account_sub_type;
	}
	public void setAccount_sub_type(String account_sub_type) {
		this.account_sub_type = account_sub_type;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getSoc_cd() {
		return soc_cd;
	}
	public void setSoc_cd(String soc_cd) {
		this.soc_cd = soc_cd;
	}
	public String getOffer_id() {
		return offer_id;
	}
	public void setOffer_id(String offer_id) {
		this.offer_id = offer_id;
	}
	public String getPackage_id() {
		return package_id;
	}
	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}
	public String getOffer_recurrence_type() {
		return offer_recurrence_type;
	}
	public void setOffer_recurrence_type(String offer_recurrence_type) {
		this.offer_recurrence_type = offer_recurrence_type;
	}
	public long getVolume_kb() {
		return volume_kb;
	}
	public void setVolume_kb(long volume_kb) {
		this.volume_kb = volume_kb;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getWcc_charge_amount() {
		return wcc_charge_amount;
	}
	public void setWcc_charge_amount(double wcc_charge_amount) {
		this.wcc_charge_amount = wcc_charge_amount;
	}
	public double getWcc_amount_before() {
		return wcc_amount_before;
	}
	public void setWcc_amount_before(double wcc_amount_before) {
		this.wcc_amount_before = wcc_amount_before;
	}
	public double getWcc_amount_after() {
		return wcc_amount_after;
	}
	public void setWcc_amount_after(double wcc_amount_after) {
		this.wcc_amount_after = wcc_amount_after;
	}
	public double getThreshold_crossed() {
		return threshold_crossed;
	}
	public void setThreshold_crossed(double threshold_crossed) {
		this.threshold_crossed = threshold_crossed;
	}
	public String getPpu_zone() {
		return ppu_zone;
	}
	public void setPpu_zone(String ppu_zone) {
		this.ppu_zone = ppu_zone;
	}
	public String getWcc_zone() {
		return wcc_zone;
	}
	public void setWcc_zone(String wcc_zone) {
		this.wcc_zone = wcc_zone;
	}
	public String getNotification_code() {
		return notification_code;
	}
	public void setNotification_code(String notification_code) {
		this.notification_code = notification_code;
	}
	public long getCharging_id() {
		return charging_id;
	}
	public void setCharging_id(long charging_id) {
		this.charging_id = charging_id;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getForce() {
		return force;
	}
	public void setForce(String force) {
		this.force = force;
	}
	@Override
	public String toString() {
		return "EdrPpuStructure [consumption_date=" + consumption_date + ", system_date=" + system_date + ", brand_id="
				+ brand_id + ", ban=" + ban + ", phone_number=" + phone_number + ", subscriber_id=" + subscriber_id
				+ ", time_zone=" + time_zone + ", edr_event=" + edr_event + ", bill_year_num=" + bill_year_num
				+ ", bill_month_num=" + bill_month_num + ", bill_cycle=" + bill_cycle + ", account_type=" + account_type
				+ ", account_sub_type=" + account_sub_type + ", language=" + language + ", soc_cd=" + soc_cd
				+ ", offer_id=" + offer_id + ", package_id=" + package_id + ", offer_recurrence_type="
				+ offer_recurrence_type + ", volume_kb=" + volume_kb + ", amount=" + amount + ", wcc_charge_amount="
				+ wcc_charge_amount + ", wcc_amount_before=" + wcc_amount_before + ", wcc_amount_after="
				+ wcc_amount_after + ", threshold_crossed=" + threshold_crossed + ", ppu_zone=" + ppu_zone
				+ ", wcc_zone=" + wcc_zone + ", notification_code=" + notification_code + ", charging_id=" + charging_id
				+ ", imsi=" + imsi + ", force=" + force + "]";
	}
	
	public EdrPpuStructure getPpuEdr(String row) {
		String sRow[] = row.split(",", -1);
		int iCtr = -1;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2018-03-21 22:17:50
		EdrPpuStructure ppuEdr = new EdrPpuStructure();
		String sConDt = sRow[++iCtr];
		String sSysDt = sRow[++iCtr];
		Date dConDate = null;
		Date dSysDt = null;
		try {
			dConDate = dateFormat.parse(sConDt);
			dSysDt = dateFormat.parse(sSysDt);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		ppuEdr.setConsumption_date(dConDate);
		ppuEdr.setSystem_date(dSysDt);
		String sBrand = sRow[++iCtr];
		ppuEdr.setBrand_id((int)getNumber(sBrand));
		ppuEdr.setBan(sRow[++iCtr]);
		ppuEdr.setPhone_number(sRow[++iCtr]);
		ppuEdr.setSubscriber_id(sRow[++iCtr]);
		ppuEdr.setTime_zone(sRow[++iCtr]);
		ppuEdr.setEdr_event(sRow[++iCtr]);
		ppuEdr.setBill_year_num((int)getNumber(sRow[++iCtr]));
		ppuEdr.setBill_month_num((int)getNumber(sRow[++iCtr]));
		ppuEdr.setBill_cycle((int)getNumber(sRow[++iCtr]));
		ppuEdr.setAccount_type(sRow[++iCtr]);
		ppuEdr.setAccount_sub_type(sRow[++iCtr]);
		ppuEdr.setLanguage(sRow[++iCtr]);
		ppuEdr.setSoc_cd(sRow[++iCtr]);
		ppuEdr.setOffer_id(sRow[++iCtr]);
		ppuEdr.setPackage_id(sRow[++iCtr]);
		ppuEdr.setOffer_recurrence_type(sRow[++iCtr]);
		ppuEdr.setVolume_kb(getNumber(sRow[++iCtr]));
		ppuEdr.setAmount(getDouble(sRow[++iCtr]));
		ppuEdr.setWcc_charge_amount(getDouble(sRow[++iCtr]));
		ppuEdr.setWcc_amount_before(getDouble(sRow[++iCtr]));
		ppuEdr.setWcc_amount_after(getDouble(sRow[++iCtr]));
		ppuEdr.setThreshold_crossed(getDouble(sRow[++iCtr]));
		ppuEdr.setPpu_zone(sRow[++iCtr]);
		ppuEdr.setWcc_zone(sRow[++iCtr]);
		ppuEdr.setNotification_code(sRow[++iCtr]);
		ppuEdr.setCharging_id(getNumber(sRow[++iCtr]));
		ppuEdr.setImsi(sRow[++iCtr]);
		ppuEdr.setForce(sRow[++iCtr]);
		return ppuEdr;
				
	}
	public HashMap<String, String> getEdrPpuInMap(){
		HashMap<String, String> ppuMap = new HashMap<String, String>();
		ppuMap.put(Constants.CONSUMPTION_DATE, this.getConsumption_date().toString());
		ppuMap.put(Constants.SYSTEM_DATE, this.getSystem_date().toString());
		ppuMap.put(Constants.BRAND_ID, ""+this.getBrand_id());
		ppuMap.put(Constants.EDR_BAN, this.getBan());
		ppuMap.put(Constants.PHONE_NUMBER, this.getPhone_number());
		ppuMap.put(Constants.SUBSCRIBER_ID, this.getSubscriber_id());
		ppuMap.put(Constants.TIME_ZONE, this.getTime_zone());
		ppuMap.put(Constants.EDR_EVENT, this.getEdr_event());
		ppuMap.put(Constants.BILL_YEAR_NUM, "" + this.getBill_year_num());
		ppuMap.put(Constants.BILL_MONTH_NUM, "" + this.getBill_month_num());
		ppuMap.put(Constants.BILL_CYCLE, "" + this.getBill_cycle());
		ppuMap.put(Constants.ACCOUNT_TYPE, this.getAccount_type());
		ppuMap.put(Constants.ACCOUNT_SUB_TYPE, this.getAccount_sub_type());
		ppuMap.put(Constants.LANGUAGE, this.getLanguage());
		ppuMap.put(Constants.SOC_CD, this.getSoc_cd());
		ppuMap.put(Constants.OFFER_ID, this.getOffer_id());
		ppuMap.put(Constants.PACKAGE_ID, this.getPackage_id());
		ppuMap.put(Constants.OFFER_RECURRENCE_TYPE, this.getOffer_recurrence_type());
		ppuMap.put(Constants.VOLUME_KB, "" + this.getVolume_kb());
		ppuMap.put(Constants.AMOUNT, ""+this.getAmount());
		ppuMap.put(Constants.WCC_CHARGE_AMOUNT,  "" + this.getWcc_charge_amount());
		ppuMap.put(Constants.WCC_AMOUNT_before,  "" + this.getWcc_amount_before());
		ppuMap.put(Constants.WCC_AMOUNT_after,  "" + this.getWcc_amount_after());
		ppuMap.put(Constants.THRESHOLD_CROSSED,  "" + this.getThreshold_crossed());
		ppuMap.put(Constants.PPU_ZONE, this.getPpu_zone());
		ppuMap.put(Constants.WCC_ZONE, this.getWcc_zone());
		ppuMap.put(Constants.NOTIFICATION_CODE, this.getNotification_code());
		ppuMap.put(Constants.CHARGING_ID,  "" + this.getCharging_id());
		ppuMap.put(Constants.IMSI, this.getImsi());
		ppuMap.put(Constants.FORCE, this.getForce());
		return ppuMap;
	}
	
	public long getNumber(String value) {
		long lResult = 0;
		if(value != null  && value.trim() != "") {
			try {
				lResult = Long.parseLong(value);
			}catch(NumberFormatException e) {
				
			}
		}
		return lResult;
	}
	public double getDouble(String value) {
		double dResult = 0;
		if(value != null  && value.trim() != "") {
			try {
				dResult = Double.parseDouble(value);
			}catch(NumberFormatException e) {
				
			}
		}else {

		}
		return dResult;
	}
}
