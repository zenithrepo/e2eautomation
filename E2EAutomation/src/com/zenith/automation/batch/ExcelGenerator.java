package com.zenith.automation.batch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.hssf.usermodel.*;

import com.zenith.automation.framework.Constants;

public class ExcelGenerator {
	private static ExcelGenerator excelGenerator = null;
	private ExcelGenerator() {

	}
	
	public static ExcelGenerator getExcelGenerator() {
		if(excelGenerator == null) {
			excelGenerator = new ExcelGenerator();
		}
		return excelGenerator;
	}
	
	public HSSFWorkbook createWorkbook() {
		return new HSSFWorkbook();
	}
	
	public HSSFSheet createSheet(HSSFWorkbook workbook) {
		HSSFSheet sheet = workbook.createSheet();
		return sheet;
	}
	
	public void addRow(HashMap<String, String> hashMapData, HSSFSheet sheet, int rowNum) {
		int iCellCtr = -1;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGING_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.GGSN_IP));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.IMEI));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.IMSI));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SGSN_IP));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.MCC_MNC));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGING_CHARACTERISTICS));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_IDENTIFIER));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCESS_POINT_NAME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TERMINATION_CAUSE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONSUMPTION_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.VOLUME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RECEIVED_UOM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCESS_LOGIN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONTRACT_ITEM_EXTERNAL_ID	));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONTRACT_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TIME_ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BRAND_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.OFFER_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PACKAGE_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.OFFER_START_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.OFFER_EXPIRY_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.UNITS_CONSUMED));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.UOM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.REMAINING_UNITS));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.GRANTED_SERVICE_UNITS));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.DEVICE_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGE_AMOUNT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LIST_CHARGE_AMOUNT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LOCAL_TIME_OF_EVENT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGING_RESERVATION_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONFIRMATION_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PACKAGE_UOM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RAT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATING_EXCEPTION_RSN_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_YEAR_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_MONTH_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_CYCLE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_PKG));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATE_PLAN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.MIN_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.MARKET));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TDR_BAN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EXTERNAL_SUB_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVING_SID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EVENT_SOURCE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EVENT_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_LEVEL));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EVENT_NAME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATING_REASON_CODE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SESSIONID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CELL_SITE_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ZONE_FILTER));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TRANSACTION_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGED_VOLUME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SOC_SEQ_NO));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ALLOWANCE_PREVIOUS_PERIOD	));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.NUMBER_OF_CYCLES));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ALLOWANCE_QUOTA_PERIOD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.INTERVAL_START_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.INTERVAL_END_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RECURRING_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_FILTER_GROUP));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SHARING_GROUP_NAME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.MONTHLY_CAP_VALUE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.MONTHLY_CHARGE_ACCUMULATION	));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ZONE_FILTER_GROUP));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATE_PER_SCALE_PERIOD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SCALE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SCALE_START_LEG));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SCALE_END_LEG));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LOCATION_INFO_PART_1));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LOCATION_INFO_PART_2));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CURRENT_THRESHOLD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SPECIAL_RATING_CONDITION_CD	));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_CUTOVER_INDICATOR));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PRODUCTION_STATUS_));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.FILE_NM_TXT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_OPTION_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SOURCE_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PMN_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LOC_CITY_NAME));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LOC_PROV_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.COUNTRY_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATING_PERIOD_MONTH));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RATING_PERIOD_YR));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SPECIAL_RATING_CONDITION_SERVICE_PKG));
	}
	
	public void addHeader(HSSFSheet sheet) {
		int iCellCtr = -1, rowNum = 0;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGING_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.GGSN_IP);
		rowData.createCell(++iCellCtr).setCellValue(Constants.IMEI);
		rowData.createCell(++iCellCtr).setCellValue(Constants.IMSI);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SGSN_IP);
		rowData.createCell(++iCellCtr).setCellValue(Constants.MCC_MNC);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGING_CHARACTERISTICS);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_IDENTIFIER);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCESS_POINT_NAME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TERMINATION_CAUSE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONSUMPTION_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.VOLUME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RECEIVED_UOM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCESS_LOGIN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONTRACT_ITEM_EXTERNAL_ID	);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONTRACT_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TIME_ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BRAND_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.OFFER_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PACKAGE_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.OFFER_START_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.OFFER_EXPIRY_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.UNITS_CONSUMED);
		rowData.createCell(++iCellCtr).setCellValue(Constants.UOM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.REMAINING_UNITS);
		rowData.createCell(++iCellCtr).setCellValue(Constants.GRANTED_SERVICE_UNITS);
		rowData.createCell(++iCellCtr).setCellValue(Constants.DEVICE_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGE_AMOUNT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LIST_CHARGE_AMOUNT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LOCAL_TIME_OF_EVENT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGING_RESERVATION_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONFIRMATION_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PACKAGE_UOM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RAT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATING_EXCEPTION_RSN_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_YEAR_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_MONTH_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_CYCLE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_PKG);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATE_PLAN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.MIN_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.MARKET);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TDR_BAN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EXTERNAL_SUB_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVING_SID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EVENT_SOURCE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EVENT_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_LEVEL);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EVENT_NAME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATING_REASON_CODE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SESSIONID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CELL_SITE_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ZONE_FILTER);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TRANSACTION_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGED_VOLUME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SOC_SEQ_NO);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ALLOWANCE_PREVIOUS_PERIOD	);
		rowData.createCell(++iCellCtr).setCellValue(Constants.NUMBER_OF_CYCLES);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ALLOWANCE_QUOTA_PERIOD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.INTERVAL_START_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.INTERVAL_END_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RECURRING_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_FILTER_GROUP);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SHARING_GROUP_NAME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.MONTHLY_CAP_VALUE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.MONTHLY_CHARGE_ACCUMULATION	);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ZONE_FILTER_GROUP);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATE_PER_SCALE_PERIOD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SCALE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SCALE_START_LEG);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SCALE_END_LEG);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LOCATION_INFO_PART_1);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LOCATION_INFO_PART_2);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CURRENT_THRESHOLD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SPECIAL_RATING_CONDITION_CD	);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_CUTOVER_INDICATOR);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PRODUCTION_STATUS_);
		rowData.createCell(++iCellCtr).setCellValue(Constants.FILE_NM_TXT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_OPTION_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SOURCE_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PMN_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LOC_CITY_NAME);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LOC_PROV_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.COUNTRY_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATING_PERIOD_MONTH);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RATING_PERIOD_YR);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SPECIAL_RATING_CONDITION_SERVICE_PKG);

	}
	
	public void addEdrPpuRow(HashMap<String, String> hashMapData, HSSFSheet sheet, int rowNum) {
		int iCellCtr = -1;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONSUMPTION_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SYSTEM_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BRAND_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EDR_BAN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PHONE_NUMBER));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SUBSCRIBER_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TIME_ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EDR_EVENT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_YEAR_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_MONTH_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_CYCLE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCOUNT_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCOUNT_SUB_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LANGUAGE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SOC_CD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.OFFER_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PACKAGE_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.OFFER_RECURRENCE_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.VOLUME_KB));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.AMOUNT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.WCC_CHARGE_AMOUNT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.WCC_AMOUNT_before));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.WCC_AMOUNT_after));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.THRESHOLD_CROSSED));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.PPU_ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.WCC_ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.NOTIFICATION_CODE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CHARGING_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.IMSI));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.FORCE));
	}

	public void addEdrPpuHeader(HSSFSheet sheet) {
		int iCellCtr = -1, rowNum = 0;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONSUMPTION_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SYSTEM_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BRAND_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EDR_BAN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PHONE_NUMBER);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SUBSCRIBER_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TIME_ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EDR_EVENT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_YEAR_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_MONTH_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_CYCLE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCOUNT_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCOUNT_SUB_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LANGUAGE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SOC_CD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.OFFER_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PACKAGE_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.OFFER_RECURRENCE_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.VOLUME_KB);
		rowData.createCell(++iCellCtr).setCellValue(Constants.AMOUNT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.WCC_CHARGE_AMOUNT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.WCC_AMOUNT_before);
		rowData.createCell(++iCellCtr).setCellValue(Constants.WCC_AMOUNT_after);
		rowData.createCell(++iCellCtr).setCellValue(Constants.THRESHOLD_CROSSED);
		rowData.createCell(++iCellCtr).setCellValue(Constants.PPU_ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.WCC_ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.NOTIFICATION_CODE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CHARGING_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.IMSI);
		rowData.createCell(++iCellCtr).setCellValue(Constants.FORCE);		
	}
	
	public void addEdrBlockRow(HashMap<String, String> hashMapData, HSSFSheet sheet, int rowNum) {
		int iCellCtr = -1;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONSUMPTION_DATE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCESS_LOGIN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BRAND_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONTRACT_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.CONTRACT_ITEM_EXTERNAL_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EXTERNAL_SUB_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BAN));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TIME_ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.EDR_EVENT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_YEAR_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_MONTH_NUM));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BILL_CYCLE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.SERVICE_TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.TYPE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.LEVEL));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.APPLICATION_SOURCE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ZONE));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.REQUESTOR_ID));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.BLOCKING_THRESHOLD));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.RAT));
		rowData.createCell(++iCellCtr).setCellValue(hashMapData.get(Constants.ACCOUNT_LEVEL_IND));
	}
	
	public void addEdrBlockHeader(HSSFSheet sheet) {
		int iCellCtr = -1, rowNum = 0;
		HSSFRow rowData = sheet.createRow(rowNum);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONSUMPTION_DATE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCESS_LOGIN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BRAND_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONTRACT_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.CONTRACT_ITEM_EXTERNAL_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EXTERNAL_SUB_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EDR_BAN);
		rowData.createCell(++iCellCtr).setCellValue(Constants.TIME_ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EDR_EVENT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_YEAR_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_MONTH_NUM);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BILL_CYCLE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.SERVICE_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.EDR_TYPE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.LEVEL);
		rowData.createCell(++iCellCtr).setCellValue(Constants.APPLICATION_SOURCE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ZONE);
		rowData.createCell(++iCellCtr).setCellValue(Constants.REQUESTOR_ID);
		rowData.createCell(++iCellCtr).setCellValue(Constants.BLOCKING_THRESHOLD);
		rowData.createCell(++iCellCtr).setCellValue(Constants.RAT);
		rowData.createCell(++iCellCtr).setCellValue(Constants.ACCOUNT_LEVEL_IND);
	}
	

	public void generateFile(HSSFWorkbook workbook,String absFilePath) {
        FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(absFilePath);
			workbook.write(fileOut);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if(fileOut != null) {
					fileOut.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
}
