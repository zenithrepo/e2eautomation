package com.zenith.automation.batch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.zenith.automation.framework.TestAssert;

public class ExcelParser {
	/*
		//Get Excepected Result
		//Get Actual Result
		//Compare both;
		public HashMap<String,Long> getAccumulateValues(TestAssert testAssert){
			ArrayList<String> alAssertType = testAssert.getAssertType();
			HashMap<String, String> expectedResult = convertAlToMap(alAssertType, "");//map key with with values present in assert
			HashMap<String, String> accumulatedResult = convertAlToMap(new ArrayList<String>(expectedResult.keySet()), null);//map key with null
			
			BatchCollection batchColl = new BatchCollection();
			List<String> allFiles = batchColl.listAllFiles();
			
			getSumOfColumn(allFiles, accumulatedResult, testAssert.getSessionId());
			System.out.println("=======>>>>Asserted Map:"+expectedResult);
			System.out.println("=======>>>>Computed Map:"+accumulatedResult);
			int validatorIdx;
			return null;
		} 
		
		
		public HSSFSheet getSheet(String filename, int sheetNumber) {
			HSSFSheet sheet = null;
			try {
				FileInputStream fileIn = new FileInputStream(filename);
				POIFSFileSystem fs = new POIFSFileSystem(fileIn);
			    HSSFWorkbook filenm = new HSSFWorkbook(fs);
			    sheet = filenm.getSheetAt(sheetNumber);
			}catch (FileNotFoundException e) {
				e.printStackTrace();
			}catch(IOException io) {
				io.printStackTrace();
			}catch(Exception e) {
				e.printStackTrace();
			}
			return sheet;
		}
		
		public Row getRow(HSSFSheet sheet, int rowNumber) {
			Row row = sheet.getRow(rowNumber);
			return row;
		}

		public int getColumnIndex(String absFilename, String columnName) {
			int columnIndex = -1;
			HSSFSheet sheet = getSheet(absFilename, 0);
			Row firstRow = getRow(sheet, 0);
		    for(Cell cell:firstRow){
		        if (cell.getStringCellValue().equals(columnName)){
		        	columnIndex = cell.getColumnIndex();
		        }
		    }
			return columnIndex;
		}
		
		//Iterate each files and get the values of columns in assert type and then store in computed map
		public void getSumOfColumn(List<String> allFiles, HashMap<String, String> accumulateMap, String sessionId) {// Validation done on IMEI
			boolean isFirstTime = true;
			HashMap<String, Integer> columnIndexMap = new HashMap<String, Integer>();
			int validatorIndex = -1;
			for(String file : allFiles) {
				if(isFirstTime) {
					loadColumnIndexes(columnIndexMap, file);
					validatorIndex = columnIndexMap.get("IMEI");
					isFirstTime = false;
				}
				HSSFSheet sheet = getSheet(file, 0);
				for(Row row : sheet) {//process each files
					Cell validationCell = row.getCell(validatorIndex);
					if(validationCell.getStringCellValue().equals(sessionId)) {
						ArrayList<String> allColumns = new ArrayList<String>(columnIndexMap.keySet());
						for(String key : allColumns) {//get values for each column
							int columnId = columnIndexMap.get(key);
							Cell dynamicCell = row.getCell(columnId);
							updateMapCounter(accumulateMap,key, dynamicCell.getStringCellValue());
						}
					}
				}
			}
		}
		
		public void updateMapCounter(HashMap<String, String> accumulateMap, String key, String value) {
			if(isInteger(value)) {//Integer
				String valueFromMap = accumulateMap.get(key);
				if(valueFromMap == null || valueFromMap == "") {
					accumulateMap.put(key, value);
				}else {
					Long parseValue = Long.parseLong(valueFromMap);
					Long inValue = Long.parseLong(value);
					accumulateMap.put(key, Long.toString(parseValue + inValue));
				}
			}else {//String
				accumulateMap.put(key, value);
			}
		}
		
		public void loadColumnIndexes(HashMap<String, Integer> hashMap, String absFilename) {
			ArrayList<String> alColumns = new ArrayList<String>(hashMap.keySet());
			for(String column : alColumns) {
				Integer index = getColumnIndex(absFilename,  column);
				hashMap.put(column, index);
			}
			hashMap.put("IMEI", getColumnIndex(absFilename,  "IMEI"));// Added default. IMEI Used when verification with Session ID
		}
	
		public HashMap<String,String> convertAlToMap(ArrayList<String> al, String sValue){
			HashMap<String, String> hashMap = new HashMap<String, String>();
			for(String keyValue : al) {
				String key = keyValue.split("=")[0];
				String value;
				if(sValue != null) {
					value = keyValue.split("=")[1];
				}else {
					value = null;
				}
				hashMap.put(key, value);
			}
			return hashMap;
		}
		
		public boolean isInteger( String input ) {
			  try {
			    Integer.parseInt( input );
			    return true;
			   }catch( Exception e ) {
			    return false;
			   }
		}
	
*/	
}
