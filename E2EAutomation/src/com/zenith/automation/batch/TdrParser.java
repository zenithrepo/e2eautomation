package com.zenith.automation.batch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.framework.TestExecutorHandler;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;

public class TdrParser {
		PropLoader propLoader = PropLoader.getPropLoaderInstance();
		String local = propLoader.getValue("TEST_RESULT");
		
		//Get Excepected Result
		//Get Actual Result
		//Compare both;
		public TestExecutorHandler getAccumulateValues(TestExecutorHandler testHandler, String local){
			TestAssert testAssert = (TestAssert)testHandler;
			BatchCollection batchColl = new BatchCollection();
			ArrayList<String> alAssertType = testAssert.getAssertType();
			HashMap<String, String> expectedResult = batchColl.convertAlToMap(alAssertType, "");//map key with with values present in assert
			HashMap<String, String> accumulatedResult = batchColl.convertAlToMap(new ArrayList<String>(expectedResult.keySet()), null);//map key with null
			
			List<String> allFiles = batchColl.listAllFiles(local);
			ArrayList<TdrStructure> allTdrs = (ArrayList<TdrStructure>) batchColl.getDecodedTDR(allFiles,testAssert.getSessionId());
			String filePath = propLoader.getValue("TEST_RESULT");
			filePath = filePath+"\\"+testHandler.getSuiteName()+"\\"+testHandler.getTestCaseName()+"\\";
			CommonUtility.verifyDirectory(filePath);
			accumulateAssertColumn(allTdrs, accumulatedResult,filePath, testAssert.getSessionId());
			testHandler.actualResultMap = accumulatedResult;
			testHandler.expectedResultMap = expectedResult;
			System.out.println("=======>>>>Asserted Map:"+expectedResult);
			System.out.println("=======>>>>Computed Map:"+accumulatedResult);
			return testHandler;
		} 
		
		

		
		//Iterate each files and get the values of columns in assert type and then store in computed map
		// Also generate a file for all the TDRs
		public void accumulateAssertColumn(List<TdrStructure> allTdrs, HashMap<String, String> accumulateMap, String filePath, String filename) {
			BatchCollection bCol = new BatchCollection();
			ExcelGenerator excelGenerator = ExcelGenerator.getExcelGenerator();//Create Generator obj
			HSSFWorkbook workbook = excelGenerator.createWorkbook();//Create excel
			HSSFSheet sheet = excelGenerator.createSheet(workbook);//Create sheet
			excelGenerator.addHeader(sheet);//Add Header in excel
			int iRowCtr = 0;
			ArrayList<String> alAssertColums = new ArrayList<String>(accumulateMap.keySet());
			for(TdrStructure tdr : allTdrs) {
				HashMap<String, String> hashMap = tdr.getTdrInMap();
				excelGenerator.addRow(hashMap, sheet, ++iRowCtr);//Add row in sheet
				for(String column : alAssertColums) {
					bCol.updateMapCounter(accumulateMap, column, hashMap.get(column));
				}
			}
			if(allTdrs.size() > 0) {
				excelGenerator.generateFile(workbook, filePath+filename+".xls");//Generate file
				System.out.println("Merged TDR :"+filePath+filename+".xls");
			}else {
				System.out.println("No TDR matched....");
			}
		}
		
	
}
