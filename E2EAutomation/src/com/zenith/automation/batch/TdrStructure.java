package com.zenith.automation.batch;

import java.util.HashMap;
import java.util.LinkedHashMap;

import com.zenith.automation.framework.Constants;

public class TdrStructure {
	long charging_id;
	String ggsn_ip;
	String imei;
	String imsi;
	String sgsn_ip;
	int mcc_mnc;
	String charging_characteristics;
	String service_identifier;
	String service_id;
	String access_point_name;
	String termination_cause;
	String consumption_date;
	long volume;
	String received_uom;
	String access_login;
	String contract_item_external_id;
	String contract_id;
	String time_zone;
	int brand_id;
	String offer_id;
	String package_id;
	String offer_start_date;
	String offer_expiry_date;
	long units_consumed;
	String uom;
	long remaining_units;
	long granted_service_units;
	String device_type;
	double charge_amount;
	double list_charge_amount;
	String local_time_of_event;
	String charging_reservation_id;
	String confirmation_date;
	String package_uom;
	long rat;
	int rating_exception_rsn_cd;
	int bill_year_num;
	int bill_month_num;
	int bill_cycle;
	String service_pkg;
	String rate_plan;
	String min_cd;
	String market;
	String ban;
	String external_sub_id;
	String serving_sid;
	String event_source;
	String event_type;
	String service;
	String service_level;
	String event_name;
	String rating_reason_code;
	String session_id;
	String cell_site_id;
	String zone_filter;
	int transaction_type;
	long charged_volume;
	int soc_seq_no;
	int allowance_previous_period;
	int number_of_cycles;
	int allowance_quota_period;
	String interval_start_date;
	String interval_end_date;
	String recurring_type;
	String service_filter_group;
	String sharing_group_name;
	long monthly_cap_value;
	long monthly_charge_accumulation;
	String zone_filter_group;
	double rate_per_scale_period;
	int scale;
	long scale_start_leg;
	long scale_end_leg;
	String location_info_part_1;
	String location_info_part_2;
	int current_threshold;
	String special_rating_condition_cd;
	String bill_cutover_indicator;
	String production_status_;
	String file_nm_txt;
	String service_option_cd;
	String source_id;
	String pmn_id;
	String loc_city_name;
	String loc_prov_cd;
	String country_cd;
	int rating_period_month;
	int rating_period_yr;
	String special_rating_condition_service_pkg;
	public long getCharging_id() {
		return charging_id;
	}
	public void setCharging_id(long charging_id) {
		this.charging_id = charging_id;
	}
	public String getGgsn_ip() {
		return ggsn_ip;
	}
	public void setGgsn_ip(String ggsn_ip) {
		this.ggsn_ip = ggsn_ip;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getImsi() {
		return imsi;
	}
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	public String getSgsn_ip() {
		return sgsn_ip;
	}
	public void setSgsn_ip(String sgsn_ip) {
		this.sgsn_ip = sgsn_ip;
	}
	public int getMcc_mnc() {
		return mcc_mnc;
	}
	public void setMcc_mnc(int mcc_mnc) {
		this.mcc_mnc = mcc_mnc;
	}
	public String getCharging_characteristics() {
		return charging_characteristics;
	}
	public void setCharging_characteristics(String charging_characteristics) {
		this.charging_characteristics = charging_characteristics;
	}
	public String getService_identifier() {
		return service_identifier;
	}
	public void setService_identifier(String service_identifier) {
		this.service_identifier = service_identifier;
	}
	public String getService_id() {
		return service_id;
	}
	public void setService_id(String service_id) {
		this.service_id = service_id;
	}
	public String getAccess_point_name() {
		return access_point_name;
	}
	public void setAccess_point_name(String access_point_name) {
		this.access_point_name = access_point_name;
	}
	public String getTermination_cause() {
		return termination_cause;
	}
	public void setTermination_cause(String termination_cause) {
		this.termination_cause = termination_cause;
	}
	public String getConsumption_date() {
		return consumption_date;
	}
	public void setConsumption_date(String consumption_date) {
		this.consumption_date = consumption_date;
	}
	public long getVolume() {
		return volume;
	}
	public void setVolume(long volume) {
		this.volume = volume;
	}
	public String getReceived_uom() {
		return received_uom;
	}
	public void setReceived_uom(String received_uom) {
		this.received_uom = received_uom;
	}
	public String getAccess_login() {
		return access_login;
	}
	public void setAccess_login(String access_login) {
		this.access_login = access_login;
	}
	public String getContract_item_external_id() {
		return contract_item_external_id;
	}
	public void setContract_item_external_id(String contract_item_external_id) {
		this.contract_item_external_id = contract_item_external_id;
	}
	public String getContract_id() {
		return contract_id;
	}
	public void setContract_id(String contract_id) {
		this.contract_id = contract_id;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	public int getBrand_id() {
		return brand_id;
	}
	public void setBrand_id(int brand_id) {
		this.brand_id = brand_id;
	}
	public String getOffer_id() {
		return offer_id;
	}
	public void setOffer_id(String offer_id) {
		this.offer_id = offer_id;
	}
	public String getPackage_id() {
		return package_id;
	}
	public void setPackage_id(String package_id) {
		this.package_id = package_id;
	}
	public String getOffer_start_date() {
		return offer_start_date;
	}
	public void setOffer_start_date(String offer_start_date) {
		this.offer_start_date = offer_start_date;
	}
	public String getOffer_expiry_date() {
		return offer_expiry_date;
	}
	public void setOffer_expiry_date(String offer_expiry_date) {
		this.offer_expiry_date = offer_expiry_date;
	}
	public long getUnits_consumed() {
		return units_consumed;
	}
	public void setUnits_consumed(long units_consumed) {
		this.units_consumed = units_consumed;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public long getRemaining_units() {
		return remaining_units;
	}
	public void setRemaining_units(long remaining_units) {
		this.remaining_units = remaining_units;
	}
	public long getGranted_service_units() {
		return granted_service_units;
	}
	public void setGranted_service_units(long granted_service_units) {
		this.granted_service_units = granted_service_units;
	}
	public String getDevice_type() {
		return device_type;
	}
	public void setDevice_type(String device_type) {
		this.device_type = device_type;
	}
	public double getCharge_amount() {
		return charge_amount;
	}
	public void setCharge_amount(double charge_amount) {
		this.charge_amount = charge_amount;
	}
	public double getList_charge_amount() {
		return list_charge_amount;
	}
	public void setList_charge_amount(double list_charge_amount) {
		this.list_charge_amount = list_charge_amount;
	}
	public String getLocal_time_of_event() {
		return local_time_of_event;
	}
	public void setLocal_time_of_event(String local_time_of_event) {
		this.local_time_of_event = local_time_of_event;
	}
	public String getCharging_reservation_id() {
		return charging_reservation_id;
	}
	public void setCharging_reservation_id(String charging_reservation_id) {
		this.charging_reservation_id = charging_reservation_id;
	}
	public String getConfirmation_date() {
		return confirmation_date;
	}
	public void setConfirmation_date(String confirmation_date) {
		this.confirmation_date = confirmation_date;
	}
	public String getPackage_uom() {
		return package_uom;
	}
	public void setPackage_uom(String package_uom) {
		this.package_uom = package_uom;
	}
	public long getRat() {
		return rat;
	}
	public void setRat(long rat) {
		this.rat = rat;
	}
	public int getRating_exception_rsn_cd() {
		return rating_exception_rsn_cd;
	}
	public void setRating_exception_rsn_cd(int rating_exception_rsn_cd) {
		this.rating_exception_rsn_cd = rating_exception_rsn_cd;
	}
	public int getBill_year_num() {
		return bill_year_num;
	}
	public void setBill_year_num(int bill_year_num) {
		this.bill_year_num = bill_year_num;
	}
	public int getBill_month_num() {
		return bill_month_num;
	}
	public void setBill_month_num(int bill_month_num) {
		this.bill_month_num = bill_month_num;
	}
	public int getBill_cycle() {
		return bill_cycle;
	}
	public void setBill_cycle(int bill_cycle) {
		this.bill_cycle = bill_cycle;
	}
	public String getService_pkg() {
		return service_pkg;
	}
	public void setService_pkg(String service_pkg) {
		this.service_pkg = service_pkg;
	}
	public String getRate_plan() {
		return rate_plan;
	}
	public void setRate_plan(String rate_plan) {
		this.rate_plan = rate_plan;
	}
	public String getMin_cd() {
		return min_cd;
	}
	public void setMin_cd(String min_cd) {
		this.min_cd = min_cd;
	}
	public String getMarket() {
		return market;
	}
	public void setMarket(String market) {
		this.market = market;
	}
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getExternal_sub_id() {
		return external_sub_id;
	}
	public void setExternal_sub_id(String external_sub_id) {
		this.external_sub_id = external_sub_id;
	}
	public String getServing_sid() {
		return serving_sid;
	}
	public void setServing_sid(String serving_sid) {
		this.serving_sid = serving_sid;
	}
	public String getEvent_source() {
		return event_source;
	}
	public void setEvent_source(String event_source) {
		this.event_source = event_source;
	}
	public String getEvent_type() {
		return event_type;
	}
	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getService_level() {
		return service_level;
	}
	public void setService_level(String service_level) {
		this.service_level = service_level;
	}
	public String getEvent_name() {
		return event_name;
	}
	public void setEvent_name(String event_name) {
		this.event_name = event_name;
	}
	public String getRating_reason_code() {
		return rating_reason_code;
	}
	public void setRating_reason_code(String rating_reason_code) {
		this.rating_reason_code = rating_reason_code;
	}
	public String getSession_id() {
		return session_id;
	}
	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	public String getCell_site_id() {
		return cell_site_id;
	}
	public void setCell_site_id(String cell_site_id) {
		this.cell_site_id = cell_site_id;
	}
	public String getZone_filter() {
		return zone_filter;
	}
	public void setZone_filter(String zone_filter) {
		this.zone_filter = zone_filter;
	}
	public int getTransaction_type() {
		return transaction_type;
	}
	public void setTransaction_type(int transaction_type) {
		this.transaction_type = transaction_type;
	}
	public long getCharged_volume() {
		return charged_volume;
	}
	public void setCharged_volume(long charged_volume) {
		this.charged_volume = charged_volume;
	}
	public int getSoc_seq_no() {
		return soc_seq_no;
	}
	public void setSoc_seq_no(int soc_seq_no) {
		this.soc_seq_no = soc_seq_no;
	}
	public int getAllowance_previous_period() {
		return allowance_previous_period;
	}
	public void setAllowance_previous_period(int allowance_previous_period) {
		this.allowance_previous_period = allowance_previous_period;
	}
	public int getNumber_of_cycles() {
		return number_of_cycles;
	}
	public void setNumber_of_cycles(int number_of_cycles) {
		this.number_of_cycles = number_of_cycles;
	}
	public int getAllowance_quota_period() {
		return allowance_quota_period;
	}
	public void setAllowance_quota_period(int allowance_quota_period) {
		this.allowance_quota_period = allowance_quota_period;
	}
	public String getInterval_start_date() {
		return interval_start_date;
	}
	public void setInterval_start_date(String interval_start_date) {
		this.interval_start_date = interval_start_date;
	}
	public String getInterval_end_date() {
		return interval_end_date;
	}
	public void setInterval_end_date(String interval_end_date) {
		this.interval_end_date = interval_end_date;
	}
	public String getRecurring_type() {
		return recurring_type;
	}
	public void setRecurring_type(String recurring_type) {
		this.recurring_type = recurring_type;
	}
	public String getService_filter_group() {
		return service_filter_group;
	}
	public void setService_filter_group(String service_filter_group) {
		this.service_filter_group = service_filter_group;
	}
	public String getSharing_group_name() {
		return sharing_group_name;
	}
	public void setSharing_group_name(String sharing_group_name) {
		this.sharing_group_name = sharing_group_name;
	}
	public long getMonthly_cap_value() {
		return monthly_cap_value;
	}
	public void setMonthly_cap_value(long monthly_cap_value) {
		this.monthly_cap_value = monthly_cap_value;
	}
	public long getMonthly_charge_accumulation() {
		return monthly_charge_accumulation;
	}
	public void setMonthly_charge_accumulation(long monthly_charge_accumulation) {
		this.monthly_charge_accumulation = monthly_charge_accumulation;
	}
	public String getZone_filter_group() {
		return zone_filter_group;
	}
	public void setZone_filter_group(String zone_filter_group) {
		this.zone_filter_group = zone_filter_group;
	}
	public double getRate_per_scale_period() {
		return rate_per_scale_period;
	}
	public void setRate_per_scale_period(double rate_per_scale_period) {
		this.rate_per_scale_period = rate_per_scale_period;
	}
	public int getScale() {
		return scale;
	}
	public void setScale(int scale) {
		this.scale = scale;
	}
	public long getScale_start_leg() {
		return scale_start_leg;
	}
	public void setScale_start_leg(long scale_start_leg) {
		this.scale_start_leg = scale_start_leg;
	}
	public long getScale_end_leg() {
		return scale_end_leg;
	}
	public void setScale_end_leg(long scale_end_leg) {
		this.scale_end_leg = scale_end_leg;
	}
	public String getLocation_info_part_1() {
		return location_info_part_1;
	}
	public void setLocation_info_part_1(String location_info_part_1) {
		this.location_info_part_1 = location_info_part_1;
	}
	public String getLocation_info_part_2() {
		return location_info_part_2;
	}
	public void setLocation_info_part_2(String location_info_part_2) {
		this.location_info_part_2 = location_info_part_2;
	}
	public int getCurrent_threshold() {
		return current_threshold;
	}
	public void setCurrent_threshold(int current_threshold) {
		this.current_threshold = current_threshold;
	}
	public String getSpecial_rating_condition_cd() {
		return special_rating_condition_cd;
	}
	public void setSpecial_rating_condition_cd(String special_rating_condition_cd) {
		this.special_rating_condition_cd = special_rating_condition_cd;
	}
	public String getBill_cutover_indicator() {
		return bill_cutover_indicator;
	}
	public void setBill_cutover_indicator(String bill_cutover_indicator) {
		this.bill_cutover_indicator = bill_cutover_indicator;
	}
	public String getProduction_status_() {
		return production_status_;
	}
	public void setProduction_status_(String production_status_) {
		this.production_status_ = production_status_;
	}
	public String getFile_nm_txt() {
		return file_nm_txt;
	}
	public void setFile_nm_txt(String file_nm_txt) {
		this.file_nm_txt = file_nm_txt;
	}
	public String getService_option_cd() {
		return service_option_cd;
	}
	public void setService_option_cd(String service_option_cd) {
		this.service_option_cd = service_option_cd;
	}
	public String getSource_id() {
		return source_id;
	}
	public void setSource_id(String source_id) {
		this.source_id = source_id;
	}
	public String getPmn_id() {
		return pmn_id;
	}
	public void setPmn_id(String pmn_id) {
		this.pmn_id = pmn_id;
	}
	public String getLoc_city_name() {
		return loc_city_name;
	}
	public void setLoc_city_name(String loc_city_name) {
		this.loc_city_name = loc_city_name;
	}
	public String getLoc_prov_cd() {
		return loc_prov_cd;
	}
	public void setLoc_prov_cd(String loc_prov_cd) {
		this.loc_prov_cd = loc_prov_cd;
	}
	public String getCountry_cd() {
		return country_cd;
	}
	public void setCountry_cd(String country_cd) {
		this.country_cd = country_cd;
	}
	public int getRating_period_month() {
		return rating_period_month;
	}
	public void setRating_period_month(int rating_period_month) {
		this.rating_period_month = rating_period_month;
	}
	public int getRating_period_yr() {
		return rating_period_yr;
	}
	public void setRating_period_yr(int rating_period_yr) {
		this.rating_period_yr = rating_period_yr;
	}
	public String getSpecial_rating_condition_service_pkg() {
		return special_rating_condition_service_pkg;
	}
	public void setSpecial_rating_condition_service_pkg(String special_rating_condition_service_pkg) {
		this.special_rating_condition_service_pkg = special_rating_condition_service_pkg;
	}
	
	public TdrStructure getTDR(String row) {
		String sRow[] = row.split(",", -1);
		String chrId = sRow[0];
		setCharging_id((chrId != null ? Long.parseLong(chrId) : Constants.DEFAULT_NUMBER));
		setGgsn_ip(sRow[1]);
		setImei(sRow[2]);
		setImsi(sRow[3]);
		setSgsn_ip(sRow[4]);
		setMcc_mnc(sRow[5] != null ? Integer.parseInt(sRow[5]) : Constants.DEFAULT_NUMBER);
		setCharging_characteristics(sRow[6]);
		setService_identifier(sRow[7]);
		setService_id(sRow[8]);
		setAccess_point_name(sRow[9]);
		setTermination_cause(sRow[10]);
		setConsumption_date(sRow[11]);
		setVolume(sRow[12] != null ? Long.parseLong(sRow[12]) : Constants.DEFAULT_NUMBER);
		setReceived_uom(sRow[13]);
		setAccess_login(sRow[14]);
		setContract_item_external_id(sRow[15]);
		setContract_id(sRow[16]);
		setTime_zone(sRow[17]);
		setBrand_id(sRow[18] != null ? Integer.parseInt(sRow[18]) : Constants.DEFAULT_NUMBER);
		setOffer_id(sRow[19]);
		setPackage_id(sRow[20]);
		setOffer_start_date(sRow[21]);
		setOffer_expiry_date(sRow[22]);
		setUnits_consumed(sRow[23] != null ? Integer.parseInt(sRow[23]) : Constants.DEFAULT_NUMBER);
		setUom(sRow[24]);
		setRemaining_units(sRow[25] != null ? Integer.parseInt(sRow[25]) : Constants.DEFAULT_NUMBER);
		setGranted_service_units(sRow[26] != null ? Integer.parseInt(sRow[26]) : Constants.DEFAULT_NUMBER);
		setDevice_type(sRow[27]);
		setCharge_amount(sRow[28] != null ? Double.parseDouble(sRow[28]) : Constants.DEFAULT_NUMBER);
		setList_charge_amount(sRow[29] != null ? Double.parseDouble(sRow[29]) : Constants.DEFAULT_NUMBER);
		setLocal_time_of_event(sRow[30]);
		setCharging_reservation_id(sRow[31]);
		setConfirmation_date(sRow[32]);
		setPackage_uom(sRow[33]);
		setRat(sRow[34] != null ? Integer.parseInt(sRow[34]) : Constants.DEFAULT_NUMBER);
		setRating_exception_rsn_cd(sRow[35] != null ? Integer.parseInt(sRow[35]) : Constants.DEFAULT_NUMBER);
		setBill_year_num(sRow[36]  != null ? Integer.parseInt(sRow[36]) : Constants.DEFAULT_NUMBER);
		setBill_month_num(sRow[37]  != null ? Integer.parseInt(sRow[37]) : Constants.DEFAULT_NUMBER);
		setBill_cycle(sRow[38]  != null ? Integer.parseInt(sRow[38]) : Constants.DEFAULT_NUMBER);
		setService_pkg(sRow[39]);
		setRate_plan(sRow[40]);
		setMin_cd(sRow[41]);
		setMarket(sRow[42]);
		setBan(sRow[43]);
		setExternal_sub_id(sRow[44]);
		setServing_sid(sRow[45]);
		setEvent_source(sRow[46]);
		setEvent_type(sRow[47]);
		setService(sRow[48]);
		setService_level(sRow[49]);
		setEvent_name(sRow[50]);
		setRating_reason_code(sRow[51]);
		setSession_id(sRow[52]);
		setCell_site_id(sRow[53]);
		setZone_filter(sRow[54]);
		setTransaction_type(sRow[55]  != null ? Integer.parseInt(sRow[55]) : Constants.DEFAULT_NUMBER);
		setCharged_volume(sRow[56]  != null ? Integer.parseInt(sRow[56]) : Constants.DEFAULT_NUMBER);
		setSoc_seq_no(sRow[57]  != null ? Integer.parseInt(sRow[57]) : Constants.DEFAULT_NUMBER);
		setAllowance_previous_period(sRow[58]  != null ? Integer.parseInt(sRow[58]) : Constants.DEFAULT_NUMBER);
		setNumber_of_cycles(sRow[59]  != null ? Integer.parseInt(sRow[59]) : Constants.DEFAULT_NUMBER);
		setAllowance_quota_period(sRow[60]  != null ? Integer.parseInt(sRow[60]) : Constants.DEFAULT_NUMBER);
		setInterval_start_date(sRow[61]);
		setInterval_end_date(sRow[62]);
		setRecurring_type(sRow[63]);
		setService_filter_group(sRow[64]);
		setSharing_group_name(sRow[65]);
		setMonthly_cap_value(sRow[66]  != null ? Integer.parseInt(sRow[66]) : Constants.DEFAULT_NUMBER);
		setMonthly_charge_accumulation(sRow[67]  != null ? Integer.parseInt(sRow[67]) : Constants.DEFAULT_NUMBER);
		setZone_filter_group(sRow[68]);
		setRate_per_scale_period(sRow[69]  != null ? Double.parseDouble(sRow[69]) : Constants.DEFAULT_NUMBER);
		setScale(sRow[70]  != null ? Integer.parseInt(sRow[70]) : Constants.DEFAULT_NUMBER);
		setScale_start_leg(sRow[71]  != null ? Long.parseLong(sRow[71]) : Constants.DEFAULT_NUMBER);
		setScale_end_leg(sRow[72]  != null ? Long.parseLong(sRow[72]) : Constants.DEFAULT_NUMBER);
		setLocation_info_part_1(sRow[73]);
		setLocation_info_part_2(sRow[74]);
		setCurrent_threshold(sRow[75]  != null ? Integer.parseInt(sRow[75]) : Constants.DEFAULT_NUMBER);
		setSpecial_rating_condition_cd(sRow[76]);
		setBill_cutover_indicator(sRow[77]);
		setProduction_status_(sRow[78]);
		setFile_nm_txt(sRow[79]);
		setService_option_cd(sRow[80]);
		setSource_id(sRow[81]);
		setPmn_id(sRow[82]);
		setLoc_city_name(sRow[83]);
		setLoc_prov_cd(sRow[84]);
		setCountry_cd(sRow[85]);
		setRating_period_month(sRow[86]  != null ? Integer.parseInt(sRow[86]) : Constants.DEFAULT_NUMBER);
		setRating_period_yr(sRow[87]  != null ? Integer.parseInt(sRow[87]) : Constants.DEFAULT_NUMBER);
		//setSpecial_rating_condition_service_pkg(sRow[88]);
		return this;
	
	}
	
	public HashMap<String, String> getTdrInMap(){
		HashMap<String, String> tdrMap = new HashMap<String, String>();
		tdrMap.put(Constants.CHARGING_ID, "" + this.getCharging_id());
		tdrMap.put(Constants.GGSN_IP, this.getGgsn_ip());
		tdrMap.put(Constants.IMEI, this.getImei());
		tdrMap.put(Constants.IMSI, this.getImsi());
		tdrMap.put(Constants.SGSN_IP, this.getSgsn_ip());  
		tdrMap.put(Constants.MCC_MNC  , ""+this.getMcc_mnc());
		tdrMap.put(Constants.CHARGING_CHARACTERISTICS, this.getCharging_characteristics());
		tdrMap.put(Constants.SERVICE_IDENTIFIER , this.getService_identifier());
		tdrMap.put(Constants.SERVICE_ID , this.getService_id());
		tdrMap.put(Constants.ACCESS_POINT_NAME  , this.getAccess_point_name());
		tdrMap.put(Constants.TERMINATION_CAUSE  , this.getTermination_cause());
		tdrMap.put(Constants.CONSUMPTION_DATE, this.getConsumption_date());
		tdrMap.put(Constants.VOLUME,""+ this.getVolume());
		tdrMap.put(Constants.RECEIVED_UOM  , this.getReceived_uom());
		tdrMap.put(Constants.ACCESS_LOGIN  , this.getAccess_login());
		tdrMap.put(Constants.CONTRACT_ITEM_EXTERNAL_ID  	, this.getContract_item_external_id());
		tdrMap.put(Constants.CONTRACT_ID, this.getContract_id());
		tdrMap.put(Constants.TIME_ZONE, this.getTime_zone());
		tdrMap.put(Constants.BRAND_ID , ""+this.getBrand_id());
		tdrMap.put(Constants.OFFER_ID , this.getOffer_id());
		tdrMap.put(Constants.PACKAGE_ID , this.getPackage_id());
		tdrMap.put(Constants.OFFER_START_DATE, this.getOffer_start_date());
		tdrMap.put(Constants.OFFER_EXPIRY_DATE  , this.getOffer_expiry_date());
		tdrMap.put(Constants.UNITS_CONSUMED, ""+this.getUnits_consumed());
		tdrMap.put(Constants.UOM , this.getUom());
		tdrMap.put(Constants.REMAINING_UNITS , ""+this.getRemaining_units());
		tdrMap.put(Constants.GRANTED_SERVICE_UNITS, ""+this.getGranted_service_units());
		tdrMap.put(Constants.DEVICE_TYPE, this.getDevice_type());
		tdrMap.put(Constants.CHARGE_AMOUNT , ""+this.getCharge_amount());
		tdrMap.put(Constants.LIST_CHARGE_AMOUNT , ""+this.getList_charge_amount());
		tdrMap.put(Constants.LOCAL_TIME_OF_EVENT, this.getLocal_time_of_event());
		tdrMap.put(Constants.CHARGING_RESERVATION_ID , this.getCharging_reservation_id());
		tdrMap.put(Constants.CONFIRMATION_DATE  , this.getConfirmation_date());
		tdrMap.put(Constants.PACKAGE_UOM, this.getPackage_uom());
		tdrMap.put(Constants.RAT , ""+this.getRat());
		tdrMap.put(Constants.RATING_EXCEPTION_RSN_CD , ""+this.getRating_exception_rsn_cd());
		tdrMap.put(Constants.BILL_YEAR_NUM , ""+this.getBill_year_num());
		tdrMap.put(Constants.BILL_MONTH_NUM, ""+this.getBill_month_num());
		tdrMap.put(Constants.BILL_CYCLE , ""+this.getBill_cycle());
		tdrMap.put(Constants.SERVICE_PKG, this.getService_pkg());
		tdrMap.put(Constants.RATE_PLAN, this.getRate_plan());
		tdrMap.put(Constants.MIN_CD, this.getMin_cd());
		tdrMap.put(Constants.MARKET, this.getMarket());
		tdrMap.put(Constants.TDR_BAN  , this.getBan());
		tdrMap.put(Constants.EXTERNAL_SUB_ID , this.getExternal_sub_id());
		tdrMap.put(Constants.SERVING_SID, this.getServing_sid());
		tdrMap.put(Constants.EVENT_SOURCE  , this.getEvent_source());
		tdrMap.put(Constants.EVENT_TYPE , this.getEvent_type());
		tdrMap.put(Constants.SERVICE  , this.getService());
		tdrMap.put(Constants.SERVICE_LEVEL , this.getService_level());
		tdrMap.put(Constants.EVENT_NAME , this.getEvent_name());
		tdrMap.put(Constants.RATING_REASON_CODE , this.getRating_reason_code());
		tdrMap.put(Constants.SESSIONID, this.getSession_id());
		tdrMap.put(Constants.CELL_SITE_ID  , this.getCell_site_id());
		tdrMap.put(Constants.ZONE_FILTER, this.getZone_filter());
		tdrMap.put(Constants.TRANSACTION_TYPE, ""+this.getTransaction_type());
		tdrMap.put(Constants.CHARGED_VOLUME, ""+this.getCharged_volume());
		tdrMap.put(Constants.SOC_SEQ_NO , ""+this.getSoc_seq_no());
		tdrMap.put(Constants.ALLOWANCE_PREVIOUS_PERIOD  	, ""+this.getAllowance_previous_period());
		tdrMap.put(Constants.NUMBER_OF_CYCLES, ""+this.getNumber_of_cycles());
		tdrMap.put(Constants.ALLOWANCE_QUOTA_PERIOD  , ""+this.getAllowance_quota_period());
		tdrMap.put(Constants.INTERVAL_START_DATE, this.getInterval_start_date());
		tdrMap.put(Constants.INTERVAL_END_DATE  , this.getInterval_end_date());
		tdrMap.put(Constants.RECURRING_TYPE, this.getRecurring_type());
		tdrMap.put(Constants.SERVICE_FILTER_GROUP , this.getService_filter_group());
		tdrMap.put(Constants.SHARING_GROUP_NAME , this.getSharing_group_name());
		tdrMap.put(Constants.MONTHLY_CAP_VALUE  , ""+this.getMonthly_cap_value());
		tdrMap.put(Constants.MONTHLY_CHARGE_ACCUMULATION	, ""+this.getMonthly_charge_accumulation());
		tdrMap.put(Constants.ZONE_FILTER_GROUP  , this.getZone_filter_group());
		tdrMap.put(Constants.RATE_PER_SCALE_PERIOD, ""+this.getRate_per_scale_period());
		tdrMap.put(Constants.SCALE , ""+this.getScale());
		tdrMap.put(Constants.SCALE_START_LEG , ""+this.getScale_start_leg());
		tdrMap.put(Constants.SCALE_END_LEG , ""+this.getScale_end_leg());
		tdrMap.put(Constants.LOCATION_INFO_PART_1 , this.getLocation_info_part_1());
		tdrMap.put(Constants.LOCATION_INFO_PART_2 , this.getLocation_info_part_2());
		tdrMap.put(Constants.CURRENT_THRESHOLD  , ""+this.getCurrent_threshold());
		tdrMap.put(Constants.SPECIAL_RATING_CONDITION_CD	, this.getSpecial_rating_condition_cd());
		tdrMap.put(Constants.BILL_CUTOVER_INDICATOR  , this.getBill_cutover_indicator());
		tdrMap.put(Constants.PRODUCTION_STATUS_ , this.getProduction_status_());
		tdrMap.put(Constants.FILE_NM_TXT, this.getFile_nm_txt());
		tdrMap.put(Constants.SERVICE_OPTION_CD  , this.getService_option_cd());
		tdrMap.put(Constants.SOURCE_ID, this.getSource_id());
		tdrMap.put(Constants.PMN_ID, this.getPmn_id());
		tdrMap.put(Constants.LOC_CITY_NAME , this.getLoc_city_name());
		tdrMap.put(Constants.LOC_PROV_CD, this.getLoc_prov_cd());
		tdrMap.put(Constants.COUNTRY_CD , this.getCountry_cd());
		tdrMap.put(Constants.RATING_PERIOD_MONTH, ""+this.getRating_period_month());
		tdrMap.put(Constants.RATING_PERIOD_YR, ""+this.getRating_period_yr());
		tdrMap.put(Constants.SPECIAL_RATING_CONDITION_SERVICE_PKG 	, this.getSpecial_rating_condition_service_pkg());
		return tdrMap;
	}
	
	@Override
	public String toString() {
		return "TDRStructure [charging_id=" + charging_id + ", ggsn_ip=" + ggsn_ip + ", imei=" + imei + ", imsi=" + imsi
				+ ", sgsn_ip=" + sgsn_ip + ", mcc_mnc=" + mcc_mnc + ", charging_characteristics="
				+ charging_characteristics + ", service_identifier=" + service_identifier + ", service_id=" + service_id
				+ ", access_point_name=" + access_point_name + ", termination_cause=" + termination_cause
				+ ", consumption_date=" + consumption_date + ", volume=" + volume + ", received_uom=" + received_uom
				+ ", access_login=" + access_login + ", contract_item_external_id=" + contract_item_external_id
				+ ", contract_id=" + contract_id + ", time_zone=" + time_zone + ", brand_id=" + brand_id + ", offer_id="
				+ offer_id + ", package_id=" + package_id + ", offer_start_date=" + offer_start_date
				+ ", offer_expiry_date=" + offer_expiry_date + ", units_consumed=" + units_consumed + ", uom=" + uom
				+ ", remaining_units=" + remaining_units + ", granted_service_units=" + granted_service_units
				+ ", device_type=" + device_type + ", charge_amount=" + charge_amount + ", list_charge_amount="
				+ list_charge_amount + ", local_time_of_event=" + local_time_of_event + ", charging_reservation_id="
				+ charging_reservation_id + ", confirmation_date=" + confirmation_date + ", package_uom=" + package_uom
				+ ", rat=" + rat + ", rating_exception_rsn_cd=" + rating_exception_rsn_cd + ", bill_year_num="
				+ bill_year_num + ", bill_month_num=" + bill_month_num + ", bill_cycle=" + bill_cycle + ", service_pkg="
				+ service_pkg + ", rate_plan=" + rate_plan + ", min_cd=" + min_cd + ", market=" + market + ", ban="
				+ ban + ", external_sub_id=" + external_sub_id + ", serving_sid=" + serving_sid + ", event_source="
				+ event_source + ", event_type=" + event_type + ", service=" + service + ", service_level="
				+ service_level + ", event_name=" + event_name + ", rating_reason_code=" + rating_reason_code
				+ ", session_id=" + session_id + ", cell_site_id=" + cell_site_id + ", zone_filter=" + zone_filter
				+ ", transaction_type=" + transaction_type + ", charged_volume=" + charged_volume + ", soc_seq_no="
				+ soc_seq_no + ", allowance_previous_period=" + allowance_previous_period + ", number_of_cycles="
				+ number_of_cycles + ", allowance_quota_period=" + allowance_quota_period + ", interval_start_date="
				+ interval_start_date + ", interval_end_date=" + interval_end_date + ", recurring_type="
				+ recurring_type + ", service_filter_group=" + service_filter_group + ", sharing_group_name="
				+ sharing_group_name + ", monthly_cap_value=" + monthly_cap_value + ", monthly_charge_accumulation="
				+ monthly_charge_accumulation + ", zone_filter_group=" + zone_filter_group + ", rate_per_scale_period="
				+ rate_per_scale_period + ", scale=" + scale + ", scale_start_leg=" + scale_start_leg
				+ ", scale_end_leg=" + scale_end_leg + ", location_info_part_1=" + location_info_part_1
				+ ", location_info_part_2=" + location_info_part_2 + ", current_threshold=" + current_threshold
				+ ", special_rating_condition_cd=" + special_rating_condition_cd + ", bill_cutover_indicator="
				+ bill_cutover_indicator + ", production_status_=" + production_status_ + ", file_nm_txt=" + file_nm_txt
				+ ", service_option_cd=" + service_option_cd + ", source_id=" + source_id + ", pmn_id=" + pmn_id
				+ ", loc_city_name=" + loc_city_name + ", loc_prov_cd=" + loc_prov_cd + ", country_cd=" + country_cd
				+ ", rating_period_month=" + rating_period_month + ", rating_period_yr=" + rating_period_yr
				+ ", special_rating_condition_service_pkg=" + special_rating_condition_service_pkg + "]";
	}

	
}
