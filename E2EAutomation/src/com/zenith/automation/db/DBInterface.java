package com.zenith.automation.db;

import java.io.BufferedReader;


import java.io.*;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.zenith.automation.batch.ExcelParser;
import com.zenith.automation.batch.TdrStructure;
import com.zenith.automation.utility.DBUtility;

public class DBInterface {
	public static void main(String args[]) throws Exception{
		
		String query = "select TNAME from TAB";
		final String MeterQuery="select "+ 
				"      SUAC.code ext_acct_code, CACI_TECH.value phone, CND.NAME_KEY, CND.name, to_char(CT.value) counter" + 
				"      from subscriber_account SUAC, CACO,CACI, counter CT ,capa_names_dict CND, CACI_USER_TECH_ID CACI_TECH" + 
				"      where CACO.SUAC_OID = SUAC.OID" + 
				"      AND CACO.EXT_ID = ?" + 
				"      AND CACO.PARTITION_ID = SUAC.PARTITION_ID" + 
				"      AND CACI.CACO_OID = CACO.OID" + 
				"      AND CACI.PARTITION_ID = CACO.PARTITION_ID" + 
				"      AND CACI_TECH.CACO_OID = CACO.OID" + 
				"      AND CACI_TECH.CACI_OID = CACI.OID " + 
				"      AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID" + 
				"      AND CACI_TECH.END_DATE IS NULL  " + 
				"      and CND.CAPA_OID = CACI.CAPA_OID" + 
				"      and CT.SUAC_OID = SUAC.OID" + 
				"      and CT.PARTITION_ID = CACI.PARTITION_ID" + 
				"      and CT.COUN_KEY = CND.NAME_KEY" + 
				"      and CT.value != -1" + 
				"      and CND.name like 'Meter%'" + 
				"      order by CT.COUN_KEY" ;
		PreparedStatement prep_statement1 = null;
		//Ban Level counters (Query)
		String statement1 = "select  SUAC.code ext_acct_code, CACI_TECH.value phone, CND.NAME_KEY, CND.name, to_char(CT.value) counter" + 
				"      from subscriber_account SUAC, CACO,CACI, counter CT ,capa_names_dict CND, CACI_USER_TECH_ID CACI_TECH" + 
				"      where CACO.SUAC_OID = SUAC.OID" + 
				//External ID - Change Here
				"      AND CACO.EXT_ID = 'S8414737'" + 
				"      AND CACO.PARTITION_ID = SUAC.PARTITION_ID" + 
				"      AND CACI.CACO_OID = CACO.OID" + 
				"      AND CACI.PARTITION_ID = CACO.PARTITION_ID" + 
				"      AND CACI_TECH.CACO_OID = CACO.OID" + 
				"      AND CACI_TECH.CACI_OID = CACI.OID " + 
				"      AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID" + 
				"      AND CACI_TECH.END_DATE IS NULL      " + 
				"      and CND.CAPA_OID = CACI.CAPA_OID" + 
				"      and CT.SUAC_OID = SUAC.OID" + 
				"      and CT.PARTITION_ID = CACI.PARTITION_ID" + 
				"      and CT.COUN_KEY = CND.NAME_KEY" + 
				"      order by CT.COUN_KEY";
		//SAPCC sub Services (Query)
		String statement2 = "SELECT SUAC.CODE,EXT_ACT.EXTERNAL_ACCOUNT_CODE BAN, CACI_TECH.VALUE SUBSCRIBER_NO, B_CYCLE.BILL_CYCLE, B_CYCLE.CYCLE_CLOSE_DATE, SUMT_MARO.OUT_S_02 SOC, SUMT_MARO.OUT_S_03 SOC_TYPE,SUMT_MARO.IN_01 EXTENAL_OFFER_SEQ_NUM, SUMT_MARO.OUT_S_01 OFFER_ID, OFFER.RECURRENCE_TYPE, OFFER_PAKG.PACKAGE_ID, PAKG.PRICE_ID,PAKG.PRICE_TYPE, PAKG.SERVICE_FILTER_GROUP, PAKG.PACKAGE_PRIORITY, PAKG.PACKAGE_NAME, SUMT_MARO.OUT_N_01 OFFER_EFFECTIVE_DATE, SUMT_MARO.OUT_N_02 OFFER_EXPIRY_DATE, OFFER.NOTIFICATION_ID, OFFER.CAPPING_ID  " + 
				"FROM SUBSCRIBER_ACCOUNT SUAC,CACO,CACI,CACI_USER_TECH_ID CACI_TECH,SUMT,SUMT_MARO,external_account EXT_ACT, " + 
				"( select TROW.in_01 OFFER_ID, TROW.out_s_01 RECURRENCE_TYPE, TROW.out_bd_02 NUMBER_OF_PACKAGES, TROW.out_s_09 OFFER_NAME, TROW.out_s_04 NOTIFICATION_ID,TROW.out_bd_05 NOTIFICATION_PRIORITY,TROW.out_s_06 CAPPING_ID, TROW.out_bd_07 CAPPING_PRIORITY,TROW.out_s_08 ALLOW_NOTIFICATION_OVERRIDE " + 
				"  from TRANSLATION_TABLE TTAB, TRANSLATION_INSTANCE TINS, TRANSLATION_ROW TROW " + 
				"  where TTAB.oid = TINS.ttab_oid " + 
				"  and TINS.oid = TROW.tins_oid " + 
				"  and TTAB.code ='PO_BAU_OFFER_DEF' " + 
				"  and TROW.in_end is null " + 
				"  and TINS.validation_date is null " + 
				") OFFER, " + 
				"( select TROW.in_01 OFFER_ID, TROW.in_02 INDEX_ID, TROW.IN_START REF_PACKAGE_START_DATE, TROW.IN_END REF_PACKAGE_END_DATE, TROW.out_s_01 PACKAGE_ID  " + 
				"  from TRANSLATION_TABLE TTAB, TRANSLATION_INSTANCE TINS, TRANSLATION_ROW TROW " + 
				"  where TTAB.oid = TINS.ttab_oid " + 
				"  and TINS.oid = TROW.tins_oid " + 
				"  and TTAB.code ='PO_BAU_OFFER_PACKAGE_DEF' " + 
				"  and TROW.in_end is null " + 
				"  and TINS.validation_date is null " + 
				") OFFER_PAKG, " + 
				"( select TROW.in_01 PACKAGE_ID, TROW.out_s_01 SERVICE_FILTER_GROUP, TROW.out_s_02 ZONE_FILTER_GROUP, TROW.out_s_03 DEVICE_FILTER_GROUP, TROW.out_s_04 SPECIFIC_APPLICABILITY, TROW.out_s_05 PERIOD_SET_ID, TROW.out_s_06 UOM, TROW.out_s_07 SPECIFIC_ROUNDING_ID, TROW.out_s_08 PRICE_TYPE, TROW.out_s_09 PRICE_ID, TROW.out_bd_10 PACKAGE_PRIORITY, TROW.out_s_11 NOTIFICATION_STATUS, TROW.out_s_12 NOTIFICATION_GROUP_ID, TROW.out_s_13 PACKAGE_NAME " + 
				"  from TRANSLATION_TABLE TTAB, TRANSLATION_INSTANCE TINS, TRANSLATION_ROW TROW " + 
				"  where TTAB.oid = TINS.ttab_oid " + 
				"  and TINS.oid = TROW.tins_oid " + 
				"  and TTAB.code ='PO_BAU_PACKAGE_DEF' " + 
				"  and TROW.in_end is null " + 
				"  and TINS.validation_date is null " + 
				") PAKG, " + 
				"( SELECT  SUAC.OID SUAC_OID, CACI_PARA.STRING_VALUE BILL_CYCLE " + 
				"  FROM SUBSCRIBER_ACCOUNT SUAC, CACO, CACI, CAPA, CACI_PARAMETER CACI_PARA, CAPA_NAMES_DICT PARA_KEY " + 
				"  WHERE CACO.SUAC_OID = SUAC.OID " + 
				//External ID - Change here
				"  and CACI.ext_id in ('S8414737') " + 
				"  AND CACI.CACO_OID = CACO.OID " + 
				"  AND CACI.CAPA_OID = CAPA.OID " + 
				"  AND CACI_PARA.CACO_OID = CACO.OID " + 
				"  AND CACI_PARA.CACI_OID = CACI.OID " + 
				"  AND CACI_PARA.PARTITION_ID = CACI.PARTITION_ID " + 
				"  AND PARA_KEY.CAPA_OID = CAPA.OID " + 
				"  AND CACI_PARA.NAME_KEY = PARA_KEY.NAME_KEY " + 
				"  AND PARA_KEY.NAME = 'BILL-CYCLE' " + 
				"  AND CACI_PARA.END_DATE IS NULL " + 
				") SUB_CYCLE, " + 
				"( select TROW.IN_01 BILL_CYCLE, TROW.OUT_BD_01 CYCLE_CLOSE_DATE " + 
				"  from TRANSLATION_TABLE TTAB, TRANSLATION_INSTANCE TINS, TRANSLATION_ROW TROW " + 
				"  where TTAB.oid = TINS.ttab_oid " + 
				"  and TINS.oid = TROW.tins_oid " + 
				"  and TTAB.code ='PO_BAU_BILL_CYCLE' " + 
				"  and TROW.in_end is null " + 
				"  and TINS.validation_date is null " + 
				") B_CYCLE " + 
				"WHERE CACO.SUAC_OID = SUAC.OID " + 
				"AND SUMT.SUAC_OID = SUAC.OID " + 
				"AND SUB_CYCLE.SUAC_OID = SUAC.OID " + 
				"AND EXT_ACT.SUAC_OID = SUAC.OID " + 
				"AND CACI.CACO_OID = CACO.OID " + 
				"AND CACI_TECH.CACO_OID = CACO.OID " + 
				"AND CACI_TECH.CACI_OID = CACI.OID  " + 
				"AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID " + 
				"AND CACI_TECH.END_DATE IS NULL " + 
				"AND SUMT_MARO.SUMT_OID = SUMT.OID " + 
				"AND SUMT_MARO.OUT_S_01 = OFFER_PAKG.OFFER_ID " + 
				"AND OFFER.OFFER_ID = OFFER_PAKG.OFFER_ID " + 
				"AND OFFER_PAKG.PACKAGE_ID = PAKG.PACKAGE_ID " + 
				"AND SUB_CYCLE.BILL_CYCLE = B_CYCLE.BILL_CYCLE " + 
				"and SUMT_MARO.OUT_N_02> to_number(to_char(sysdate, 'yyyymmddhhssmm')) " + 
				"order by EXT_ACT.EXTERNAL_ACCOUNT_CODE, CACI_TECH.VALUE, OFFER_PAKG.OFFER_ID,OFFER_PAKG.INDEX_ID";
		//SAPCC sub Usage n Packages (Query)
		String statement3 = "select SUAC.code ext_acct_code, CACI_TECH.value phone, CND.NAME_KEY, CND.name, to_char(CT.value) counter " + 
				"   from subscriber_account SUAC, CACO,CACI, counter CT ,capa_names_dict CND, CACI_USER_TECH_ID CACI_TECH " + 
				"   where CACO.SUAC_OID = SUAC.OID " +
				//External ID - Change Here
				"   AND CACO.EXT_ID = 'S8414737' " + 
				"   AND CACO.PARTITION_ID = SUAC.PARTITION_ID " + 
				"   AND CACI.CACO_OID = CACO.OID " + 
				"   AND CACI.PARTITION_ID = CACO.PARTITION_ID " + 
				"   AND CACI_TECH.CACO_OID = CACO.OID " + 
				"   AND CACI_TECH.CACI_OID = CACI.OID  " + 
				"   AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID " + 
				"   AND CACI_TECH.END_DATE IS NULL       " + 
				"   and CND.CAPA_OID = CACI.CAPA_OID " + 
				"   and CT.SUAC_OID = SUAC.OID " + 
				" 	and CT.PARTITION_ID = CACI.PARTITION_ID " + 
				"   and CT.COUN_KEY = CND.NAME_KEY " + 
				"   and CT.value != -1 " + 
				"   order by CT.COUN_KEY ";
		//prep_statement1 = con.prepareStatement(statement1);
		//Class.forName("com.mysql.jdbc.Driver");
		
		try{  
			//replace the IP address and SID(T15) to desired database and environment
			DBUtility dbUtil = new DBUtility("jdbc:oracle:thin:@172.21.238.26:1521:T15","coreDbUser","coreDbUserPassword");
			DbProcess dbProcess = new DbProcess();
			System.out.println(dbProcess.getMeterValue(dbUtil, "S8414506").toString());
			System.out.println("================");
			Connection con=dbUtil.getConnection();
			
			//Statement stmt= con.createStatement();  
			PreparedStatement pStmt = con.prepareStatement(MeterQuery);
			pStmt.setString(1, "S8414506");
			//Run Desired Query (Statement'#')
			ResultSet rs=pStmt.executeQuery();
			System.out.println("=="+rs.getFetchSize());
			while(rs.next()) {}  
				//System.out.println("=="+rs.getString(1));  
			con.close();
		}catch(SQLException e){ 
			System.out.println(e);
			e.printStackTrace();
			System.out.println();
			}         
		

 	}
}


