package com.zenith.automation.db;

public class DbConstants {
	final static String SUBSCRIBER_LEVEL_QUERY ="select "+ 
			"      SUAC.code ext_acct_code, CACI_TECH.value phone, CND.NAME_KEY, CND.name, to_char(CT.value) counter" + 
			"      from subscriber_account SUAC, CACO,CACI, counter CT ,capa_names_dict CND, CACI_USER_TECH_ID CACI_TECH" + 
			"      where CACO.SUAC_OID = SUAC.OID" + 
			"      AND CACO.EXT_ID = ?" + 
			"      AND CACO.PARTITION_ID = SUAC.PARTITION_ID" + 
			"      AND CACI.CACO_OID = CACO.OID" + 
			"      AND CACI.PARTITION_ID = CACO.PARTITION_ID" + 
			"      AND CACI_TECH.CACO_OID = CACO.OID" + 
			"      AND CACI_TECH.CACI_OID = CACI.OID " + 
			"      AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID" + 
			"      AND CACI_TECH.END_DATE IS NULL  " + 
			"      and CND.CAPA_OID = CACI.CAPA_OID" + 
			"      and CT.SUAC_OID = SUAC.OID" + 
			"      and CT.PARTITION_ID = CACI.PARTITION_ID" + 
			"      and CT.COUN_KEY = CND.NAME_KEY" + 
			"      and CT.value != -1" + 
			"      and CND.name like 'Meter%'" + 
			"      order by CT.COUN_KEY" ;
	
	final static String BAN_LEVEL_QUERY = "      select  " + 
			"      SUAC.code ext_acct_code, CACI_TECH.value phone, CND.NAME_KEY, CND.name, to_char(CT.value) counter  " + 
			"      from subscriber_account SUAC, CACO,CACI, counter CT ,capa_names_dict CND, CACI_USER_TECH_ID CACI_TECH" + 
			"      where CACO.SUAC_OID = SUAC.OID" + 
			"      --AND CACO.EXT_ID LIKE 'S%'" + 
			"      AND CACO.EXT_ID = ?" + 
			"      AND CACO.PARTITION_ID = SUAC.PARTITION_ID" + 
			"      AND CACI.CACO_OID = CACO.OID" + 
			"      AND CACI.PARTITION_ID = CACO.PARTITION_ID" + 
			"      AND CACI_TECH.CACO_OID = CACO.OID" + 
			"      AND CACI_TECH.CACI_OID = CACI.OID" + 
			"      AND CACI_TECH.PARTITION_ID = CACI.PARTITION_ID" + 
			"      AND CACI_TECH.END_DATE IS NULL      " + 
			"      and CND.CAPA_OID = CACI.CAPA_OID" + 
			"      and CT.SUAC_OID = SUAC.OID" + 
			"      and CT.PARTITION_ID = CACI.PARTITION_ID" + 
			"      and CT.COUN_KEY = CND.NAME_KEY" + 
			"     -- and CT.value != -1" + 
			"     --and CND.name like 'Meter%'" + 
			"      order by CT.COUN_KEY"; 

}
