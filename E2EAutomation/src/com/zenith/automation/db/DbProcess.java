package com.zenith.automation.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import com.zenith.automation.utility.DBUtility;

public class DbProcess {
	public HashMap<String,String> getMeterValue(DBUtility dbUtil,String externalAccountId) {
		Connection conn = dbUtil.getConnection();
		ResultSet rs = null;
		HashMap<String, String> dbResult = null;
		try {
			PreparedStatement prepStmt = conn.prepareStatement(DbConstants.SUBSCRIBER_LEVEL_QUERY);
			prepStmt.setString(1, externalAccountId);
			rs= prepStmt.executeQuery();
			if(rs != null) {
				dbResult = new HashMap<String, String>();
			}
			ResultSetMetaData data = rs.getMetaData();
			while(rs.next()) {
				dbResult.put(rs.getString(4), rs.getString(5));  
			}
		} catch (SQLException e) {
			System.out.println("Database error....");
			e.printStackTrace();
		}catch(Exception e) {
			System.out.println("Unable to fetch record..");
			e.printStackTrace();
		}finally{
			if(dbUtil != null) {
				dbUtil.closeConnection();
			}
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return dbResult;
		
	}
	public HashMap<String,String> convertAlToMap(List<String> list){
		HashMap<String, String> resultMap = null;
		if(list != null) {
			resultMap = new HashMap<String, String>();
			for(String element : list) {
				String arrKeyVal[] = element.split("=");
				if(arrKeyVal.length == 2) {
					String keyValue[] = element.split("=");
					resultMap.put(keyValue[0], keyValue[1]);
				}else {
					resultMap.put(element, null);
				}
			}
		}
		return resultMap;
	}

}
