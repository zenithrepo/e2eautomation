package com.zenith.automation.db;

import java.util.HashMap;

public class DbResult {
	
	HashMap<String, String> mapResults;
	
	public HashMap<String, String> getMapResults() {
		return mapResults;
	}
	
	public void setMapResults(HashMap<String, String> mapResults) {
		this.mapResults = mapResults;
	}

	@Override
	public String toString() {
		return "DbResult [mapResults=" + mapResults + "]";
	}
	
}
