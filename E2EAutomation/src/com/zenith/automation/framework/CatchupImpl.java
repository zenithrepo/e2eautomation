package com.zenith.automation.framework;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.highdeal.cnd.message.StatefulServiceClient;
import com.highdeal.hci.Type;
import com.highdeal.pnr.hci.ChargeableItem;
import com.zenith.automation.highdeal.CIC;
import com.zenith.automation.highdeal.CatchupProcessor;
import com.zenith.automation.highdeal.MaintainSubscriberUtil;
import com.zenith.automation.highdeal.SubscriberMaintainProcessor;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.PropLoader;

public class CatchupImpl implements ExecutionHandler{
	
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	SubscriberMaintainProcessor processor = null;
	@Override
	public TestExecutorHandler process(TestExecutorHandler executionHandler) {

		MaintainSubscriberUtil maintainSubUtil = MaintainSubscriberUtil.getMaintainSubscriberInstanse();
		
		if(executionHandler instanceof TestStep) {
			TestStep testStep = (TestStep)executionHandler;
			if(testStep.getWaitTime() > 0) {
				Timer timer = new Timer(testStep.getWaitTime());
				timer.pause();
			}
			Connection conn = TestSuite.getEndSystems().get(testStep.getEnvironment()+"_SAPCCDISPATCHER");
			StatefulServiceClient statefulClient = maintainSubUtil.getStatefulCleint();
			if(statefulClient == null) {
				maintainSubUtil.initializeConnection(conn.getHostname(), conn.getPort(), 300);
			}
			processor = new SubscriberMaintainProcessor();
			
			try {
				if(statefulClient != null) {
					CatchupProcessor processor = new CatchupProcessor();
					ChargeableItem chargeableItem = processor.getCatchupCic(testStep);
					boolean chargeResult = processor.catchupCharge(chargeableItem, testStep.phoneNumber, "Data");
					if(chargeResult) {
						System.out.println("Catchup completed...");
					}
				
				}else {
					System.out.println("Could not connect to SAP CC : "+conn.getHostname());
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				if(statefulClient != null) {
					statefulClient.close();
				}
				System.out.println("Connenction closed to SAP CC");
			}
		}else{
			System.out.println("Unhandled class"+this.getClass().getSimpleName());
		}
		return executionHandler;
	}
	
	
	public Map<String, Object> getAllCounter(String sAllCounters){
		HashMap<String, Object> initializedCtr = null;
		if(sAllCounters != null) {
			initializedCtr = new HashMap<String, Object>();
			String counters[] = sAllCounters.split(",");
			for(String eachCtr : counters) {
				String []keyValue = eachCtr.split("=");
				String _key = keyValue[0];
				Object _value= keyValue[1];
				initializedCtr.put(_key, _value);
			}
		}else{
			System.out.println("Counter not set in configuration.prop file");
		}
		return initializedCtr;
	}
	
	public void resetCounter(String propertyKey, String seriveIdentier, String userIdentfier) {
		Map<String, Object> mapCtrs = getAllCounter(propLoader.getValue(propertyKey));
		Set<String> keys = mapCtrs.keySet();
		for(String key : keys) {
			Object _value = mapCtrs.get(key);
			CIC cic = new CIC("Counter Update");
			ChargeableItem chargeableItem = cic.getChargeableItem();
			cic.addCicField(chargeableItem, "NAME", Type.STRING, key);
			cic.addCicField(chargeableItem, "VALUE", Type.NUMBER, _value);
			boolean statusFlag = processor.resetEachCounter(chargeableItem, seriveIdentier, userIdentfier);
			if(statusFlag) {
				System.out.println("Success : Counter:"+key+ " ,updated with :"+_value.toString());
			}else {
				System.out.println("Failed : Counter:"+key+ " ,updated failed");
			}
		}
	}
}
