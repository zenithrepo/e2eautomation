package com.zenith.automation.framework;

import java.util.Date;

public class Constants {

	//Contansts to record timestamp of each Flow
	public static Date MZ_HIT_TS = null;
	//Ends here
	
	public static final long GET_OLD_NOTIFICATION = 9l;
	//As per analysis Server is running behind by 24 sec
	public static final long SERVER_RUNNIG_BEHING = 60*1000;//30 secs
	public static final long GET_OLD_FILES = 1*5*60*1000;// minutes * seconds * 1000 .Used for fetching 5 mins old files from server.
	public static final long MAX_BYTE_DOMESTIC = 10485760;//10MB
	public static final long MAX_BYTE_ROAMING = 204800;//200KB
	public static final long DEFAULT_BYTE = 204800; //when no GSU is received. send this USU
	public static final int START_SESSION_REQ = 1;
	public static final int UPDATE_SESSION_REQ = 2;
	public static final int STOP_SESSION_REQ = 3;
	//Constants for XML parsing starts here
	public static final String CONNECTION = "connection";
	public static final String CLASS = "class";
	public static final String NAME = "name";
	public static final String ENV = "env";
	public static final String URL = "url";
	public static final String USERNAME = "userid";
	public static final String PASSWORD = "password";
	public static final String SESSION_ID = "session-id";
	public static final String DIRECTORY = "dir";
	public static final String TEST_CASE = "test-case";
	public static final String TEST_SUITE = "test-suite";
	public static final String TEST_ASSERT = "test-assert";
	public static final String TEST_STEP = "test-step";
	public static final String TEST_REPORT = "test-report";
	public static final String TEST_PREREQUITSIES = "test-prerequisites";
	public static final String ASSERT_TYPE = "assert-type";
	public static final String FILE_NAME = "filename";
	public static final String PATH = "path";
	public static final String BAN = "ban";
	public static final String USAGE_TYPE = "usage-type";
	public static final String USAGE = "usage";
	public static final String SUBSCRIBER = "sub";
	public static final String WAIT_TIMER= "wait-timer-sec";
	public static final String TYPE= "type";
	public static final String PRECHECK_FIELD = "precheck-fields";
	public static final String RATING_GROUP = "rating-group";
	public static final String MCC_MNC_XML = "mcc-mnc";
	public static final String UNIT = "unit";
	public static final int  DEFAULT_NUMBER = -1;
	public static final String APN = "apn";
    public static final String MZ_HOST = "mz-host";
    public static final String PHONE_NUM = "phoneNumber";
    public static final String ACTION = "action";
    public static final String SUITE_ID = "suite-id";
    public static final String MULTI_SESSION = "multi-seesion";
    public static final String TEST_CASE_ID = "test-case-id";
    public static final String VALUES = "values";
    //Contants for XML parsing ends here
    
	//MZ Hosts Starts
    public final String POST_VM6 = "pspostmz3pgw";
    public final String PRE_VM6 = "pspremz3pgw";
    public final String PRE_MZ1 = "srbhmz1pgw";
    public final String PRE_MZ2 = "srbhmz2pgw";
    public final String PRE_MZ3 = "srbhmz3pgw";
    public final String PRE_MZ4 = "srbhmz4pgw";
    public final String POST_MZ1 = "srbhmzpost1pgw";
    public final String POST_MZ2 = "srbhmzpost2pgw";
    public final String POST_MZ3 = "srbhmzpost3pgw";
    public final String POST_MZ4 = "srbhmzpost4pgw";
    public final String POST_MZ5 = "srbhmzpost5pgw";
    //MZ Hosts ends

    //Opertors support for Assertion starts.
    public static final int OPR_EQUAL = 1;
    public static final int OPR_NOT_EQUAL = 2;
    public static final int OPR_LESS_THAN = 3;
    public static final int OPR_LT_EQUAL = 4;
    public static final int OPR_GREATER_THAN = 5;
    public static final int OPR_GT_EQUAL = 6;
    public static final int OPR_SUM_DIFF = 7;//Used when we are  not sure how much total amount will be charged, but sure that only x amount shall be charged 
    public static final int OPR_SUB_DIFF = 8;//Used when we are  not sure how much total amount will be charged, but sure that only -x amount shall be charged
    //Opertors support for Assertion ends.
    
	//Contansts for TDR Start here
	public static final String CHARGING_ID = "CHARGING-ID";
	public static final String GGSN_IP = "GGSN-IP";
	public static final String IMEI = "IMEI";
	public static final String IMSI = "IMSI";
	public static final String SGSN_IP = "SGSN-IP";
	public static final String MCC_MNC = "MCC-MNC";
	public static final String CHARGING_CHARACTERISTICS = "CHARGING-CHARACTERISTICS";
	public static final String SERVICE_IDENTIFIER = "SERVICE-IDENTIFIER";
	public static final String SERVICE_ID = "SERVICE-ID";
	public static final String ACCESS_POINT_NAME = "ACCESS-POINT-NAME";
	public static final String TERMINATION_CAUSE = "TERMINATION-CAUSE";
	public static final String CONSUMPTION_DATE = "CONSUMPTION-DATE";
	public static final String VOLUME = "VOLUME";
	public static final String RECEIVED_UOM = "RECEIVED-UOM";
	public static final String ACCESS_LOGIN = "ACCESS-LOGIN";
	public static final String CONTRACT_ITEM_EXTERNAL_ID = "CONTRACT-ITEM-EXTERNAL-ID";
	public static final String CONTRACT_ID = "CONTRACT-ID";
	public static final String TIME_ZONE = "TIME-ZONE";
	public static final String BRAND_ID = "BRAND-ID";
	public static final String OFFER_ID = "OFFER-ID";
	public static final String PACKAGE_ID = "PACKAGE-ID";
	public static final String OFFER_START_DATE = "OFFER-START-DATE";
	public static final String OFFER_EXPIRY_DATE = "OFFER-EXPIRY-DATE";
	public static final String UNITS_CONSUMED = "UNITS-CONSUMED";
	public static final String UOM = "UOM";
	public static final String REMAINING_UNITS = "REMAINING-UNITS";
	public static final String GRANTED_SERVICE_UNITS = "GRANTED-SERVICE-UNITS";
	public static final String DEVICE_TYPE = "DEVICE-TYPE";
	public static final String CHARGE_AMOUNT = "CHARGE-AMOUNT";
	public static final String LIST_CHARGE_AMOUNT = "LIST-CHARGE-AMOUNT";
	public static final String LOCAL_TIME_OF_EVENT = "LOCAL-TIME-OF-EVENT";
	public static final String CHARGING_RESERVATION_ID = "CHARGING-RESERVATION-ID";
	public static final String CONFIRMATION_DATE = "CONFIRMATION-DATE";
	public static final String PACKAGE_UOM = "PACKAGE-UOM";
	public static final String RAT = "RAT";
	public static final String RATING_EXCEPTION_RSN_CD = "RATING-EXCEPTION-RSN-CD";
	public static final String BILL_YEAR_NUM = "BILL-YEAR-NUM";
	public static final String BILL_MONTH_NUM = "BILL-MONTH-NUM";
	public static final String BILL_CYCLE = "BILL-CYCLE";
	public static final String SERVICE_PKG = "SERVICE-PKG";
	public static final String RATE_PLAN = "RATE-PLAN";
	public static final String MIN_CD = "MIN-CD";
	public static final String MARKET = "MARKET";
	public static final String TDR_BAN = "BAN";
	public static final String EXTERNAL_SUB_ID = "EXTERNAL-SUB-ID";
	public static final String SERVING_SID = "SERVING-SID";
	public static final String EVENT_SOURCE = "EVENT-SOURCE";
	public static final String EVENT_TYPE = "EVENT-TYPE";
	public static final String SERVICE = "SERVICE";
	public static final String SERVICE_LEVEL = "SERVICE-LEVEL";
	public static final String EVENT_NAME = "EVENT-NAME";
	public static final String RATING_REASON_CODE = "RATING-REASON-CODE";
	public static final String SESSIONID = "SESSION-ID";
	public static final String CELL_SITE_ID = "CELL-SITE-ID";
	public static final String ZONE_FILTER = "ZONE-FILTER";
	public static final String TRANSACTION_TYPE = "TRANSACTION-TYPE";
	public static final String CHARGED_VOLUME = "CHARGED-VOLUME";
	public static final String SOC_SEQ_NO = "SOC-SEQ-NO";
	public static final String ALLOWANCE_PREVIOUS_PERIOD = "ALLOWANCE-PREVIOUS-PERIOD";
	public static final String NUMBER_OF_CYCLES = "NUMBER-OF-CYCLES";
	public static final String ALLOWANCE_QUOTA_PERIOD = "ALLOWANCE-QUOTA-PERIOD";
	public static final String INTERVAL_START_DATE = "INTERVAL-START-DATE";
	public static final String INTERVAL_END_DATE = "INTERVAL-END-DATE";
	public static final String RECURRING_TYPE = "RECURRING-TYPE";
	public static final String SERVICE_FILTER_GROUP = "SERVICE-FILTER-GROUP";
	public static final String SHARING_GROUP_NAME = "SHARING-GROUP-NAME";
	public static final String MONTHLY_CAP_VALUE = "MONTHLY-CAP-VALUE";
	public static final String MONTHLY_CHARGE_ACCUMULATION = "MONTHLY-CHARGE-ACCUMULATION";
	public static final String ZONE_FILTER_GROUP = "ZONE-FILTER-GROUP";
	public static final String RATE_PER_SCALE_PERIOD = "RATE-PER-SCALE-PERIOD";
	public static final String SCALE = "SCALE";
	public static final String SCALE_START_LEG = "SCALE-START-LEG";
	public static final String SCALE_END_LEG = "SCALE-END-LEG";
	public static final String LOCATION_INFO_PART_1 = "LOCATION-INFO-PART-1";
	public static final String LOCATION_INFO_PART_2 = "LOCATION-INFO-PART-2";
	public static final String CURRENT_THRESHOLD = "CURRENT-THRESHOLD";
	public static final String SPECIAL_RATING_CONDITION_CD = "SPECIAL-RATING-CONDITION-CD";
	public static final String BILL_CUTOVER_INDICATOR = "BILL-CUTOVER-INDICATOR";
	public static final String PRODUCTION_STATUS_ = "PRODUCTION-STATUS-";
	public static final String FILE_NM_TXT = "FILE-NM-TXT";
	public static final String SERVICE_OPTION_CD = "SERVICE-OPTION-CD";
	public static final String SOURCE_ID = "SOURCE-ID";
	public static final String PMN_ID = "PMN-ID";
	public static final String LOC_CITY_NAME = "LOC-CITY-NAME";
	public static final String LOC_PROV_CD = "LOC-PROV-CD";
	public static final String COUNTRY_CD = "COUNTRY-CD";
	public static final String RATING_PERIOD_MONTH = "RATING-PERIOD-MONTH";
	public static final String RATING_PERIOD_YR = "RATING-PERIOD-YR";
	public static final String SPECIAL_RATING_CONDITION_SERVICE_PKG = "SPECIAL-RATING-CONDITION-SERVICE-PKG";
	public static final String TIMESTAMP= "timestamp";
	//Contants for TDR ends here
	
	//EDR PPU starts here
	//public static final String CONSUMPTION_DATE = "CONSUMPTION_DATE";
	public static final String SYSTEM_DATE = "SYSTEM_DATE";
	//public static final String BRAND_ID = "BRAND_ID";
	public static final String EDR_BAN = "BAN";
	public static final String PHONE_NUMBER = "PHONE_NUMBER";
	public static final String SUBSCRIBER_ID = "SUBSCRIBER_ID";
	//public static final String TIME_ZONE = "TIME_ZONE";
	public static final String EDR_EVENT = "EDR_EVENT";
	//public static final String BILL_YEAR_NUM = "BILL_YEAR_NUM";
	//public static final String BILL_MONTH_NUM = "BILL_MONTH_NUM";
	//public static final String BILL_CYCLE = "BILL_CYCLE";
	public static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
	public static final String ACCOUNT_SUB_TYPE = "ACCOUNT_SUB_TYPE";
	public static final String LANGUAGE = "LANGUAGE";
	public static final String SOC_CD = "SOC_CD";
	//public static final String OFFER_ID = "OFFER_ID";
	//public static final String PACKAGE_ID = "PACKAGE_ID";
	public static final String OFFER_RECURRENCE_TYPE = "OFFER_RECURRENCE_TYPE";
	public static final String VOLUME_KB = "VOLUME_KB";
	public static final String AMOUNT = "AMOUNT";
	public static final String WCC_CHARGE_AMOUNT = "WCC_CHARGE_AMOUNT";
	public static final String WCC_AMOUNT_before = "WCC_AMOUNT_BEFORE";
	public static final String WCC_AMOUNT_after = "WCC_AMOUNT_AFTER";
	public static final String THRESHOLD_CROSSED = "THRESHOLD_CROSSED";
	public static final String PPU_ZONE = "PPU_ZONE";
	public static final String WCC_ZONE = "WCC_ZONE";
	public static final String NOTIFICATION_CODE = "NOTIFICATION_CODE";
	//public static final String CHARGING_ID = "CHARGING_ID";
	//public static final String IMSI = "IMSI";
	public static final String FORCE = "FORCE";
	//EDR PPU ends here

	//EDR BLOCK start here
	//public static final String CONSUMPTION_DATE = "CONSUMPTION_DATE";
	//public static final String BRAND_ID = "BRAND_ID";
	//public static final String CONTRACT_ID = "CONTRACT_ID";
	//public static final String CONTRACT_ITEM_EXTERNAL_ID = "CONTRACT_ITEM_EXTERNAL_ID";
	//public static final String EXTERNAL_SUB_ID = "EXTERNAL_SUB_ID";
	//public static final String BAN = "BAN";
	//public static final String TIME ZONE = "TIME ZONE";
	//public static final String EDR_EVENT = "EDR_EVENT";
	//public static final String BILL_YEAR_NUM = "BILL_YEAR_NUM";
	//public static final String BILL_MONTH_NUM = "BILL_MONTH_NUM";
	//public static final String BILL_CYCLE = "BILL_CYCLE";
	public static final String SERVICE_TYPE = "SERVICE_TYPE";
	public static final String EDR_TYPE = "TYPE";
	public static final String LEVEL = "LEVEL";
	public static final String APPLICATION_SOURCE = "APPLICATION_SOURCE";
	public static final String ZONE = "ZONE";
	public static final String REQUESTOR_ID = "REQUESTOR_ID";
	public static final String BLOCKING_THRESHOLD = "BLOCKING_THRESHOLD";
	//public static final String RAT = "RAT";
	public static final String ACCOUNT_LEVEL_IND = "ACCOUNT_LEVEL_IND";
	//EDR BLOCK ends here
	
}

