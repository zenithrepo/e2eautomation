package com.zenith.automation.framework;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import com.zenith.automation.utility.PropLoader;

public class DefaultParams {
	PropLoader pLoader = PropLoader.getPropLoaderInstance();
	
	final static String FIELD_SEP = "\\|";
	final static String VALUE_SEP = ",";
	public static void main(String []a) {
		DefaultParams df = new DefaultParams();
		Map<String,LinkedList<ParamStructure> > fv = df.getDefaultFieldValue();
		for(String key : fv.keySet()) {
			System.out.println("key:"+key);
			LinkedList<ParamStructure> ll = fv.get(key) ;
			for(ParamStructure ps : ll) {
				System.out.println(ps.toString());
			}
		}
		
	}
	
	public Map<String,LinkedList<ParamStructure> > getDefaultFieldValue(){
		Map<String,LinkedList<ParamStructure>>  paramValue = new LinkedHashMap<String,LinkedList<ParamStructure>>();
		//Online MZ
		if(pLoader.getValue("OLMZ_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("OLMZ_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1);
				liFields.add(getParam(field));
			}
			paramValue.put("OL_MZ", liFields);	
		}
		
		if(pLoader.getValue("SAPCC_TDR_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("SAPCC_TDR_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1); 
				liFields.add(getParam(field));
			}
			paramValue.put("TDR", liFields);	
		}
		
		if(pLoader.getValue("SAPCC_EDR_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("SAPCC_EDR_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1); 
				liFields.add(getParam(field));
			}
			paramValue.put("EDR", liFields);	
		}
		

		if(pLoader.getValue("SAPCC_BLOCKEDR_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("SAPCC_BLOCKEDR_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1); 
				liFields.add(getParam(field));
			}
			paramValue.put("BLOCKEDR", liFields);	
		}

		if(pLoader.getValue("SAPCC_METER_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("SAPCC_METER_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1); 
				liFields.add(getParam(field));
			}
			paramValue.put("SAPCC_METER", liFields);	
		}

		if(pLoader.getValue("NOTIF_PARAMS") != null) {
			LinkedList<ParamStructure> liFields = new LinkedList<ParamStructure>();
			String []fields = pLoader.getValue("NOTIF_PARAMS").split(FIELD_SEP);
			for(String eachElement : fields) {
				String field[] = eachElement.split(VALUE_SEP, -1); 
				liFields.add(getParam(field));
			}
			paramValue.put("NOTIF_PARAMS", liFields);	
		}
		return paramValue;
	}
	public ParamStructure getParam(String []values) {
		
		if(values.length == 3) {
			return new ParamStructure(values[0], values[1], values[2]);
		}else {
			return null;
		}
	}
}
