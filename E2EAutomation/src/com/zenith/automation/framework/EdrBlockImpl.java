package com.zenith.automation.framework;

import com.zenith.automation.batch.BatchCollection;
import com.zenith.automation.batch.EdrBlockParser;
import com.zenith.automation.batch.EdrPpuParser;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;

public class EdrBlockImpl  implements ExecutionHandler {

	@Override
	public TestExecutorHandler process(TestExecutorHandler testHandler) {
		if(testHandler instanceof TestAssert) {
			TestAssert testAssert = (TestAssert)testHandler;
			String tmpDir = null;
			if(testAssert.getWaitTime() > 0) {
				Timer timer = new Timer(testAssert.getWaitTime());
				timer.pause();
			}
			try {
				PropLoader propLoader = PropLoader.getPropLoaderInstance();
				tmpDir = propLoader.getValue("TEST_RESULT");
				tmpDir = tmpDir +"\\"+ testHandler.getSuiteName()+"\\downloads\\edrblock\\";
				CommonUtility.verifyDirectory(tmpDir);
	
				Connection conn = TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_EDRBLOCK");
				//get all tdr..processed 2 mins back
				BatchCollection batchCollection = new BatchCollection();
				batchCollection.copyFilesToLocal(conn.getUrl(), conn.getDir(), conn.getUsername(), conn.getPassword(), tmpDir);
			}catch(Exception e) {
				e.printStackTrace();
			}
			EdrBlockParser ppuParser = new EdrBlockParser();
			testHandler = ppuParser.compareBlockEdr(testAssert, tmpDir);
		}

		return testHandler;
	}

}
