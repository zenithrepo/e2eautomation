package com.zenith.automation.framework;

import com.zenith.automation.batch.BatchCollection;
import com.zenith.automation.batch.EdrPpuParser;
import com.zenith.automation.batch.TdrParser;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;

public class EdrPpuImpl implements ExecutionHandler {

	@Override
	public TestExecutorHandler process(TestExecutorHandler testHandler) {
		if(testHandler instanceof TestAssert) {
			String tmpDir = null;
			TestAssert testAssert = (TestAssert)testHandler;
			if(testAssert.getWaitTime() > 0) {
				Timer timer = new Timer(testAssert.getWaitTime());
				timer.pause();
			}
			try {
				PropLoader propLoader = PropLoader.getPropLoaderInstance();
				tmpDir = propLoader.getValue("TEST_RESULT");
				tmpDir = tmpDir +"\\"+ testHandler.getSuiteName()+"\\downloads\\edrppu\\";
				CommonUtility.verifyDirectory(tmpDir);
	
				Connection conn = TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_EDRPPU");
				//get all tdr..processed 2 mins back
				BatchCollection batchCollection = new BatchCollection();
				batchCollection.copyFilesToLocal(conn.getUrl(), conn.getDir(), conn.getUsername(), conn.getPassword(), tmpDir);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//accumulate the columns provided in assert type
			//  ...getAccumulateVaues()
			EdrPpuParser ppuParser = new EdrPpuParser();
			testHandler = ppuParser.comparePpuEdr(testAssert, tmpDir);
		}
		return testHandler;
	}

}
