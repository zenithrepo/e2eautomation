package com.zenith.automation.framework;

public interface ExecutionHandler {
	public TestExecutorHandler process(TestExecutorHandler executionHandler); 

}
