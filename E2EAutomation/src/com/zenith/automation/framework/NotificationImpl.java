package com.zenith.automation.framework;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.zenith.automation.notification.NotificationProcess;
import com.zenith.automation.notification.NotificationResult;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.DBUtility;

public class NotificationImpl implements ExecutionHandler{

	@Override
	public TestExecutorHandler process(TestExecutorHandler executionHandler) {
		if(executionHandler instanceof TestAssert) {
			TestAssert testAssert = (TestAssert)executionHandler;
			int waitTime = testAssert.getWaitTime();
			if(waitTime > 0) {
				Timer timer = new Timer(waitTime);
				timer.pause();
			}
			Connection conn = TestSuite.getEndSystems().get(executionHandler.getEnvironment()+"_NOTIFICATIONDB");
			DBUtility dbUtil = new DBUtility(conn.getUrl(), conn.getUsername(), conn.getPassword());
			NotificationProcess notifProcess = new NotificationProcess();
			executionHandler.actualResultNotification = notifProcess.getNotification(dbUtil, testAssert);
			executionHandler.expectedResultNotification = convertAssertToLiNotif(testAssert.getAssertType());
			System.out.println("=======>>>> Assert Value  "+testAssert.getAssertType());
			System.out.println("=======>>>> Computed Value"+notifProcess.getNotification(dbUtil, testAssert));
			
		}
		return executionHandler;
	}
	
	//Convert Assert type to List of Notification.Mulitple Notification to be separated by "|" and reach field to be separated by "," 
	public List<NotificationResult> convertAssertToLiNotif(ArrayList<String> notification){
		List<NotificationResult> liNotif = new LinkedList<NotificationResult>();
		if(notification != null) {
			for(String notif : notification) {
				NotificationResult notifResult = new NotificationResult();
				String fields[] = notif.split(",");
				for(String field : fields) {//STATUS=DELIVRD,TRANSACTION_DEFINITION_CD=ABC
					if(field.equalsIgnoreCase("Status")) {
						notifResult.setSmsProcessStatusCode(field);
					}else if(field.equalsIgnoreCase("TRANSACTION_DEFINITION_CD")) {
						notifResult.setTransactionDefinitionCode(field);
					}
					liNotif.add(notifResult);
				}
			}
			
		}

		return liNotif;
	}

}
