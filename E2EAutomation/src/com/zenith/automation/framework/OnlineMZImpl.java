package com.zenith.automation.framework;

import com.zenith.automation.http.HTTPHandler;
import com.zenith.automation.thread.Timer;

public class OnlineMZImpl implements ExecutionHandler {

	@Override
	public TestExecutorHandler process(TestExecutorHandler testHandler) {
		if(testHandler instanceof TestStep) {
			TestStep testStep = (TestStep)testHandler;
			if(testStep.getWaitTime() > 0) {
				Timer timer = new Timer(testStep.getWaitTime());
				timer.pause();
			}
			HTTPHandler httpHandler = new HTTPHandler();
			boolean didHitOnlineMz = httpHandler.hitOnlineMZ(testStep, TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_OLMZ"));
			if(didHitOnlineMz) {
				//System.out.println(">>>>>> Request sent MZ");
			}else {
				System.out.println("Failed to hit online Mz");
			}

		}else {
			System.out.println("Unhandled class");
		}
		return testHandler;
	}

}
