package com.zenith.automation.framework;

public class Operation {
	private String key;
	private Object value;
	private int operator;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public int getOperator() {
		return operator;
	}
	public void setOperator(int operator) {
		this.operator = operator;
	}
	@Override
	public String toString() {
		return "Operation [key=" + key + ", value=" + value + ", operator=" + operator + "]";
	}

	
}
