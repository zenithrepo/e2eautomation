package com.zenith.automation.framework;

public class ParamStructure {
	private String name;
	private String defValue;
	private String description;
	
	
	public ParamStructure(String name, String defValue, String description) {
		super();
		this.name = name;
		this.defValue = defValue;
		this.description = description;
	}
	
	public String getName() {
		return name;
	}
	public String getDefValue() {
		return defValue;
	}
	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		return "ParamStructure [name=" + name + ", defValue=" + defValue + ", description=" + description + "]";
	}
	
	
}
