package com.zenith.automation.framework;

import java.io.FileNotFoundException
;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jaxen.expr.DefaultAbsoluteLocationPath;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.zenith.automation.utility.PropLoader;


//References from
//https://www.javacodegeeks.com/2012/01/xml-parsing-using-saxparser-with.html
public class ReadXML extends DefaultHandler{
	TestSuite testSuite;
	String sTestCaseName = null;
	TestCase testCase = null;
	String tmpValue;
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	
	public TestSuite getDecodedXML() {
		return this.testSuite;
	}
	
	
	public static void main(String argv[]){
		new ReadXML();
	}
	
	public ReadXML(){
		testSuite = new TestSuite();
		testSuite.endSystems = new LinkedHashMap();
		testSuite.testCases = new LinkedHashMap();
		parseDocument();
	}
	
    private void parseDocument() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(propLoader.getValue("TEST_DATA_XML"), this);
        } catch (ParserConfigurationException e) {
            System.out.println("ParserConfig error");
        } catch (SAXException e) {
            System.out.println("SAXException : xml not well formed");
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found Exception");
        }catch(IOException e) {
        	System.out.println("IO Exception");
        }catch(Exception e) {
        	e.printStackTrace();
        }
    }
    
    @Override
    public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
    	//System.out.println("startElement:"+s+":"+s1+":"+elementName+":"+attributes.toString());
    	if (elementName.equalsIgnoreCase("test-suite")) {
    		testSuite.setTestSuiteName(attributes.getValue(Constants.NAME));
    		testSuite.setEnv(attributes.getValue(Constants.ENV));
    		if(testSuite.getEnv() != null) {
    			setConnections(testSuite,testSuite.getEnv());
    		}
    		if(attributes.getValue(Constants.SUITE_ID) != null &&  attributes.getValue(Constants.SUITE_ID).trim() != "") {
    			testSuite.setSuiteId(Integer.parseInt(attributes.getValue(Constants.SUITE_ID)));
    		}
    	}
    	

    	if (elementName.equalsIgnoreCase(Constants.TEST_CASE)) {
    		testCase  = new TestCase();
    		testCase.steps = new LinkedList<TestExecutorHandler>();
    		sTestCaseName = attributes.getValue(Constants.NAME);
    		testCase.setName(sTestCaseName);
    		String sTestCaseEnv = attributes.getValue(Constants.ENV);
    		if(sTestCaseEnv != null) {//Environment from test case level
    			testCase.setEnvironment(sTestCaseEnv);
    			setConnections(testSuite,sTestCaseEnv);
    		}else {//environment from testSuite level
    			testCase.setEnvironment(testSuite.getEnv());
    			setConnections(testSuite,testSuite.getEnv());
    		}
    		if(attributes.getValue(Constants.TEST_CASE_ID) != null &&  attributes.getValue(Constants.TEST_CASE_ID).trim() != "") {
    			testCase.setTestCaseId(Integer.parseInt(attributes.getValue(Constants.TEST_CASE_ID)));
    		}

    		testSuite.testCases.put(sTestCaseName, testCase);
    	}
    	if (elementName.equalsIgnoreCase(Constants.TEST_PREREQUITSIES)) {
    		TestPrerequisities testPrerequisties = new TestPrerequisities();
    		testPrerequisties.setPkgClass(attributes.getValue(Constants.CLASS));
    		testPrerequisties.setType(Constants.TYPE);
    		testPrerequisties.setPreCheckField(attributes.getValue(Constants.PRECHECK_FIELD));
    		testPrerequisties.setSub(attributes.getValue(Constants.SUBSCRIBER));
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testPrerequisties.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testPrerequisties.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		testPrerequisties.setBan(attributes.getValue(Constants.BAN));
    		testPrerequisties.setBan(attributes.getValue(Constants.PHONE_NUM));
    		testCase.steps.add(testPrerequisties);
    	}

    	if (elementName.equalsIgnoreCase(Constants.TEST_STEP)) {
    		TestStep testStep = new TestStep();
    		testStep.setPkgClass(attributes.getValue(Constants.CLASS));
    		testStep.setBan(attributes.getValue(Constants.BAN));
    		testStep.setSubscriberId(attributes.getValue(Constants.SUBSCRIBER));
    		testStep.setSessionId(attributes.getValue(Constants.SESSION_ID));
    		String sUsage = attributes.getValue(Constants.USAGE);
    		if(sUsage != null && sUsage != "") {
    			testStep.setUsage(Integer.parseInt(sUsage));
    		}else {
    			testStep.setUsage(Constants.DEFAULT_NUMBER);
    		}
    		String rg = attributes.getValue(Constants.RATING_GROUP);
    		if(rg != null && rg != "") {
    			testStep.setRatingGroup(rg);
    		}else {
    			testStep.setRatingGroup("10101");
    		}
    		testStep.setUnit(attributes.getValue(Constants.UNIT));
    		if(attributes.getValue(Constants.MCC_MNC_XML) != null && attributes.getValue(Constants.MCC_MNC_XML) != "") {
    			int iMccMnc =-1;
    			iMccMnc = Integer.parseInt(attributes.getValue(Constants.MCC_MNC_XML));
    			testStep.setMccMnc(iMccMnc);
    		}else {
    			testStep.setMccMnc(302220);
    		}
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testStep.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testStep.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		testStep.setApn(attributes.getValue(Constants.APN));
    		testStep.setMzHost(attributes.getValue(Constants.MZ_HOST));
    		testStep.setTimestamp(attributes.getValue(Constants.TIMESTAMP));
    		testStep.setPhoneNumber(attributes.getValue(Constants.PHONE_NUM));
    		testStep.setAction(attributes.getValue(Constants.ACTION));
    		testStep.setMultiSession(attributes.getValue(Constants.MULTI_SESSION));
    		testStep.setValues(attributes.getValue(Constants.VALUES));
    		testStep.setType(attributes.getValue(Constants.TYPE));
    		testCase.steps.add(testStep);
    	}
    	if (elementName.equalsIgnoreCase(Constants.TEST_ASSERT)) {
    		TestAssert testAssert =  new TestAssert();
    		testAssert.setPkgClass(attributes.getValue(Constants.CLASS));
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testAssert.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testAssert.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		testAssert.setIdentifier(attributes.getValue(Constants.SUBSCRIBER));
    		testAssert.setSessionId(attributes.getValue(Constants.SESSION_ID));
    		testAssert.setBan(attributes.getValue(Constants.BAN));
    		ArrayList alAssetType = new ArrayList(Arrays.asList(attributes.getValue(Constants.ASSERT_TYPE).split(",")));
    		testAssert.setAssertType(alAssetType);
    		testCase.steps.add(testAssert);
    		//testCase.validationSteps.add(testAssert);
    	}
    	if (elementName.equalsIgnoreCase(Constants.TEST_REPORT)) {
    		TestReport testReport = new TestReport();
    		testReport.setFileName(attributes.getValue(Constants.FILE_NAME));
    		testReport.setFileToDir(attributes.getValue(Constants.PATH));
    		testSuite.testReport = testReport;
    	}
    }
    
    
    @Override
    public void endElement(String s, String s1, String element) throws SAXException {
 
    	if (element.equals("book")) {
    		//bookL.add(bookTmp);
    	}

	    if (element.equalsIgnoreCase("isbn")) {
	        //bookTmp.setIsbn(tmpValue);
	    }
	
	    if (element.equalsIgnoreCase("title")) {
	        //bookTmp.setTitle(tmpValue);
	    }
	    if(element.equalsIgnoreCase("author")){
	       //bookTmp.getAuthors().add(tmpValue);
	    }
	    if(element.equalsIgnoreCase("price")){
	        //bookTmp.setPrice(Integer.parseInt(tmpValue));
	    }
	    if(element.equalsIgnoreCase("regDate")){
	        try {
	            //bookTmp.setRegDate(sdf.parse(tmpValue));
	        } catch (Exception e) {
	            System.out.println("date parsing error");
	        }
	    }

    }
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
    	tmpValue = new String(ac, i, j);
    }
    
    public void setConnections(TestSuite testSuite, String env) {
    	String connections=propLoader.getValue("CONNECT_SYSTEM");
    	String connectArr[] = connections.split(",");
    	
    	for(int i=0; i<connectArr.length; i++) {
    		Connection connection = new Connection();
    		connection.setName(env+"_"+connectArr[i]);
    		connection.setUrl(propLoader.getValue(env+"_"+connectArr[i]+"_"+"URL"));
    		connection.setUsername(propLoader.getValue(env+"_"+connectArr[i]+"_"+"USERID"));
    		connection.setPassword(propLoader.getValue(env+"_"+connectArr[i]+"_"+"PASSWORD"));
    		connection.setDir(propLoader.getValue(env+"_"+connectArr[i]+"_"+"DIR"));
    		connection.setHostname(propLoader.getValue(env+"_"+connectArr[i]+"_"+"HOSTNAME"));
    		int defultPort = -1;
    		String sPort = propLoader.getValue(env+"_"+connectArr[i]+"_"+"PORT");
    		if(sPort != null && sPort.trim() != "") {
    			defultPort = Integer.parseInt(sPort.trim());
    		}
    		connection.setPort(defultPort);
    		testSuite.endSystems.put(env+"_"+connectArr[i], connection);
    	}
    }

}
