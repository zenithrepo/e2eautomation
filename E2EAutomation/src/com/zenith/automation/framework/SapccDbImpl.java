package com.zenith.automation.framework;

import com.zenith.automation.db.DbProcess;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.DBUtility;

public class SapccDbImpl implements ExecutionHandler {

	@Override
	public TestExecutorHandler process(TestExecutorHandler  testHandler) {
		if(testHandler instanceof TestAssert) {
			TestAssert testAssert = (TestAssert)testHandler;
			if(testAssert.getWaitTime() > 0) {
				Timer timer = new Timer(testAssert.getWaitTime());
				timer.pause();
			}
			Connection conn = TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_SAPCCDB");
			DBUtility dbUtil = new DBUtility(conn.getUrl(), conn.getUsername(),conn.getPassword());
			DbProcess dbProcess = new DbProcess();
			testHandler.expectedResultMap = dbProcess.convertAlToMap(testAssert.getAssertType());
			testHandler.actualResultMap = dbProcess.getMeterValue(dbUtil, testAssert.getIdentifier());
			System.out.println("=======>>>>Expected Result:"+testHandler.expectedResultMap);
			System.out.println("=======>>>>Actual Results:"+testHandler.actualResultMap);
		}else if(testHandler instanceof TestPrerequisities) {
			TestPrerequisities testPrerequisities = (TestPrerequisities)testHandler;
			if(testPrerequisities.getWaitTime() > 0) {
				Timer timer = new Timer(testPrerequisities.getWaitTime());
				timer.pause();
			}
			Connection conn = TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_SAPCCDB");
			DBUtility dbUtil = new DBUtility(conn.getUrl(), conn.getUsername(),conn.getPassword());
			DbProcess dbProcess = new DbProcess();
			testHandler.expectedResultMap = dbProcess.getMeterValue(dbUtil, testPrerequisities.getSub());
			System.out.println("=======>>>>Test Prerequisites:"+testHandler.expectedResultMap);

		}
		return testHandler;
	}

}
