package com.zenith.automation.framework;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.highdeal.cnd.message.StatefulServiceClient;
import com.highdeal.hci.Type;
import com.highdeal.pnr.hci.ChargeableItem;
import com.zenith.automation.highdeal.CIC;
import com.zenith.automation.highdeal.MaintainSubscriberUtil;
import com.zenith.automation.highdeal.SubscriberMaintainProcessor;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.PropLoader;

public class SubscriberMaintainanceImpl implements ExecutionHandler{
	
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	SubscriberMaintainProcessor processor = null;
	static {
		
	}
	@Override
	public TestExecutorHandler process(TestExecutorHandler executionHandler) {

		MaintainSubscriberUtil maintainSubUtil = MaintainSubscriberUtil.getMaintainSubscriberInstanse();
		
		if(executionHandler instanceof TestStep) {
			TestStep testStep = (TestStep)executionHandler;
			if(testStep.getWaitTime() > 0) {
				Timer timer = new Timer(testStep.getWaitTime());
				timer.pause();
			}
			Connection conn = TestSuite.getEndSystems().get(testStep.getEnvironment()+"_SAPCCDISPATCHER");
			StatefulServiceClient statefulClient = maintainSubUtil.getStatefulCleint();
			if(statefulClient == null) {
				maintainSubUtil.initializeConnection(conn.getHostname(), conn.getPort(), 300);
				statefulClient = maintainSubUtil.getStatefulCleint();
			}
			processor = new SubscriberMaintainProcessor();
			try {
				if(statefulClient != null) {
					String sAction = testStep.getAction();
					if( sAction.equalsIgnoreCase("RESET_ALL")) {//Reset individual  level and master level
						//1.) Reset individual Data level
						System.out.println("Reset data level counters");
						resetCounter("ALL_INDIVIDUAL_DATA_COUNTERS", "Data", testStep.getPhoneNumber());
						
						System.out.println("Reseting contract level counters");
						//2.) Reset individual Counter level
						//a. Call meter cic
						//b. Parse fields of Meter.only reset value 0 for key containing meter and value > 0.
						
						//resetCounter("ALL_INDIVIDUAL_CONTRACT_COUNTERS", "Contract", testStep.getPhoneNumber());
						ChargeableItem chargeableItem = processor.getReportChargableItem(3, 1);
						Map<String, Double> counterToReset = processor.getCurrentMeterValues(chargeableItem, "Contract", testStep.getPhoneNumber());
						resetCounter(counterToReset, "Contract", testStep.getPhoneNumber());
						
						//3. Reset ban level data counter
						System.out.println("Reseting master level data counters");
						resetCounter("ALL_MASTER_DATA_COUNTERS", "Data", testStep.getBan());

						//4. Reset ban level Contract counter
						System.out.println("Reseting contract level counters");
						//resetCounter("ALL_MASTER_CONTRACT_COUNTERS", "Contract", testStep.getBan());
						ChargeableItem banChargeableItem = processor.getReportChargableItem(3, 1);
						Map<String, Double> banCounterToReset = processor.getCurrentMeterValues(banChargeableItem, "Contract", testStep.getPhoneNumber());
						resetCounter(banCounterToReset, "Contract", testStep.getBan());

					}else if(sAction.equalsIgnoreCase("reset_individual")) {
						System.out.println("Resting counters at individual level");
						//1.) Reset individual Data level
						System.out.println("Reset data level counters");
						resetCounter("ALL_INDIVIDUAL_DATA_COUNTERS", "Data", testStep.getPhoneNumber());
						
						//2.) Reset individual Counter level
						System.out.println("Reseting contract level counters");
						//resetCounter("ALL_INDIVIDUAL_CONTRACT_COUNTERS", "Contract", testStep.getPhoneNumber());
						ChargeableItem chargeableItem = processor.getReportChargableItem(3, 1);
						Map<String, Double> counterToReset = processor.getCurrentMeterValues(chargeableItem, "Contract", testStep.getPhoneNumber());
						resetCounter(counterToReset, "Contract", testStep.getPhoneNumber());
						
					}else if(sAction.equalsIgnoreCase("reset_master")) {
						//3. Reset ban level data counter
						System.out.println("Reseting master level data counters");
						resetCounter("ALL_MASTER_DATA_COUNTERS", "Data", testStep.getBan());

						//4. Reset ban level Contract counter
						System.out.println("Reseting contract level counters");
						//resetCounter("ALL_MASTER_CONTRACT_COUNTERS", "Contract", testStep.getBan());
						ChargeableItem banChargeableItem = processor.getReportChargableItem(2, 1);
						Map<String, Double> banCounterToReset = processor.getCurrentMeterValues(banChargeableItem, "Contract", testStep.getBan());
						resetCounter(banCounterToReset, "Contract", testStep.getBan());
						
					}else if(sAction.equalsIgnoreCase("set_specific_counter")) {
						//Decide the counter shall be on mater level on subscriber level.
						//Update based on phone number or ban number
						System.out.println("Setting sepecific counter"+testStep.getValues());
						if(testStep.getValues() != null && testStep.getValues() != "") {
							Map<String, Object> countersMap = getAllCounter(testStep.getValues());
							Set<String> keys = countersMap.keySet();
							for(String key : keys) {
								Map<String, Long> eachCtrMap = new HashMap<String, Long>();
								Object _value = countersMap.get(key);
								eachCtrMap.put(key, Long.parseLong(_value.toString()));
								System.out.println("updating counter "+key+ ", value:"+_value);
								String serviceId = null, userServiceIdentifier = null;
								if(key.contains("Meter") || key.contains("Offer") || key.contains("LastRe")) {//Contract level
									serviceId = "Contract";
								}else if(key.contains("PPU") ) {//Data level
									serviceId = "Data";
								}else {
									System.out.println("Unhandled level indentification of filed type:"+key);
								}
								if(testStep.getPhoneNumber() != null) {
									userServiceIdentifier = testStep.getPhoneNumber();
								}else if(testStep.getBan() != null) {
									userServiceIdentifier = testStep.getBan();
								}else {
									System.out.println("User identifer missing");
								}
								resetLongCounter(eachCtrMap,serviceId, userServiceIdentifier );
							}
						}
					}else if(sAction.equalsIgnoreCase("unblock")) {
						if(testStep.getPhoneNumber() != null && testStep.getPhoneNumber() != "") {
							boolean status = processor.changeStatus(2, "Data", testStep.getPhoneNumber(), testStep.getType()) ;//Action 1= block, action 2 = unblock
							if(status) {
								System.out.println("Success : Subscriber : " +testStep.getPhoneNumber() + " is unblocked now" );
							}else {
								System.out.println("Failed : Failed to unblock Subscriber : " +testStep.getPhoneNumber());
							}
						}
						if(testStep.getBan() != null && testStep.getBan() != "") {
							boolean status = processor.changeStatus(2, "Data", testStep.getBan(),testStep.getType()) ;//Action 1= block, action 2 = unblock
							if(status) {
								System.out.println("Success : Subscriber : " +testStep.getBan() + " is unblocked now" );
							}else {
								System.out.println("Failed : Failed to unblock Subscriber : " +testStep.getBan());
							}
						}
					}else if(sAction.equalsIgnoreCase("block")) {
						boolean status = processor.changeStatus(1, "Data", testStep.getPhoneNumber(), testStep.getType()) ;//Action 1= block, action 2 = unblock
						if(status) {
							System.out.println("Success : Subscriber : " +testStep.getPhoneNumber() + " is blocked now" );
						}else {
							System.out.println("Failed : Failed to block Subscriber Subscriber : " +testStep.getPhoneNumber() );
						}
					}else{
						System.out.println("Unhandled action..."+sAction);
					}
				}else {
					System.out.println("Could not connect to SAP CC : "+conn.getHostname());
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else{
			System.out.println("Unhandled class"+this.getClass().getSimpleName());
		}
		return executionHandler;
	}
	
	public Map<String, Object> getAllCounter(String sAllCounters){
		HashMap<String, Object> initializedCtr = null;
		if(sAllCounters != null) {
			initializedCtr = new HashMap<String, Object>();
			String counters[] = sAllCounters.split(",");
			for(String eachCtr : counters) {
				String []keyValue = eachCtr.split("=");
				String _key = keyValue[0];
				Object _value= keyValue[1];
				initializedCtr.put(_key, _value);
			}
		}else{
			System.out.println("Counter not set in configuration.prop file");
		}
		return initializedCtr;
	}
	
	public void resetCounter(String propertyKey, String seriveIdentier, String userIdentfier) {
		Map<String, Object> mapCtrs = getAllCounter(propLoader.getValue(propertyKey));
		Set<String> keys = mapCtrs.keySet();
		for(String key : keys) {
			Object _value = mapCtrs.get(key);
			CIC cic = new CIC("Counter Update");
			ChargeableItem chargeableItem = cic.getChargeableItem();
			cic.addCicField(chargeableItem, "NAME", Type.STRING, key);
			cic.addCicField(chargeableItem, "VALUE", Type.NUMBER, _value);
			boolean statusFlag = processor.resetEachCounter(chargeableItem, seriveIdentier, userIdentfier);
			if(statusFlag) {
				System.out.println("Success : Counter:"+key+ " ,updated with :"+_value.toString());
			}else {
				System.out.println("Failed : Counter:"+key+ " ,updated failed");
			}
		}
	}
	
	public void resetCounter(Map<String, Double> counterMap, String seriveIdentier, String userIdentfier) {
		Set<String> keys = counterMap.keySet();
		for(String key : keys) {
			Object _value = counterMap.get(key);
			System.out.println("Key:"+key+", value"+_value);
			CIC cic = new CIC("Counter Update");
			ChargeableItem chargeableItem = cic.getChargeableItem();
			cic.addCicField(chargeableItem, "NAME", Type.STRING, key);
			cic.addCicField(chargeableItem, "VALUE", Type.NUMBER, 0);
			boolean statusFlag = processor.resetEachCounter(chargeableItem, seriveIdentier, userIdentfier);
			if(statusFlag) {
				System.out.println("Success : Counter:"+key+ " ,updated with :"+_value.toString());
			}else {
				System.out.println("Failed : Counter:"+key+ " ,updated failed");
			}
		}
	}

	public void resetLongCounter(Map<String, Long> counterMap, String seriveIdentier, String userIdentfier) {
		Set<String> keys = counterMap.keySet();
		for(String key : keys) {
			Object _value = counterMap.get(key);
			long lValue = Long.parseLong(_value.toString());
			System.out.println(_value);
			CIC cic = new CIC("Counter Update");
			ChargeableItem chargeableItem = cic.getChargeableItem();
			cic.addCicField(chargeableItem, "NAME", Type.STRING, key);
			cic.addCicField(chargeableItem, "VALUE", Type.NUMBER, lValue);
			boolean statusFlag = processor.resetEachCounter(chargeableItem, seriveIdentier, userIdentfier);
			if(statusFlag) {
				System.out.println("Success : Counter:"+key+ " ,updated with :"+lValue);
			}else {
				System.out.println("Failed : Counter:"+key+ " ,updated failed");
			}
		}
	}

}
