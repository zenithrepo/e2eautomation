package com.zenith.automation.framework;

import com.zenith.automation.batch.BatchCollection;
import com.zenith.automation.batch.TdrParser;
import com.zenith.automation.thread.Timer;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;

public class TdrImpl implements ExecutionHandler {

	@Override
	public TestExecutorHandler process(TestExecutorHandler  testHandler) {
		TestExecutorHandler testExecutorHandler = null;
		if(testHandler instanceof TestAssert) {
			TestAssert testAssert = (TestAssert)testHandler;
			String tmpDir = null;
			// Means session ID not set
			if(testAssert.getSessionId() == null) {
				testAssert.setSessionId(testHandler.getSessionId());
			}
			if(testAssert.getWaitTime() > 0) {
				Timer timer = new Timer(testAssert.getWaitTime());
				timer.pause();
			}
			try {
				PropLoader propLoader = PropLoader.getPropLoaderInstance();
				tmpDir = propLoader.getValue("TEST_RESULT");
				tmpDir = tmpDir +"\\"+ testHandler.getSuiteName()+"\\downloads\\tdr\\";
				CommonUtility.verifyDirectory(tmpDir);
				Connection conn = TestSuite.getEndSystems().get(testHandler.getEnvironment()+"_TDR");
				//get all tdr..processed 2 mins back
				BatchCollection batchCollection = new BatchCollection();
				batchCollection.copyFilesToLocal(conn.getUrl(), conn.getDir(), conn.getUsername(), conn.getPassword(),tmpDir);
			}catch(Exception e) {
				e.printStackTrace();
			}
			//accumulate the columns provided in assert type
			TdrParser csvParser = new TdrParser();
			testExecutorHandler = csvParser.getAccumulateValues(testAssert, tmpDir);
		}
		return testExecutorHandler;
	}

}
