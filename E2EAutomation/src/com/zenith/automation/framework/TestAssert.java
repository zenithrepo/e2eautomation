package com.zenith.automation.framework;

import java.util.ArrayList;

public class TestAssert extends TestExecutorHandler{
	String pkgClass;
	String identifier;
	int waitTime;
	String sessionId;
	String ban;
	ArrayList<String> assertType;
	
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}

	
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}


	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public ArrayList<String> getAssertType() {
		return assertType;
	}
	public void setAssertType(ArrayList<String> assertType) {
		this.assertType = assertType;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "TestAssert [pkgClass=" + pkgClass + ", identifier=" + identifier + ", waitTime=" + waitTime
				+ ", sessionId=" + sessionId + ", ban=" + ban + ", assertType=" + assertType + "]";
	}
	@Override
	public TestExecutorHandler execute(TestExecutorHandler teh) {
		return teh.invokeBL(teh);
	}

		
}
