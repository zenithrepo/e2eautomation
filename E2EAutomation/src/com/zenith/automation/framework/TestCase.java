package com.zenith.automation.framework;

import java.util.LinkedList;

public class TestCase {
	String name;
	String environment;
	int testCaseId;
	LinkedList<TestExecutorHandler> steps;
	//Below fields are for accessing global data across each test Step
	String _sessionId;
	
	
	public int getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getSessionId() {
		return _sessionId;
	}
	public void setSessionId(String sessionId) {
		this._sessionId = sessionId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public LinkedList<TestExecutorHandler> getSteps() {
		return steps;
	}
	public void setSteps(LinkedList<TestExecutorHandler> steps) {
		this.steps = steps;
	}
	public String getEnvironment() {
		return environment;
	}
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	@Override
	public String toString() {
		return "TestCase [name=" + name + ", environment=" + environment + ", testCaseId=" + testCaseId + ", steps="
				+ steps + ", _sessionId=" + _sessionId + "]";
	}
	

	

}
