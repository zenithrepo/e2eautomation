package com.zenith.automation.framework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.zenith.automation.notification.NotificationResult;

public abstract class TestExecutorHandler {
	public String sessionId;//Pass data across multiple Steps
	public String environment;
	public String suiteName;//Pass data across multiple Steps
	public String testCaseName;//Pass data across multiple Steps
	public String exceptionMessage;

	public HashMap<String, String> expectedResultMap = new HashMap<String, String>();
	public HashMap<String, String> actualResultMap = new HashMap<String, String>();
	public List<String> actualResultList = new ArrayList<String>();
	public List<String> expectedResultList = new ArrayList<String>();
	public List<NotificationResult> actualResultNotification = new ArrayList<NotificationResult>();
	public List<NotificationResult> expectedResultNotification = new ArrayList<NotificationResult>();
	
	public abstract TestExecutorHandler execute(TestExecutorHandler teh);
	
	public TestExecutorHandler invokeBL(TestExecutorHandler handler) {
		TestExecutorHandler testExecutorHandler = null;
		String className = null;
		if(handler instanceof TestPrerequisities) {
			TestPrerequisities testPrerequisities = (TestPrerequisities)handler;
			className = testPrerequisities.getPkgClass();
		}else if((handler instanceof TestStep)) {
			TestStep testStep = (TestStep)handler;
			className = testStep.getPkgClass();
		}else if((handler instanceof TestAssert)) {
			TestAssert testAssert = (TestAssert)handler;
			className = testAssert.getPkgClass();
		}else if((handler instanceof TestReport)) {
			TestReport testReport = (TestReport)handler;
		}else {
			System.out.println("Unhandled Class......"+this.getClass().getSimpleName());
		}
		try {
			Class cls = Class.forName(className);
			ExecutionHandler executionHandler = (ExecutionHandler)cls.newInstance();
			testExecutorHandler = executionHandler.process(handler);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return testExecutorHandler;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSuiteName() {
		return suiteName;
	}

	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	
}
