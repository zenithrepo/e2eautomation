package com.zenith.automation.framework;

public class TestPrerequisities extends TestExecutorHandler{
	String pkgClass;
	String type;
	String sub;
	String preCheckField;
	int waitTime;
	String ban;
	String phoneNumber;
	

	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getPreCheckField() {
		return preCheckField;
	}
	public void setPreCheckField(String preCheckField) {
		this.preCheckField = preCheckField;
	}

	
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Override
	public TestExecutorHandler execute(TestExecutorHandler teh) {
		return teh.invokeBL(teh);
	}
	

	@Override
	public String toString() {
		return "TestPrerequisities [pkgClass=" + pkgClass + ", type=" + type + ", sub=" + sub + ", preCheckField="
				+ preCheckField + ", waitTime=" + waitTime + ", ban=" + ban + ", phoneNumber=" + phoneNumber + "]";
	}

}
