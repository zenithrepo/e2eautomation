package com.zenith.automation.framework;

public class TestReport extends TestExecutorHandler{
	String fileToDir;
	String fileName;
	public String getFileToDir() {
		return fileToDir;
	}
	public void setFileToDir(String fileToDir) {
		this.fileToDir = fileToDir;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	@Override
	public String toString() {
		return "TestReport [fileToDir=" + fileToDir + ", fileName=" + fileName + "]";
	}
	@Override
	public TestExecutorHandler execute(TestExecutorHandler teh) {
		return teh.invokeBL(teh);
	}
	
	
}
