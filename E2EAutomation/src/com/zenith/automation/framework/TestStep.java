package com.zenith.automation.framework;


public class TestStep extends TestExecutorHandler{
	String pkgClass;
	String ban;
	String subscriberId;
	String phoneNumber;
	String sessionId;
	int mccMnc;
	String ratingGroup;
	long usage;
	String unit;
	int waitTime;
	String apn;
	String mzHost;
	String timestamp;
	String action;
	String multiSession;
	String values;
	String type;
	

	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValues() {
		return values;
	}
	public void setValues(String values) {
		this.values = values;
	}
	public String getMultiSession() {
		return multiSession;
	}
	public void setMultiSession(String multiSession) {
		this.multiSession = multiSession;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getApn() {
		return apn;
	}
	public void setApn(String apn) {
		this.apn = apn;
	}
	public String getMzHost() {
		return mzHost;
	}
	public void setMzHost(String mzHost) {
		this.mzHost = mzHost;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public long getUsage() {
		return usage;
	}
	public void setUsage(long usage) {
		this.usage = usage;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getMccMnc() {
		return mccMnc;
	}
	public void setMccMnc(int mccMnc) {
		this.mccMnc = mccMnc;
	}
	public String getRatingGroup() {
		return ratingGroup;
	}
	public void setRatingGroup(String ratingGroup) {
		this.ratingGroup = ratingGroup;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Override
	public TestExecutorHandler execute(TestExecutorHandler teh) {
		return teh.invokeBL(teh);
	}

	
}
