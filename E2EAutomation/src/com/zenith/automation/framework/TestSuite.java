package com.zenith.automation.framework;


import java.util.LinkedHashMap;

public class TestSuite {
	String testSuiteName;
	String env;
	int suiteId;
	static LinkedHashMap<String, Connection> endSystems;
	LinkedHashMap<String, TestCase> testCases;
	TestReport testReport;
	
	
	public int getSuiteId() {
		return suiteId;
	}
	public void setSuiteId(int suiteId) {
		this.suiteId = suiteId;
	}
	public String getTestSuiteName() {
		return testSuiteName;
	}
	public void setTestSuiteName(String testSuiteName) {
		this.testSuiteName = testSuiteName;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}

	public static LinkedHashMap<String, Connection> getEndSystems() {
		return endSystems;
	}
	public void setEndSystems(LinkedHashMap<String, Connection> endSystems) {
		this.endSystems = endSystems;
	}
	public LinkedHashMap<String, TestCase> getTestCases() {
		return testCases;
	}
	public void setTestCases(LinkedHashMap<String, TestCase> testCases) {
		this.testCases = testCases;
	}
	public TestReport getTestReport() {
		return testReport;
	}
	public void setTestReport(TestReport testReport) {
		this.testReport = testReport;
	}
	@Override
	public String toString() {
		return "TestSuite [testSuiteName=" + testSuiteName + ", env=" + env + ", suiteId=" + suiteId + ", testCases="
				+ testCases + ", testReport=" + testReport + "]";
	}
	

}


