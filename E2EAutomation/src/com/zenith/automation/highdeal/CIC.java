package com.zenith.automation.highdeal;

import java.math.BigDecimal;

import com.highdeal.hci.Type;
import com.highdeal.pnr.hci.ChargeableItem;
import com.highdeal.pnr.hci.Property;
import com.highdeal.util.scan.annotation.SysProp;

public class CIC {
	
	private String cicName;
	
	public CIC(String name) {
		this.cicName = name;
	}
	
	public ChargeableItem getChargeableItem() {
		ChargeableItem ci = new ChargeableItem(this.cicName);
		return ci;
	}
	
	public void addCicField(ChargeableItem cic, String name,int type, Object value) {
		cic.addProperty(getProperty(name, type, value));
	}
	
	public Property getProperty(String name,int type, Object value) {
		if(type == Type.NUMBER) {
			String sValue = value.toString();
			return new Property(name, type, new BigDecimal(Long.parseLong(sValue)));	
		}else  if(type == Type.STRING){
			return new Property(name, type, value.toString());
		}else {
			System.out.println("Unhadled type : "+type);
			return null;
		}
		
	}

}
