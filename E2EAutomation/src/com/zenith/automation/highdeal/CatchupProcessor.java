package com.zenith.automation.highdeal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.highdeal.hci.Type;
import com.highdeal.pnr.hci.ChargeableItem;
import com.highdeal.pnr.hci.PurchaseOrder;
import com.highdeal.pnr.tif.DetailRecord;
import com.zenith.automation.framework.TestStep;
import com.zenith.automation.http.HTTPHandler;

public class CatchupProcessor {
	MaintainSubscriberUtil subscriberUtil = null;
	
	public CatchupProcessor(){
		subscriberUtil = MaintainSubscriberUtil.getMaintainSubscriberInstanse();
	}
	
	
	public boolean catchupCharge(ChargeableItem ci, String serviceId, String userServiceId) {
		boolean resetFlag = false;
		PurchaseOrder purchaseOrder = subscriberUtil.checkCharge(ci, serviceId, userServiceId);
		if(purchaseOrder.getTransactions().size() > 0) {
			DetailRecord  detailRecord = purchaseOrder.getTransactions().get(0).getDetailRecord();
			if(detailRecord != null) {
				resetFlag = true;
			}
		}else {
			resetFlag = false;
		}
		return resetFlag;
	}
	
	public ChargeableItem getCatchupCic(TestStep  testStep) {
		HTTPHandler httpHandler = new HTTPHandler();
		Date eventTs = null;
		try {
			eventTs = new SimpleDateFormat("yyyyMMddHHmmss").parse(testStep.getTimestamp());
		} catch (ParseException e) {
			if(eventTs == null) {
				eventTs = new Date();
			}
			e.printStackTrace();
			
		}
		CIC cic = new CIC("Mobile Packet Data");
		ChargeableItem chargeableItem = cic.getChargeableItem();
		cic.addCicField(chargeableItem, "SGSN IP",  Type.STRING,  "155.23.56.144");
		cic.addCicField(chargeableItem, "GGSN IP",  Type.STRING, "124.250.12.1");
		cic.addCicField(chargeableItem, "RATING GROUP" ,  Type.NUMBER, testStep.getRatingGroup() );
		cic.addCicField(chargeableItem, "SERVICE ID" ,  Type.NUMBER, testStep.getPhoneNumber() );
		cic.addCicField(chargeableItem, "IMSI",  Type.STRING, "IMSI111" );
		cic.addCicField(chargeableItem, "IMEI",  Type.STRING, testStep.getSessionId());
		cic.addCicField(chargeableItem, "ACCESS POINT NAME",  Type.STRING, "APN" );
		cic.addCicField(chargeableItem, "MCC-MNC",  Type.STRING, testStep.getMccMnc() );
		cic.addCicField(chargeableItem, "TERMINATION CAUSE",  Type.STRING, "cause" );
		cic.addCicField(chargeableItem, "TRANSACTION TYPE" ,  Type.NUMBER, 3 );
		cic.addCicField(chargeableItem, "VOLUME" ,  Type.NUMBER, httpHandler.computeUsage(testStep));
		cic.addCicField(chargeableItem, "AMOUNT" ,  Type.NUMBER, 0);
		cic.addCicField(chargeableItem, "CHARGING CHARACTERISTICS" ,  Type.NUMBER, 56893433 );
		cic.addCicField(chargeableItem, "CHARGING ID" ,  Type.NUMBER, 986673 );
		cic.addCicField(chargeableItem, "PPU SESSION ID" ,  Type.NUMBER, 5678865);
		cic.addCicField(chargeableItem, "RAT" ,  Type.NUMBER, 6 );
		cic.addCicField(chargeableItem, "CONFIRMATION-DATE", Type.DATE, eventTs);
		cic.addCicField(chargeableItem, "RATING-RSN-CD" ,  Type.NUMBER, 2 );
		cic.addCicField(chargeableItem, "EVENT-NAME",  Type.STRING, "eventName" );
		cic.addCicField(chargeableItem, "EVENT-SOURCE",  Type.STRING, "source");
		cic.addCicField(chargeableItem, "EVENT-TYPE",  Type.STRING, "type" );
		cic.addCicField(chargeableItem, "FILE-NM-TXT",  Type.STRING, "dummyFilename");
		cic.addCicField(chargeableItem, "MARKET",  Type.STRING, "PCS" );
		cic.addCicField(chargeableItem, "MIN-CD",  Type.STRING, "M70312210" );
		cic.addCicField(chargeableItem, "SERVICE-LEVEL",  Type.STRING, "WISP1" );
		cic.addCicField(chargeableItem, "SERVICE-OPTION-CD",  Type.STRING, "SOCD");
		cic.addCicField(chargeableItem, "SERVING-SID",  Type.STRING, "SSID" );
		cic.addCicField(chargeableItem, "DEVICE-TYPE",  Type.STRING, "FPH" );
		cic.addCicField(chargeableItem, "SHARING-GROUPS-LIST", Type.STRING,  ";" );
		cic.addCicField(chargeableItem, "SYSTEM-DATE", Type.DATE, eventTs );
		cic.addCicField(chargeableItem, "LOCATION-INFO-PART-1",  Type.STRING, "LOC1" );
		cic.addCicField(chargeableItem, "LOCATION-INFO-PART-2",  Type.STRING, "LOC2" );
		cic.addCicField(chargeableItem, "SOURCE-ID",  Type.STRING, "SRC_ID" );
		cic.addCicField(chargeableItem, "PMN-ID",  Type.STRING, "PMN_ID" );
		cic.addCicField(chargeableItem, "LOC-CITY-NAME",  Type.STRING, "TORONTO");
		cic.addCicField(chargeableItem, "LOC-PROV-CD",  Type.STRING, "ON" );
		cic.addCicField(chargeableItem, "COUNTRY-CD",  Type.STRING, "CAN" );
		cic.addCicField(chargeableItem, "TIME-ZONE",  Type.STRING, "EST" );
		return chargeableItem;
		
	}


}
