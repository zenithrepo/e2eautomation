package com.zenith.automation.highdeal;

import java.util.Map;

public class CounterDAO {
	private String userServiceId;
	private String serviceId;
	private Map<String, Object> allFields;
	public String getUserServiceId() {
		return userServiceId;
	}
	public void setUserServiceId(String userServiceId) {
		this.userServiceId = userServiceId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public Map<String, Object> getAllFields() {
		return allFields;
	}
	public void setAllFields(Map<String, Object> mapAllFields) {
		this.allFields = mapAllFields;
	}
	@Override
	public String toString() {
		return "CounterDAO [userServiceId=" + userServiceId + ", serviceId=" + serviceId + ", allFields=" + allFields
				+ "]";
	}
	

}
