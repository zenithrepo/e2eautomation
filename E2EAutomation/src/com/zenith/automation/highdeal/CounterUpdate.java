package com.zenith.automation.highdeal;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;

import com.highdeal.cnd.message.CommunicationFailureException;
import com.highdeal.cnd.message.StatefulServiceClient;
import com.highdeal.hci.ServerFailureException;
import com.highdeal.hci.Type;
import com.highdeal.hci.XMLAttributes;
import com.highdeal.pnr.hci.ChargeableItem;
import com.highdeal.pnr.hci.ForbiddenChargeException;
import com.highdeal.pnr.hci.InvalidItemException;
import com.highdeal.pnr.hci.Property;
import com.highdeal.pnr.hci.PurchaseOrder;
import com.highdeal.pnr.hci.TransactionClearingException;
import com.highdeal.pnr.tif.DetailRecord;


public class CounterUpdate {
	//String subSsID = "4161536883";
	String subSsID = "A70814094";
	String service = "Contract"; //Action=3, Rat =1
	//String service = "Data";////Action=2, Rat =1
	public static void main(String ar[]) {
		CounterUpdate cUpdate = new CounterUpdate();
		
		statefulClient = getClient();

		try {
			System.out.println("Request sent to CC");
			PurchaseOrder po = statefulClient.charge(cUpdate.getReportChargableItem(2,1),cUpdate.service, cUpdate.subSsID, new Date(),StatefulServiceClient.ALL_TRANSACTION, false);
			//PurchaseOrder po = statefulClient.charge(cUpdate.getChargableItem(),cUpdate.service, cUpdate.subSsID, new Date(),StatefulServiceClient.ALL_TRANSACTION, false);
			
			//PurchaseOrder po = statefulClient.charge(cUpdate.getChargableItem() , "Data", cUpdate.subSsID, new Date(),StatefulServiceClient.ALL_TRANSACTION, false);
			DetailRecord  detail = po.getTransactions().get(0).getDetailRecord();
			System.out.println("Key Value");
			for(String s : detail.getDetails()) {
				System.out.println("Key : "+ s + " ,Value : "+ detail.getDetail(s));
			}
		} catch (InvalidItemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ForbiddenChargeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionClearingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServerFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			statefulClient.close();
		}
		
	}
	public ChargeableItem getChargableItem() {
		ChargeableItem ci =  new ChargeableItem();
		ci.setName("Counter Update");
		ci.addProperty(getProperty("NAME", Type.STRING, "LastRecurringDate"));
		ci.addProperty(getProperty("VALUE", Type.NUMBER, new BigDecimal(20180406060000l)));
		return ci;
	}

	public ChargeableItem getReportChargableItem(int action, int rat) {
		ChargeableItem ci =  new ChargeableItem();
		ci.setName("Report");
		ci.addProperty(getProperty("ACTION", Type.NUMBER, new BigDecimal(action)));
		ci.addProperty(getProperty("RAT", Type.NUMBER, new BigDecimal(rat)));
		return ci;
	}

	public Property getProperty(String name,int type, Object value) {
		return new Property(name, type, value);
	}
	
	public static StatefulServiceClient statefulClient = null;
	public static StatefulServiceClient  getClient() {
		if(statefulClient == null) {
			try {
				statefulClient = new StatefulServiceClient("vmsapcc21", 2100, 12000);
				System.out.println("Connectio  success");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Cannot connect to CC");
				e.printStackTrace();
			}
		}
		return statefulClient;
	}

}
