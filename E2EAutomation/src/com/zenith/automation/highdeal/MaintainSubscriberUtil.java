package com.zenith.automation.highdeal;

import java.io.IOException;
import java.util.Date;

import com.highdeal.cnd.message.CommunicationFailureException;
import com.highdeal.cnd.message.StatefulServiceClient;
import com.highdeal.hci.ServerFailureException;
import com.highdeal.pnr.hci.ChargeableItem;
import com.highdeal.pnr.hci.ForbiddenChargeException;
import com.highdeal.pnr.hci.InvalidItemException;
import com.highdeal.pnr.hci.PurchaseOrder;
import com.highdeal.pnr.hci.TransactionClearingException;
import com.highdeal.pnr.tif.DetailRecord;
import com.zenith.automation.utility.PropLoader;

public class MaintainSubscriberUtil {
	
	private static StatefulServiceClient statefulClient = null;
	private static MaintainSubscriberUtil maintainSubscriber = null;
	
	
	private MaintainSubscriberUtil() {
		
	}
	
	public static MaintainSubscriberUtil getMaintainSubscriberInstanse() {
		if(maintainSubscriber == null) {
			maintainSubscriber = new MaintainSubscriberUtil();
		}
		return maintainSubscriber;
	}
	
	
	public  void  initializeConnection(String hostname , int port, int timeout) {
		if(statefulClient == null) {
			try {
				statefulClient = new StatefulServiceClient(hostname, port, timeout);
				System.out.println("Connected to  Hostname : "+hostname +" ,port : "+port);
			} catch (IOException e) {
				System.out.println("Failed to connect host:"+hostname+" on port:"+port);
				e.printStackTrace();
			}
		}
	}
	
	public StatefulServiceClient getStatefulCleint() {
		return statefulClient;
	}
	
	public void closeConnection() {
		if(statefulClient != null) {
			statefulClient.close();
		}
	}
	
	public PurchaseOrder checkCharge(ChargeableItem ci , String serviceId, String userServiceId) {
		StatefulServiceClient statefulServiceClient = MaintainSubscriberUtil.getMaintainSubscriberInstanse().getStatefulCleint();
		PurchaseOrder po = null;
				
		try {
			po = statefulServiceClient.charge(ci, serviceId, userServiceId, new Date(), StatefulServiceClient.ALL_TRANSACTION, false);
			/*
			DetailRecord  detail = po.getTransactions().get(0).getDetailRecord();
			for(String s : detail.getDetails()) {
				System.out.println("Key : "+ s + " ,Value : "+ detail.getDetail(s));
			}
			*/
		} catch (InvalidItemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ForbiddenChargeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionClearingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServerFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CommunicationFailureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return po;
	}


}
