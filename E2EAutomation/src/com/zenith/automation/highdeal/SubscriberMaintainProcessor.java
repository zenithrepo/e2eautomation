package com.zenith.automation.highdeal;

import java.math.BigDecimal;

import java.util.*;

import com.highdeal.cnd.message.StatefulServiceClient;
import com.highdeal.hci.Type;
import com.highdeal.pnr.hci.ChargeableItem;
import com.highdeal.pnr.hci.Property;
import com.highdeal.pnr.hci.PurchaseOrder;
import com.highdeal.pnr.tif.DetailRecord;

public class SubscriberMaintainProcessor {

	MaintainSubscriberUtil subscriberUtil = null;
	
	public SubscriberMaintainProcessor(){
		subscriberUtil = MaintainSubscriberUtil.getMaintainSubscriberInstanse();
	}
	
	
	public Map<String,Double> getCurrentMeterValues(ChargeableItem ci, String serviceId, String userServiceId){
		Map<String,Double> counters = null;
		PurchaseOrder purchaseOrder = subscriberUtil.checkCharge(ci, serviceId, userServiceId);
		DetailRecord  detailRecord = purchaseOrder.getTransactions().get(0).getDetailRecord();
		if(detailRecord.size() > 0) {
			counters =  new HashMap<String, Double>();
		}
		for(String sKey : detailRecord.getDetails()) {
			
			Object value = detailRecord.getDetail(sKey);
			double dValue = -1d;
			System.out.println("Counter list counter :"+sKey + ",Value="+value);
			if(sKey.contains("counter.Meter") ) {
				if(value.toString() != null) {
					dValue = Double.parseDouble(value.toString());
				}else {
					System.out.println("Failed to cast value \""+value.toString()+ "\" to Double");
				}
				String actualKey = (sKey.split("\\."))[1];
				if(dValue > 0) {
					counters.put(actualKey, 0d);
					System.out.println("Added counter :"+actualKey + ",Value="+dValue);
				}
			}
		}
		return counters;
	}
	
	//Will return true is counter is reset, false if not reset.
	public boolean resetEachCounter(ChargeableItem ci, String serviceId, String userServiceId) {
		boolean resetFlag = false;
		PurchaseOrder purchaseOrder = subscriberUtil.checkCharge(ci, serviceId, userServiceId);
		if(purchaseOrder.getTransactions().size() > 0) {
			DetailRecord  detailRecord = purchaseOrder.getTransactions().get(0).getDetailRecord();
			System.out.println(detailRecord.getDetails().toString());
			if(detailRecord != null) {
				resetFlag = true;
			}
		}else {
			resetFlag = false;
		}
		return resetFlag;
	}
	
	public boolean changeStatus(int blockUnblock, String serviceId, String userServiceId, String unblockType) {
		CIC cic = new CIC("Data Blocking");
		ChargeableItem chargeableItem = cic.getChargeableItem();
		cic.addCicField(chargeableItem, "ACTION", Type.NUMBER, blockUnblock);
		cic.addCicField(chargeableItem, "TYPE", Type.STRING, unblockType);
		cic.addCicField(chargeableItem, "LEVEL", Type.STRING, "ALL");
		cic.addCicField(chargeableItem, "APPLICATION SOURCE", Type.STRING, "Data");
		cic.addCicField(chargeableItem, "ZONE", Type.STRING, "CAN");
		cic.addCicField(chargeableItem, "REQUESTOR ID", Type.STRING, "");
		cic.addCicField(chargeableItem, "VERIFICATION REQUIRED", Type.STRING, "N");

		boolean resetFlag = false;
		PurchaseOrder purchaseOrder = subscriberUtil.checkCharge(chargeableItem, serviceId, userServiceId);
		if(purchaseOrder.getTransactions().size() > 0) {
			DetailRecord  detailRecord = purchaseOrder.getTransactions().get(0).getDetailRecord();
			if(detailRecord != null) {
				resetFlag = true;
			}
		}else {
			resetFlag = false;
		}
		return resetFlag;
	}
		
	public CounterDAO parseDetail(DetailRecord dr) {
		CounterDAO counterDao =  new CounterDAO();
		Map<String, Object> _rtCounter = new HashMap<String, Object>();
		for(String key : dr.getDetails()) {
			System.out.println("Key :"+key+" ,Value:"+dr.getDetail(key).toString());
			_rtCounter.put(key, dr.getDetail(key));
		}
		counterDao.setAllFields(_rtCounter);
		return counterDao;
	}
	
	

	public Property getProperty(String name,int type, Object value) {
		return new Property(name, type, value);
	}
	
	public ChargeableItem getReportChargableItem(int action, int rat) {
		ChargeableItem ci =  new ChargeableItem();
		ci.setName("Report");
		ci.addProperty(getProperty("ACTION", Type.NUMBER, new BigDecimal(action)));
		ci.addProperty(getProperty("RAT", Type.NUMBER, new BigDecimal(rat)));
		return ci;
	}
	
}
