package com.zenith.automation.http;
import java.io.BufferedReader;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import com.jcraft.jsch.jce.Random;
import com.zenith.automation.framework.Connection;
import com.zenith.automation.framework.Constants;
import com.zenith.automation.framework.ReadXML;
import com.zenith.automation.framework.TestExecutorHandler;
import com.zenith.automation.framework.TestStep;
import com.zenith.automation.utility.CommonUtility;

public class HTTPHandler {

	private final String USER_AGENT = "Mozilla/5.0";
	CommonUtility commUtil  = new CommonUtility();
	public boolean hitOnlineMZ(TestExecutorHandler handler, Connection connection)  {
		TestStep testStep = null;
		if(handler instanceof TestStep) {
			testStep = (TestStep)handler;
		}
		
		boolean didHit = false;
		Constants.MZ_HIT_TS = new Date();//commUtil.converDateToUTC(null);

		RequestGenerator rg = new RequestGenerator();
		
		//First send usage for Start session
		//based on the response send the usage for Update session or stop session
		long totalUsage = computeUsage(testStep), lPartialUsage = 0,iReqNo = 0, totalReqSent = 0;
		if(testStep.getMultiSession() != null &&  testStep.getMultiSession().equalsIgnoreCase("TRUE")) {//two simultaneous  sessions.. one after other in parallel.
			UserInputParameters uipSession1 = new UserInputParameters();
			UserInputParameters uipSession2 = new UserInputParameters();
			Date d1 = uipSession2.getObjCreateTs();
			uipSession2.setObjCreateTs(new Date(uipSession2.getObjCreateTs().getTime() + 5000));// added 5 secs to change the session id of both sessions
			uipSession1 = uipSession1.getDefaultValues(testStep);
			uipSession2 = uipSession2.getDefaultValues(testStep);
			if(testStep.getSessionId() == null) {// Session id  from test step shall be blank
				testStep.setSessionId(uipSession1.getSESSION_ID()+ "|" + uipSession2.getSESSION_ID());//Storing both the session id on session by "|" pipe separated
			}
			long lPartialUsage1 = 0, lPartialUsage2 = 0;
			String req1 = rg.updatePerRequestParams(uipSession1, testStep, connection, (int)++iReqNo, Constants.START_SESSION_REQ, 0l, 1);
			String req2 = rg.updatePerRequestParams(uipSession2, testStep, connection, (int)++iReqNo, Constants.START_SESSION_REQ, 0l, 1);
			handler.setSessionId(testStep.getSessionId());
			
			//Send both start session req.
			String response1 = sendGet(req1);//Start send for session 1
			String response2 = sendGet(req2);//Start send for session 2
			lPartialUsage1 = getValueFromResponse(response1, "CC_Total_Octets");//-1
			lPartialUsage2 = getValueFromResponse(response2, "CC_Total_Octets");//-1
			do {
				
				if((totalReqSent % 2) == 0) {
					
				}else {
					
				}
				totalReqSent++;
			}while(totalUsage > 0);
			

			
		}else {//only one session
			UserInputParameters uips = new UserInputParameters();
			uips = uips.getDefaultValues(testStep);
			if(testStep.getSessionId() == null) {
				testStep.setSessionId(uips.getSESSION_ID());
			}
			String req = rg.updatePerRequestParams(uips, testStep, connection, (int)++iReqNo, Constants.START_SESSION_REQ, 0l, 1);
			handler.setSessionId(testStep.getSessionId());
			do {
				totalUsage = totalUsage - lPartialUsage;//501
				String response = sendGet(req);
				//System.out.println("MZ Response==>"+response);
				didHit = true;totalReqSent++;
				lPartialUsage = getValueFromResponse(response, "CC_Total_Octets");//-1
				if(totalUsage <= lPartialUsage || lPartialUsage <= 0) {//send Stop
					req = rg.updatePerRequestParams(uips, testStep, connection, (int)++iReqNo, Constants.STOP_SESSION_REQ, totalUsage, 0);
					if(lPartialUsage == -1) {
						System.out.println("No GSU(CC_Octets) granted from CC. Sending stop");
						totalUsage = 0;
					}
				}else {//send update
					req = rg.updatePerRequestParams(uips, testStep, connection, (int)++iReqNo, Constants.UPDATE_SESSION_REQ, lPartialUsage, 1);
				}
			}while(totalUsage > 0);
			handler.actualResultList = new ArrayList<String>();
			handler.actualResultList.add(totalReqSent+" request send");
			System.out.println("=======>>>> "+totalReqSent+" requests sent to MZ");
			
		}
		return didHit;
	}

	// HTTP GET request
	private String sendGet(String url)  {
		StringBuffer response = null;
		URL obj;
		BufferedReader in = null;
		try {
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("User-Agent", USER_AGENT);
			int responseCode = con.getResponseCode();
			in = new BufferedReader(new InputStreamReader(con.getInputStream()));

			String inputLine;
			response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			//System.out.println("<<<<<<"+response);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// optional default is GET
		
		return response.toString();
	}

	// HTTP POST request
	private void sendPost() throws Exception {

		String url = "";
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		//System.out.println(response.toString());

	}
	
	//Get GSU from response. If no GSU is received. return -1 and set stop session request to MZ
	// response : Response from CC
	// field form which we have to extract GSU
	public long getValueFromResponse(String response, String field) {
		long ccOctets = -1;
		int fieldIdx = response.indexOf(field);
		int fieldLength = field.length();
		//CC_Total_Octets: 2314240          
		//Ignoring field length, field : and field space
		if(fieldIdx != -1) {
			String fieldValue = response.substring(fieldIdx + fieldLength + 1, fieldIdx+fieldLength+11);
			try {
				fieldValue = fieldValue.trim();
				ccOctets = Long.parseLong(fieldValue);
			}catch(NumberFormatException e) {
				System.out.println(response);
				System.out.println("Failed to typecast field:"+field+" with value:"+ fieldValue+", (Derived value).");
				e.printStackTrace();
			}catch(Exception e) {
				System.out.println(response);
				System.out.println("Unhandled exception");
				e.printStackTrace();
			}
		}else {
			System.out.println("Field "+field+"  is missing in response");
			System.out.println(response);
		}
		return ccOctets;
	}
	
	//Always returns in bytes... if no unit specifies... bytes will be considered
	public long computeUsage(TestStep testStep) {
		long usage = testStep.getUsage();
		String usageUnit = testStep.getUnit();
		if(usageUnit != null){
			if(usageUnit.equalsIgnoreCase("B")) {
				usage = testStep.getUsage();
			}else if(usageUnit.equalsIgnoreCase("KB")) {
				usage = usage * 1024;
			}else if(usageUnit.equalsIgnoreCase("MB")) {
				usage = usage * 1024 * 1024;
			}else if(usageUnit.equalsIgnoreCase("GB")) {
				usage = usage * 1024 * 1024 * 1024;
			}else if(usageUnit.equalsIgnoreCase("TB")) {
				usage = usage * 1024 * 1024 * 1024 * 1024;
			}else {
				System.out.println("Test step : usage unit undefined.");
			}
		}
		return usage;
	}

}