package com.zenith.automation.http;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.zenith.automation.framework.Connection;
import com.zenith.automation.framework.Constants;
import com.zenith.automation.framework.TestStep;

public class RequestGenerator {
	RequestParameters rp = RequestParameters.getRequestParameters();

	public String updatePerRequestParams(UserInputParameters uips, TestStep testStep, Connection connection,int requestNumber,int requestType, long usage, int mscc_rsu) {
		String request = null;
		uips.setREQUEST_NUMBER(""+requestNumber);
		uips.setREQUEST_TYPE(""+requestType);
		uips.setMSCC_USU(""+usage);
		uips.setMSCC_RSU(""+mscc_rsu);
		Date currDt = uips.getObjCreateTs();
		currDt.setTime(currDt.getTime()  + 1000);
		uips.setHOUR(""+currDt.getHours());
		uips.setMINUTE(""+currDt.getMinutes());
		uips.setSECOND(""+currDt.getSeconds());
		uips.setObjCreateTs(currDt);
		uips.setSRHDATE(new SimpleDateFormat("MM/dd/yyyy").format(currDt));
		request = connection.getUrl()+(uips.getHttpQuery(rp).toString());
		return request;
	}
	
	
	@Deprecated
	public ArrayList<String> sendUsage(Map<Integer, Integer> usagePerReq, TestStep testStep, Connection connection) {
		ArrayList<String> allReqestUrl = new ArrayList<String>(); 
		UserInputParameters uips = new UserInputParameters();
		uips = uips.getDefaultValues(testStep);
		uips.setSESSION_ID(testStep.getSessionId());
		uips.setIMEISV(testStep.getSessionId());
		uips.setMSISDN(testStep.getSubscriberId());
		int totalRequest = usagePerReq.size(),iCtr = 1;
		while(totalRequest >= iCtr) {
			//uips.setPerRequestParams(uips);
			int usageToBeSend = usagePerReq.get(iCtr);
			String request = null;
			uips.setREQUEST_NUMBER(""+iCtr);
			if(iCtr == 1) {//start session request
				uips.setREQUEST_TYPE("1");
				uips.setMSCC_USU("0");
			}else if(iCtr == totalRequest) {//Stop session request
				uips.setREQUEST_TYPE("3");
				uips.setMSCC_USU(""+usageToBeSend);
			}else {//Update session request
				uips.setREQUEST_TYPE("2");
				uips.setMSCC_USU(""+usageToBeSend);
			}
			request = connection.getUrl()+(uips.getHttpQuery(rp).toString());
			allReqestUrl.add(request);
			iCtr++;
		}
		return allReqestUrl;
	}
	
	

	/* Number of request shall be inclusion of start, update and stop
	 * Minimum 2 request is required.
	 */
	public Map<Integer, Integer> getEachUSU(int totalUsageToBeSent, int numberOfReq){
		Map<Integer, Integer> usagePerReq = new HashMap();
		int kbPerReq = totalUsageToBeSent/(numberOfReq-1), iCtr = 1;
		
		while(numberOfReq  >= iCtr) {
			if(iCtr == 1) {//Start Session request
				usagePerReq.put(iCtr, 0);
			}else if(iCtr == numberOfReq){ // Last Request
				usagePerReq.put(iCtr,  kbPerReq + (totalUsageToBeSent -  kbPerReq*(iCtr-1)) );
			}else { // Update request
				usagePerReq.put(iCtr, kbPerReq);
			}
			
			iCtr++;
		}
		return usagePerReq;
	}
	
	/* Number of request shall be inclusion of start, update and stop
	 * One parameter required, total usage to be sent.
	 * Max usu 20MB, to be send per hit
	 */
	
	public Map<Integer, Integer> getEachUSU(long totalUsageToBeSent, String usageType){
		long MAX_USU;
		if(usageType != null && usageType.equalsIgnoreCase("domestic")) {
			MAX_USU = Constants.MAX_BYTE_DOMESTIC;
		}else {
			MAX_USU = Constants.MAX_BYTE_ROAMING;
		}
		int iCtr = 2;
		Map<Integer, Integer> usagePerReq = new HashMap();
		usagePerReq.put(1, 0);
		while(totalUsageToBeSent  > 0) {
			if(totalUsageToBeSent >= MAX_USU) {
				usagePerReq.put(iCtr, (int)MAX_USU);
			}else {
				usagePerReq.put(iCtr, (int)totalUsageToBeSent);
			}
			totalUsageToBeSent = totalUsageToBeSent - MAX_USU;
			iCtr++;
		}
		return usagePerReq;
	}


}
