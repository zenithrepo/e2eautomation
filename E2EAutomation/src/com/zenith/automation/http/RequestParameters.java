package com.zenith.automation.http;

public class RequestParameters {
			
			private final String SESSION_ID = "session_id";
			private final String RAT = "rat";
			private final String TGPP_CHARGING_ID = "tgpp_charging_id";
			private final String TGPP_CHARGING_CHARACTERISTICS = "tgpp_charging_characteristics";
			private final String CALLED_STATION_ID = "called_station_id";
			private final String TGPP_SGSN_MCC_MNC = "tgpp_sgsn_mcc_mnc";
			private final String MSISDN = "msisdn";
			private final String IMSI = "imsi";
			private final String TGPP_LAC = "tgpp_lac";
			private final String MSCC_RG = "mscc_rg";
			private final String MSCC_RSU = "mscc_rsu";
			private final String TGPP_SAC = "tgpp_sac";
			private final String SRHDATE = "srhdate";
			private final String HOUR = "hour";
			private final String MINUTE = "minute";
			private final String SECOND = "second";
			private final String MSCC_USU = "mscc_usu";
			private final String MSCC_USE = "mscc_use";
			private final String MSCC_REPORTING_REASON = "mscc_reporting_reason";
			private final String REQUEST_NUMBER = "request_number";
			private final String TGPP_GGSN_ADDRESS = "tgpp_ggsn_address";
			private final String TGPP_SGSN_ADDRESS = "tgpp_sgsn_address";
			private final String USER_NAME = "user_name";
			private final String TGPP_CG_ADDRESS = "tgpp_cg_address";
			private final String SERVICE_CONTEXT_ID = "service_context_id";
			private final String REQUEST_TYPE = "request_type";
			private final String TGPP_GPRS_NEG_QOS = "tgpp_gprs_neg_qos";
			private final String IMEISV = "imeisv";
			private final String MZ = "mz";
			private final String SEPERATOR_AMPS = "&";
			private final String SEPERATOR_EQUALS = "=";
			
			public String getSESSION_ID() {
				return SESSION_ID;
			}
			public String getRAT() {
				return RAT;
			}
			public String getTGPP_CHARGING_ID() {
				return TGPP_CHARGING_ID;
			}
			public String getTGPP_CHARGING_CHARACTERISTICS() {
				return TGPP_CHARGING_CHARACTERISTICS;
			}
			public String getCALLED_STATION_ID() {
				return CALLED_STATION_ID;
			}
			public String getTGPP_SGSN_MCC_MNC() {
				return TGPP_SGSN_MCC_MNC;
			}
			public String getMSISDN() {
				return MSISDN;
			}
			public String getIMSI() {
				return IMSI;
			}
			public String getTGPP_LAC() {
				return TGPP_LAC;
			}
			public String getMSCC_RG() {
				return MSCC_RG;
			}
			public String getMSCC_RSU() {
				return MSCC_RSU;
			}
			public String getTGPP_SAC() {
				return TGPP_SAC;
			}
			public String getSRHDATE() {
				return SRHDATE;
			}
			public String getHOUR() {
				return HOUR;
			}
			public String getMINUTE() {
				return MINUTE;
			}
			public String getSECOND() {
				return SECOND;
			}
			public String getMSCC_USU() {
				return MSCC_USU;
			}
			public String getMSCC_USE() {
				return MSCC_USE;
			}
			public String getMSCC_REPORTING_REASON() {
				return MSCC_REPORTING_REASON;
			}
			public String getREQUEST_NUMBER() {
				return REQUEST_NUMBER;
			}
			public String getTGPP_GGSN_ADDRESS() {
				return TGPP_GGSN_ADDRESS;
			}
			public String getTGPP_SGSN_ADDRESS() {
				return TGPP_SGSN_ADDRESS;
			}
			public String getUSER_NAME() {
				return USER_NAME;
			}
			public String getTGPP_CG_ADDRESS() {
				return TGPP_CG_ADDRESS;
			}
			public String getSERVICE_CONTEXT_ID() {
				return SERVICE_CONTEXT_ID;
			}
			public String getREQUEST_TYPE() {
				return REQUEST_TYPE;
			}
			public String getTGPP_GPRS_NEG_QOS() {
				return TGPP_GPRS_NEG_QOS;
			}
			public String getIMEISV() {
				return IMEISV;
			}
			public String getMZ() {
				return MZ;
			}
			public String getSEPERATOR_AMPS() {
				return SEPERATOR_AMPS;
			}
			public String getSEPERATOR_EQUALS() {
				return SEPERATOR_EQUALS;
			}
			private RequestParameters() {
	
			}
			private static RequestParameters requestParameters = new RequestParameters();
			public static RequestParameters getRequestParameters() {
				return requestParameters;
			}
}
