package com.zenith.automation.http;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.print.attribute.standard.DateTimeAtCompleted;

import org.apache.poi.ss.usermodel.DateUtil;

import com.zenith.automation.framework.TestStep;


public class UserInputParameters {
	private String 	SESSION_ID;
	private String 	RAT;
	private String 	TGPP_CHARGING_ID;
	private String 	TGPP_CHARGING_CHARACTERISTICS;
	private String 	CALLED_STATION_ID;
	private String 	TGPP_SGSN_MCC_MNC;
	private String 	MSISDN;
	private String 	IMSI;
	private String 	TGPP_LAC;
	private String 	MSCC_RG;
	private String 	MSCC_RSU;
	private String 	TGPP_SAC;
	private String 	SRHDATE;
	private String 	HOUR;
	private String 	MINUTE;
	private String 	SECOND;
	private String 	MSCC_USU;
	private String 	MSCC_USE;
	private String 	MSCC_REPORTING_REASON;
	private String 	REQUEST_NUMBER;
	private String 	TGPP_GGSN_ADDRESS;
	private String 	TGPP_SGSN_ADDRESS;
	private String 	USER_NAME;
	private String 	TGPP_CG_ADDRESS;
	private String 	SERVICE_CONTEXT_ID;
	private String 	REQUEST_TYPE;
	private String 	TGPP_GPRS_NEG_QOS;
	private String 	IMEISV;
	private String 	MZ;
	private Date objCreateTs;
	public UserInputParameters() {
		this.objCreateTs = new Date();
	}
	public String getSESSION_ID() {
		return SESSION_ID;
	}
	public void setSESSION_ID(String sESSION_ID) {
		SESSION_ID = sESSION_ID;
	}
	public String getRAT() {
		return RAT;
	}
	public void setRAT(String rAT) {
		RAT = rAT;
	}
	public String getTGPP_CHARGING_ID() {
		return TGPP_CHARGING_ID;
	}
	public void setTGPP_CHARGING_ID(String tGPP_CHARGING_ID) {
		TGPP_CHARGING_ID = tGPP_CHARGING_ID;
	}
	public String getTGPP_CHARGING_CHARACTERISTICS() {
		return TGPP_CHARGING_CHARACTERISTICS;
	}
	public void setTGPP_CHARGING_CHARACTERISTICS(String tGPP_CHARGING_CHARACTERISTICS) {
		TGPP_CHARGING_CHARACTERISTICS = tGPP_CHARGING_CHARACTERISTICS;
	}
	public String getCALLED_STATION_ID() {
		return CALLED_STATION_ID;
	}
	public void setCALLED_STATION_ID(String cALLED_STATION_ID) {
		CALLED_STATION_ID = cALLED_STATION_ID;
	}
	public String getTGPP_SGSN_MCC_MNC() {
		return TGPP_SGSN_MCC_MNC;
	}
	public void setTGPP_SGSN_MCC_MNC(String tGPP_SGSN_MCC_MNC) {
		TGPP_SGSN_MCC_MNC = tGPP_SGSN_MCC_MNC;
	}
	public String getMSISDN() {
		return MSISDN;
	}
	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}
	public String getIMSI() {
		return IMSI;
	}
	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}
	public String getTGPP_LAC() {
		return TGPP_LAC;
	}
	public void setTGPP_LAC(String tGPP_LAC) {
		TGPP_LAC = tGPP_LAC;
	}
	public String getMSCC_RG() {
		return MSCC_RG;
	}
	public void setMSCC_RG(String mSCC_RG) {
		MSCC_RG = mSCC_RG;
	}
	public String getMSCC_RSU() {
		return MSCC_RSU;
	}
	public void setMSCC_RSU(String mSCC_RSU) {
		MSCC_RSU = mSCC_RSU;
	}
	public String getTGPP_SAC() {
		return TGPP_SAC;
	}
	public void setTGPP_SAC(String tGPP_SAC) {
		TGPP_SAC = tGPP_SAC;
	}
	public String getSRHDATE() {
		return SRHDATE;
	}
	public void setSRHDATE(String sRHDATE) {
		SRHDATE = sRHDATE;
	}
	public String getHOUR() {
		return HOUR;
	}
	public void setHOUR(String hOUR) {
		HOUR = hOUR;
	}
	public String getMINUTE() {
		return MINUTE;
	}
	public void setMINUTE(String mINUTE) {
		MINUTE = mINUTE;
	}
	public String getSECOND() {
		return SECOND;
	}
	public void setSECOND(String sECOND) {
		SECOND = sECOND;
	}
	public String getMSCC_USU() {
		return MSCC_USU;
	}
	public void setMSCC_USU(String mSCC_USU) {
		MSCC_USU = mSCC_USU;
	}
	public String getMSCC_USE() {
		return MSCC_USE;
	}
	public void setMSCC_USE(String mSCC_USE) {
		MSCC_USE = mSCC_USE;
	}
	public String getMSCC_REPORTING_REASON() {
		return MSCC_REPORTING_REASON;
	}
	public void setMSCC_REPORTING_REASON(String mSCC_REPORTING_REASON) {
		MSCC_REPORTING_REASON = mSCC_REPORTING_REASON;
	}
	public String getREQUEST_NUMBER() {
		return REQUEST_NUMBER;
	}
	public void setREQUEST_NUMBER(String rEQUEST_NUMBER) {
		REQUEST_NUMBER = rEQUEST_NUMBER;
	}
	public String getTGPP_GGSN_ADDRESS() {
		return TGPP_GGSN_ADDRESS;
	}
	public void setTGPP_GGSN_ADDRESS(String tGPP_GGSN_ADDRESS) {
		TGPP_GGSN_ADDRESS = tGPP_GGSN_ADDRESS;
	}
	public String getTGPP_SGSN_ADDRESS() {
		return TGPP_SGSN_ADDRESS;
	}
	public void setTGPP_SGSN_ADDRESS(String tGPP_SGSN_ADDRESS) {
		TGPP_SGSN_ADDRESS = tGPP_SGSN_ADDRESS;
	}
	public String getUSER_NAME() {
		return USER_NAME;
	}
	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}
	public String getTGPP_CG_ADDRESS() {
		return TGPP_CG_ADDRESS;
	}
	public void setTGPP_CG_ADDRESS(String tGPP_CG_ADDRESS) {
		TGPP_CG_ADDRESS = tGPP_CG_ADDRESS;
	}
	public String getSERVICE_CONTEXT_ID() {
		return SERVICE_CONTEXT_ID;
	}
	public void setSERVICE_CONTEXT_ID(String sERVICE_CONTEXT_ID) {
		SERVICE_CONTEXT_ID = sERVICE_CONTEXT_ID;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getTGPP_GPRS_NEG_QOS() {
		return TGPP_GPRS_NEG_QOS;
	}
	public void setTGPP_GPRS_NEG_QOS(String tGPP_GPRS_NEG_QOS) {
		TGPP_GPRS_NEG_QOS = tGPP_GPRS_NEG_QOS;
	}
	public String getIMEISV() {
		return IMEISV;
	}
	public void setIMEISV(String iMEISV) {
		IMEISV = iMEISV;
	}
	public String getMZ() {
		return MZ;
	}
	public void setMZ(String mZ) {
		MZ = mZ;
	}

	public Date getObjCreateTs() {
		return objCreateTs;
	}
	public void setObjCreateTs(Date objCreateTs) {
		this.objCreateTs = objCreateTs;
	}
	public StringBuilder getHttpQuery(RequestParameters rp) {
		StringBuilder query = new StringBuilder();
		
		query.append(rp.getSESSION_ID());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getSESSION_ID());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getRAT());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getRAT());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getTGPP_CHARGING_ID());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_CHARGING_ID());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getTGPP_CHARGING_CHARACTERISTICS());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_CHARGING_CHARACTERISTICS());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getCALLED_STATION_ID());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getCALLED_STATION_ID());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getTGPP_SGSN_MCC_MNC());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_SGSN_MCC_MNC());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getMSISDN());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSISDN());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getIMSI());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getIMSI());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getTGPP_LAC());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_LAC());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getMSCC_RG());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSCC_RG());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getMSCC_RSU());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSCC_RSU());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getTGPP_SAC());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_SAC());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getSRHDATE());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getSRHDATE());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getHOUR());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getHOUR());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getMINUTE());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMINUTE());
		query.append(rp.getSEPERATOR_AMPS());
		
		
		query.append(rp.getSECOND());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getSECOND());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getMSCC_USU());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSCC_USU());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getMSCC_USE());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSCC_USE());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getMSCC_REPORTING_REASON());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMSCC_REPORTING_REASON());
		query.append(rp.getSEPERATOR_AMPS());
		
		query.append(rp.getREQUEST_NUMBER());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getREQUEST_NUMBER());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getTGPP_GGSN_ADDRESS());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_GGSN_ADDRESS());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getTGPP_SGSN_ADDRESS());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_SGSN_ADDRESS());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getUSER_NAME());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getUSER_NAME());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getTGPP_CG_ADDRESS());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_CG_ADDRESS());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getSERVICE_CONTEXT_ID());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getSERVICE_CONTEXT_ID());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getREQUEST_TYPE());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getREQUEST_TYPE());
		query.append(rp.getSEPERATOR_AMPS());

		query.append(rp.getTGPP_GPRS_NEG_QOS());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getTGPP_GPRS_NEG_QOS());
		query.append(rp.getSEPERATOR_AMPS());


		query.append(rp.getIMEISV());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getIMEISV());
		query.append(rp.getSEPERATOR_AMPS());


		query.append(rp.getMZ());
		query.append(rp.getSEPERATOR_EQUALS());
		query.append(this.getMZ());


		return query;
	}
	
	//Set all the default values
	public UserInputParameters getDefaultValues(TestStep testStep) {
		UserInputParameters uip = new UserInputParameters();
		Date currDt = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdSessionId = new SimpleDateFormat("ddMMyyhhmmssSSS");
		uip.setObjCreateTs(currDt);
		uip.setRAT("1");
		uip.setTGPP_CHARGING_ID("1535819776");
		uip.setTGPP_CHARGING_CHARACTERISTICS("8");

		uip.setIMSI("302221000005157");
		uip.setTGPP_LAC("");
		uip.setMSCC_RSU("1");
		uip.setTGPP_SAC("");
		String sDate = sdf.format(currDt);

		uip.setMSCC_USU("0");
		uip.setMSCC_USE("1");
		uip.setMSCC_REPORTING_REASON("1");
		uip.setTGPP_GGSN_ADDRESS("209.29.244.115");
		uip.setTGPP_SGSN_ADDRESS("209.29.244.132");
		uip.setUSER_NAME("username");
		uip.setTGPP_CG_ADDRESS("1.1.1.1");
		uip.setSERVICE_CONTEXT_ID("Telus@3gpp.org");
		uip.setTGPP_GPRS_NEG_QOS("99-1B931F739668B07483FFFF");
		if(testStep.getTimestamp() != null) {
			try {
				Date inputTs = new SimpleDateFormat("yyyyMMddHHmmss").parse(testStep.getTimestamp());
				uip.setObjCreateTs(inputTs);
				uip.setSRHDATE(sdf.format(inputTs));
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else {
			uip.setSRHDATE(sDate);
		}
		if(testStep.getSubscriberId() != null) {
			uip.setMSISDN(testStep.getSubscriberId());
		}else {
			uip.setMSISDN("16472137510");
		}

		if(testStep.getRatingGroup() != null && testStep.getRatingGroup() != "") {
			uip.setMSCC_RG(testStep.getRatingGroup());
		}else {
			uip.setMSCC_RG("10103");
		}
		if(testStep.getMccMnc() != -1 ) {
			uip.setTGPP_SGSN_MCC_MNC(""+testStep.getMccMnc());
		}else {
			uip.setTGPP_SGSN_MCC_MNC("302221");
		}

		if(testStep.getSessionId() != null) {
			uip.setSESSION_ID(testStep.getSessionId());
			uip.setIMEISV(testStep.getSessionId());
		}else {
			String customSessionId = testStep.getTestCaseName()+"_sid_"+sdSessionId.format(objCreateTs); 
			uip.setSESSION_ID(customSessionId);
			uip.setIMEISV(customSessionId);
		}
		if(testStep.getApn() != null && testStep.getApn() != "") {
			uip.setCALLED_STATION_ID(testStep.getApn());
		}else {
			uip.setCALLED_STATION_ID("sp.telus.com");
		}
		if(testStep.getMzHost() != null) {
			String mzHostTmp = testStep.getMzHost();
			if(mzHostTmp.equalsIgnoreCase("POST_MZ1")) {
				uip.setMZ("srbhmzpost1pgw");
			}else if(mzHostTmp.equalsIgnoreCase("POST_MZ2")) {
				uip.setMZ("srbhmzpost2pgw");
			}else if(mzHostTmp.equalsIgnoreCase("POST_MZ3")) {
				uip.setMZ("srbhmzpost3pgw");
			}else if(mzHostTmp.equalsIgnoreCase("POST_MZ4")) {
				uip.setMZ("srbhmzpost4pgw");
			}else if(mzHostTmp.equalsIgnoreCase("POST_MZ5")) {
				uip.setMZ("srbhmzpost5pgw");
			}else if(mzHostTmp.equalsIgnoreCase("PRE_MZ1")) {
				uip.setMZ("srbhmz1pgw");
			}else if(mzHostTmp.equalsIgnoreCase("PRE_MZ2")) {
				uip.setMZ("srbhmz2pgw");
			}else if(mzHostTmp.equalsIgnoreCase("PRE_MZ3")) {
				uip.setMZ("srbhmz3pgw");
			}else if(mzHostTmp.equalsIgnoreCase("PRE_MZ4")) {
				uip.setMZ("srbhmz4pgw");
			}else if(mzHostTmp.equalsIgnoreCase("PRE_VM6")) {
				uip.setMZ("pspremz3pgw");
			}else if(mzHostTmp.equalsIgnoreCase("POST_VM6")) {
				uip.setMZ("pspostmz3pgw");
			}else {
				uip.setMZ("srbhmzpost1pgw");
			}
		}else {
			uip.setMZ("srbhmzpost1pgw");
		}
		return uip;
		
	}
}
