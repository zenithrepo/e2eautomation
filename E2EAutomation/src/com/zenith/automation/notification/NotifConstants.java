package com.zenith.automation.notification;

public class NotifConstants {
	public static String NOTIF_QUERY="select e.BILLING_ACCOUNT_NUM, o.DEST_ADDR_NUM, e.CNTCT_EFFECTIVE_START_TS,H.TRANSACTION_DEFINITION_CD , o.SMS_PROCESS_STAT_CD, o.MSG_CONTENT_TXT " + 
			" from OUTBND_SMS_CUSTOMER_NOTIF o," + 
			" CNTCT_EVENT e," + 
			" CNTCT_EVENT_HEADER h " + 
			" where e.BILLING_ACCOUNT_NUM = ? " + 
			" and o.CNTCT_EVENT_ID = e.CNTCT_EVENT_ID " + 
			" and e.CNTCT_EVENT_HEADER_ID = h.CNTCT_EVENT_HEADER_ID " + 
			" and O.CNTCT_EFFECTIVE_START_TS > ?";
}
