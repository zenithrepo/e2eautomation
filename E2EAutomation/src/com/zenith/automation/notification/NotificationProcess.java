package com.zenith.automation.notification;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.zenith.automation.framework.Constants;
import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.utility.DBUtility;

public class NotificationProcess {
	public List<NotificationResult> getNotification(DBUtility dbUtil, TestAssert testAssert){
		ArrayList<NotificationResult> al = null;
		Connection conn = dbUtil.getConnection();
		ResultSet rs = null;
		try {
			PreparedStatement prepStmt = conn.prepareStatement(NotifConstants.NOTIF_QUERY);
			prepStmt.setString(1, testAssert.getBan());
			prepStmt.setDate(2, this.getNotifDate());
			rs= prepStmt.executeQuery();
			if(rs != null) {
				al = new ArrayList<NotificationResult>();
			}
			ResultSetMetaData data = rs.getMetaData();
			while(rs.next()) {
				NotificationResult notifResult = new NotificationResult();
				notifResult.setBan(rs.getString(1));
				notifResult.setDestinationAddress(rs.getString(2));
				notifResult.setEffectiveStartDate(rs.getDate(3));
				notifResult.setTransactionDefinitionCode(rs.getString(4));
				notifResult.setSmsProcessStatusCode(rs.getString(5));
				notifResult.setMsgContent(rs.getString(6));
				al.add(notifResult);
			}
		} catch (SQLException e) {
			System.out.println("Database error....");
			e.printStackTrace();
		}catch(Exception e) {
			System.out.println("Unable to fetch record..");
			e.printStackTrace();
		}
		
		dbUtil.getConnection();
		return al;
	}
	
	public Date getNotifDate() {
		long lHitTs = 0;
		if(Constants.MZ_HIT_TS != null) {
			lHitTs = (Constants.MZ_HIT_TS.getTime() );
		}else {
			lHitTs = (new java.util.Date().getTime() - Constants.GET_OLD_NOTIFICATION);
		}
		java.sql.Date sqlDate = new java.sql.Date( lHitTs );
		return sqlDate;
	}

}
