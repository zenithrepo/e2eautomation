package com.zenith.automation.notification;

import java.util.Date;

public class NotificationResult {
	String ban;
	String destinationAddress;
	String transactionDefinitionCode;
	String smsProcessStatusCode;
	String msgContent;
	Date effectiveStartDate;
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getDestinationAddress() {
		return destinationAddress;
	}
	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}
	public String getTransactionDefinitionCode() {
		return transactionDefinitionCode;
	}
	public void setTransactionDefinitionCode(String transactionDefinitionCode) {
		this.transactionDefinitionCode = transactionDefinitionCode;
	}
	public String getSmsProcessStatusCode() {
		return smsProcessStatusCode;
	}
	public void setSmsProcessStatusCode(String smsProcessStatusCode) {
		this.smsProcessStatusCode = smsProcessStatusCode;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public Date getEffectiveStartDate() {
		return effectiveStartDate;
	}
	public void setEffectiveStartDate(Date effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}
	@Override
	public String toString() {
		return "NotificationResult [ban=" + ban + ", destinationAddress=" + destinationAddress
				+ ", transactionDefinitionCode=" + transactionDefinitionCode + ", smsProcessStatusCode="
				+ smsProcessStatusCode + ", msgContent=" + msgContent + ", effectiveStartDate=" + effectiveStartDate
				+ "]";
	}
	
	
	
}
