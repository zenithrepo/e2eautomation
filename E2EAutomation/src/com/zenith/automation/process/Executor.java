package com.zenith.automation.process;

import java.util.Date;
import java.util.HashMap;


import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;


import com.zenith.automation.framework.ReadXML;
import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.framework.TestCase;
import com.zenith.automation.framework.TestExecutorHandler;
import com.zenith.automation.framework.TestPrerequisities;
import com.zenith.automation.framework.TestStep;
import com.zenith.automation.framework.TestSuite;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.reporting.ObjectToXMLParser;
import com.zenith.reporting.ResultDAO;

public class Executor {
	/*
	 * 1)Decode the XML and get the decoded object., if not dont go to next step
	 * 2)Check All connection properties are reachable and able to connect. If not dont go to next step
	 * 3)Take first test case.
	 * 4)Execute the test step one by one.
	 * 5)Once all test step are executed. then execute test assert one by one
	 * 6)Go back to test step 3 until all test cases are completed.
	 *  
	 * */
	static CommonUtility commonUtil = null;
	
	public static void main(String[] args) {
		Executor exe = new Executor();
		ObjectToXMLParser xmlParser = new ObjectToXMLParser();
		commonUtil = new CommonUtility();
		//Step 1.
		ReadXML readXML = new ReadXML();
		TestSuite testSuite = readXML.getDecodedXML();
		System.out.println("Test Suite:"+testSuite);
		String envSuiteLevel = null, envTestCaseLevel = null;
		//Step 2.
		/*
		 * Logic to checking all the connections comes here
		 * */
		
		//Step 3
		//Execute each testcase, one by one(including prerequisites, execution and validation)
		HashMap<String, TestCase> testCases = testSuite.getTestCases();
		Iterator it = testCases.entrySet().iterator();
		envSuiteLevel = testSuite.getEnv();
		while(it.hasNext()) {
			Map.Entry<String, TestCase> entry =  (Entry<String, TestCase>) it.next();
			String tcName = entry.getKey();
			TestCase testCase = entry.getValue();
			envTestCaseLevel = testCase.getEnvironment();
			String sessionId = null;
			for(TestExecutorHandler teh : testCase.getSteps()) {
				if(sessionId != null) {
					teh.setSessionId(sessionId);
				}
				try {
					System.out.println("************ Results of "+teh.getClass().getSimpleName()+" begins ************");
					if(envTestCaseLevel != null && envTestCaseLevel != "") {
						teh.setEnvironment(envTestCaseLevel);
					}else if(envSuiteLevel != null && envSuiteLevel != "") {
						teh.setEnvironment(envSuiteLevel);
					}else {
						System.out.println("environment not set..");
					}
					Date stepStartTs = new Date();
					teh.setSuiteName(testSuite.getTestSuiteName());
					teh.setTestCaseName(testCase.getName());
					teh = teh.execute(teh);
					ResultDAO resultDao = exe.compareResult(teh,testSuite.getTestSuiteName(),tcName, stepStartTs);
					xmlParser.updateResult(resultDao);
					System.out.println("************ Results of "+teh.getClass().getSimpleName()+" ends **************\n");
					//For passing the session ID accorss muliple steps
					if(teh.getSessionId() != null) {
						sessionId = teh.getSessionId();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}// Step 6 : go to step 3, if required
		xmlParser.generateXml(null);
	}
	
	public ResultDAO compareResult(TestExecutorHandler handler, String suiteName, String testCaseName, Date startTs ) {
		ResultDAO result = new ResultDAO();
		if(handler instanceof TestPrerequisities) {
			TestPrerequisities testPrerequisities = (TestPrerequisities)handler;
			result.setTestStep(testPrerequisities.getPkgClass());
		}else if((handler instanceof TestStep)) {
			TestStep testStep = (TestStep)handler;
			result.setTestStep(testStep.getPkgClass());
		}else if((handler instanceof TestAssert)) {
			TestAssert testAssert = (TestAssert)handler;
			result.setTestStep(testAssert.getPkgClass());
		}else {
			System.out.println("new class.."+handler.getClass().getName());
		}
		result.setTestSuite(suiteName);
		result.setTestCase(testCaseName);
		String tcName = result.getTestStep();
		tcName = tcName.substring(tcName.lastIndexOf(".")+1, tcName.length()-4);
		result.setTestStep(tcName.toUpperCase());
		if(handler.actualResultList != null) {
			result.setActualResult(handler.actualResultList);
		}
		if(handler.expectedResultList != null) {
			result.setExpectedResult(handler.expectedResultList);
		}
		if(handler.expectedResultList != null && handler.actualResultList != null) {
			
		}
		if(handler.actualResultMap != null) {
			result.setActualResult(handler.actualResultMap);
		}
		if(handler.expectedResultMap != null) {
			result.setExpectedResult(handler.expectedResultMap);
		}
		result.setTestStartTime(startTs);
		result.setTestEndTime(new Date());
		String executionTs = commonUtil.normailzeExTime(result.getTestEndTime().getTime() - result.getTestStartTime().getTime());
		result.setExecutionTime(executionTs);
		System.out.println("Actual Result:"+result.getActualResult());
		System.out.println("Excepted Result:"+result.getExpectedResult());
		result.setStatus((""+result.getActualResult().toString().equals(result.getExpectedResult().toString())).toUpperCase());
		return result;
	} 
	
	public void mapFromTestStepToTestCase(TestStep testS) {
		
	}
}
