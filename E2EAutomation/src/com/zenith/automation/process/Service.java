package com.zenith.automation.process;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import com.zenith.automation.framework.ReadXML;
import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.framework.TestCase;
import com.zenith.automation.framework.TestExecutorHandler;
import com.zenith.automation.framework.TestPrerequisities;
import com.zenith.automation.framework.TestStep;
import com.zenith.automation.framework.TestSuite;
import com.zenith.automation.highdeal.MaintainSubscriberUtil;
import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;
import com.zenith.reporting.ObjectToXMLParser;
import com.zenith.reporting.ResultDAO;

public class Service {
	static CommonUtility commonUtil = null;
	static ObjectToXMLParser xmlParser = null;
	static PropLoader  propLoader = null;
	public Service() {
		this.commonUtil = new CommonUtility();
		this.xmlParser = new ObjectToXMLParser();
	}
	
	public static void main(String args[]) {
		String configPath = null;
		if(args != null && args.length > 0) {
			System.out.println("taking args");
			configPath = args[0];
		}
		PropLoader.loadProp(configPath);
		propLoader = PropLoader.getPropLoaderInstance();
		Service srvc = new Service();
		srvc.trigger();
	}
	
	
	public void trigger() {
		ReadXML readXML = new ReadXML();
		TestSuite testSuite = readXML.getDecodedXML();

		executeTestSuite(testSuite);
		xmlParser.generateXml(propLoader.getValue("TEST_RESULT_XML_OUTPUT"));
	}
	
	public void executeTestSuite(TestSuite testSuite) {
		LinkedHashMap<String, TestCase> allTCs = testSuite.getTestCases();
		executeTestCasesMap(allTCs, testSuite.getTestSuiteName(), testSuite.getSuiteId(), testSuite.getEnv());
		
	}
	public void executeTestCasesMap(LinkedHashMap<String, TestCase> testCases, String suiteName, int suiteId, String env) {
		Set<String> tcs = testCases.keySet();
		for(String testCase : tcs) {
			executeTestCase(testCases.get(testCase), suiteName, suiteId, env);
		}
	}
	
	public void executeTestCase(TestCase testCase, String suiteName, int suiteId, String env) {
		int exeSeqNo = 0;
		if(suiteName == null || suiteName == "") {
			suiteName = "CustomSuite"+System.currentTimeMillis()/100;
		}
		
		if(testCase.getEnvironment() != null) {
			env = testCase.getEnvironment();
		}
		for(TestExecutorHandler teh : testCase.getSteps()) {
			exeSeqNo++;
			Date stepStartTs = new Date();
			try {
				System.out.println("************ Results of "+teh.getClass().getSimpleName()+" begins ************");
				transferTestCaseValueToTestHandler(teh, testCase);
				teh.setSuiteName(suiteName);
				teh = teh.execute(teh);
				tranferHandlerValueToTestCase(testCase, teh);
			} catch (Exception e) {
				e.printStackTrace();
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				teh.setExceptionMessage(sw.toString());
			}finally {
				ResultDAO resultDao = compareResult(teh,suiteName,testCase.getName(), stepStartTs, suiteId, testCase.getTestCaseId(), exeSeqNo, env);
				xmlParser.updateResult(resultDao);
				System.out.println("************ Results of "+teh.getClass().getSimpleName()+" ends **************\n");
				
			}
		}
		//Close all connections
		MaintainSubscriberUtil maintainSubscriber = MaintainSubscriberUtil.getMaintainSubscriberInstanse();
		maintainSubscriber.closeConnection();
	}
	
	//Usable if we have to transfer values from Test Handler to Test Case
	public void tranferHandlerValueToTestCase(TestCase testCase, TestExecutorHandler handler) {
		if( handler.getSessionId() != null) {
			testCase.setSessionId(handler.getSessionId());
		}
	}
	
	public void transferTestCaseValueToTestHandler(TestExecutorHandler handler, TestCase testCase) {
		handler.setEnvironment(testCase.getEnvironment());
		if(testCase.getSessionId() !=  null) {
			handler.setSessionId(testCase.getSessionId());
		}
		handler.setTestCaseName(testCase.getName());
	}
	
	public ResultDAO compareResult(TestExecutorHandler handler, String suiteName, String testCaseName, Date startTs, int suiteId, int testCaseId, int exeSeqNum, String env ) {
		ResultDAO result = new ResultDAO();
		if(handler instanceof TestPrerequisities) {
			TestPrerequisities testPrerequisities = (TestPrerequisities)handler;
			result.setTestStep(testPrerequisities.getPkgClass());
		}else if((handler instanceof TestStep)) {
			TestStep testStep = (TestStep)handler;
			result.setTestStep(testStep.getPkgClass());
		}else if((handler instanceof TestAssert)) {
			TestAssert testAssert = (TestAssert)handler;
			result.setTestStep(testAssert.getPkgClass());
		}else {
			System.out.println("new class.."+handler.getClass().getName());
		}
		result.setEnv(env);
		result.setTestSuite(suiteName);
		result.setTestCase(testCaseName);
		result.setTestSuiteId(suiteId);
		result.setTestCaseId(testCaseId);
		result.setExecutionSeqNo(exeSeqNum);
		String tcName = result.getTestStep();
		tcName = tcName.substring(tcName.lastIndexOf(".")+1, tcName.length()-4);
		result.setTestStep(tcName.toUpperCase());
		if(handler.actualResultList != null && handler.actualResultList.size() > 0 ) {
			result.setActualResult(handler.actualResultList.toString());
			result.setSummary(handler.actualResultList.toString());
		}
		if(handler.expectedResultList != null) {
			result.setExpectedResult(handler.expectedResultList);
		}
		if(handler.expectedResultList != null && handler.actualResultList != null) {
			
		}
		if(handler.actualResultMap != null) {
			result.setActualResult(handler.actualResultMap);
		}
		if(handler.expectedResultMap != null) {
			result.setExpectedResult(handler.expectedResultMap);
		}
		result.setTestStartTime(startTs);
		result.setTestEndTime(new Date());
		String executionTs = commonUtil.normailzeExTime(result.getTestEndTime().getTime() - result.getTestStartTime().getTime());
		result.setExecutionTime(executionTs);
		if(result.getActualResult().toString().equals(result.getExpectedResult().toString())) {
			result.setStatus("SUCCESS");
			result.setSummary("Expected result "+result.getExpectedResult().toString()+ ", and actual result is "+result.getActualResult().toString());
		}else {
			result.setStatus("FAILED");
			result.setSummary("Expected result "+result.getExpectedResult().toString()+ ", but actual result is "+result.getActualResult().toString());
		}
		if(handler.getExceptionMessage() != null) {
			result.setSummary(handler.getExceptionMessage());
		}

		return result;
	}
}
