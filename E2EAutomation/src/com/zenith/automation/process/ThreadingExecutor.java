package com.zenith.automation.process;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.zenith.automation.framework.ReadXML;
import com.zenith.automation.framework.TestCase;
import com.zenith.automation.framework.TestSuite;

public class ThreadingExecutor {
	private final int PARALLEL_THREADS = 3;
	
	
	public static void main(String args[]) {
		ThreadingExecutor executor = new ThreadingExecutor();
		ReadXML readXML = new ReadXML();
		TestSuite testSuite = readXML.getDecodedXML();
		
		Service srvc = new Service();
		ArrayList<LinkedHashMap<String, TestCase>> splittedTc = executor.splitInMultipleTC(testSuite.getTestCases());
		for(LinkedHashMap<String, TestCase> linkedHashMap : splittedTc) {
			new ThreadingService(srvc, linkedHashMap, testSuite.getTestSuiteName()).run();
		}
		srvc.xmlParser.generateXml(null);
	}
	
	public ArrayList<LinkedHashMap<String, TestCase>> splitInMultipleTC(LinkedHashMap<String, TestCase> testCases){
		int iNoOfTc = testCases.size();
		ArrayList<LinkedHashMap<String, TestCase>> alTcs = new ArrayList<LinkedHashMap<String, TestCase>>();
		if(iNoOfTc <= PARALLEL_THREADS) {//One to one thread per test case
			for(String key : testCases.keySet()) {
				LinkedHashMap<String, TestCase> splittedMapTc = new LinkedHashMap<String, TestCase>();
				splittedMapTc.put(key, testCases.get(key));
				alTcs.add(splittedMapTc);
			}
		}else {//divide and split it
			int additionalTc = iNoOfTc % PARALLEL_THREADS, iCtr = 0;
			int tcPerThread[] = new int[PARALLEL_THREADS];
			for(int i = 0 ; i< PARALLEL_THREADS ; i++) {// Compute number of TC per thread
				tcPerThread[i] = iNoOfTc / PARALLEL_THREADS;
				if(i < additionalTc) {
					tcPerThread[i] = tcPerThread[i] + 1;
				}
			}
			int threadSize = tcPerThread.length;
			for(int thread = 0; thread< threadSize; thread++) {
				int addCtr =0;
				int tcToBeAddedPerThread = tcPerThread[thread];
				LinkedHashMap<String, TestCase> splittedMapTc = new LinkedHashMap<String, TestCase>();
				Object []keys = testCases.keySet().toArray();
				do {
					String key = keys[addCtr].toString();
					splittedMapTc.put(key, testCases.get(key));
					testCases.remove(key);
					addCtr++;
				}while(tcToBeAddedPerThread > addCtr);
				alTcs.add(splittedMapTc);
			}
		}
		return alTcs;
	}

}
