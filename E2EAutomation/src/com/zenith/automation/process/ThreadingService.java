package com.zenith.automation.process;

import java.util.*;

import com.zenith.automation.framework.TestCase;

public class ThreadingService extends Thread{

	
	String suiteName = null;
	Service service = null;
	LinkedHashMap<String, TestCase> testCases = null;
	
	
	public ThreadingService(Service service, LinkedHashMap<String, TestCase> testCaseSet, String suiteName) {
		this.service = service;
		this.testCases = testCaseSet;
		this.suiteName = suiteName;
	}
	
	public void run() {
		service.executeTestCasesMap(testCases, suiteName, -1, null);
	}

	

}
