package com.zenith.automation.thread;

import com.zenith.automation.framework.TestAssert;

public class Timer {
	public int sleep;
	public Timer(int sleep) {
		this.sleep = sleep * 1000;//Seconds into millis
		
	}
	public boolean pause() {
		try {
			System.out.println("Waiting for "+this.sleep+ " millis.....");
			Thread.sleep(this.sleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
}
