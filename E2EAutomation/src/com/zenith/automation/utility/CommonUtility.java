package com.zenith.automation.utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.zenith.automation.framework.TestCase;

public class CommonUtility {
	//Exit code = 9, abort immediately

	public static void exit(int exitCode, String message) {
		if(exitCode == 9) {
			System.out.println("Aborting execution:"+message);
			System.exit(0);
		}
	}

	public static boolean verifyDirectory(String absPath) {
		boolean result = false;
		File file = new File(absPath);
		if(file.exists() && file.isDirectory()) {
			result = true;
		}else {
			result = file.mkdirs();
		}
		return result;
	}

	public static boolean isDirectoryPresent(String absPath) {
		boolean result = false;
		File file = new File(absPath);
		if(file.exists() && file.isDirectory()) {
			result = true;
		}else {
			result = false;
		}
		return result;
	}
	
	public static boolean createDir(String absPath) {
		boolean result = false;
		File f = new File(absPath);
		result = f.mkdir();
		return result;
	}
	
	public Date converDateToUTC(Date sysDate) {
		Date dConvertedDt = null;
		if(sysDate == null) {
			dConvertedDt = new Date();
		}else {
			dConvertedDt = sysDate;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			String sUtc = dateFormat.format(dConvertedDt);
			dConvertedDt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(sUtc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dConvertedDt;
	}
	//returun execution time on readable format in expected format.
	// Parameter expected in milliseconds
	public String normailzeExTime(long executionTime) {
		String time = null;
		double diff = 0;
		if(executionTime < 1000) {//Millis
			diff = executionTime;
			time = executionTime + " millisecond";
		}else if(executionTime < 60000) { // seconds
			diff = executionTime/1000.0;
			diff = Math.round(diff*100.0)/100.0;
			time = diff + " second";
		}else if(executionTime < 3600000) { // Mins
			diff = executionTime/60000.0;
			diff = Math.round(diff*100.0)/100.0;
			time = diff + " minute";
		}else if(executionTime < 86400000) {// Hours
			diff = executionTime/3600000.0f;
			diff = Math.round(diff*100.0)/100.0;
			time = diff + " hour";
		}else {//days
			diff = executionTime/86400000.0f;
			diff = Math.round(diff*100.0)/100.0;
			time = diff + " day";
		}
		if(diff > 1.0) {
			time = time + "s";
		}
		return time;
	}

}
