package com.zenith.automation.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtility {
	
	private Connection conn = null;
	public DBUtility(String dbUrl, String username, String password) {
		conn = this.getDBConnection(dbUrl, username, password);
	}
	private Connection getDBConnection(String dbUrl, String username, String password) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(dbUrl,username,password);
		} catch (SQLException e) {
			System.out.println("SQL Exception while conneting to database");
			e.printStackTrace();
		}catch(Exception e) {
			System.out.println("Unhadled Exception while connection to database");
			e.printStackTrace();
		}
		return conn;
	}
	public void closeConnection() {
		 if(this.conn != null) {
			 try {
				this.conn.close();
			} catch (SQLException e) {
				System.out.println("Unable to close the database connection");
				e.printStackTrace();
			}
		 }
	 }
	 public Connection getConnection() {
		 return this.conn;
	 }

}
