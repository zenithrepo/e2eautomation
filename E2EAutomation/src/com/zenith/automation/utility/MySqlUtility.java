package com.zenith.automation.utility;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class MySqlUtility {

	private String DB_DRIVER = null;
	private String DB_CONNECTION = null;
	private String DB_USER = null;
	private String DB_PASSWORD = null;
	
	public MySqlUtility() {
		PropLoader propLoader = PropLoader.getPropLoaderInstance();
		this.DB_DRIVER = propLoader.getValue("REPORTING_DRIVER");
		this.DB_CONNECTION = propLoader.getValue("REPORTING_DB_HOST");
		this.DB_USER = propLoader.getValue("REPORTING_DB_USERNAME");
		this.DB_PASSWORD = propLoader.getValue("REPORTING_DB_USERNAME");
	}

	public  Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			dbConnection = DriverManager.getConnection(this.DB_CONNECTION, this.DB_USER, this.DB_PASSWORD);  
			Class.forName(this.DB_DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dbConnection;
	}

}
