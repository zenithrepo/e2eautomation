package com.zenith.automation.utility;

import java.io.FileInputStream;

import java.io.InputStream;
import java.util.Properties;

public class PropLoader {
	
	private static PropLoader propLoader = null;
	private static String configPath = null;
	private Properties prop;
	
	private PropLoader() {
		this.initializeProperties(configPath);
	}

	private void initializeProperties(String configPath) {
		prop =  new Properties();
		InputStream is = null;
		try{
			if(configPath ==  null || configPath == "") {
				configPath = "src/configuration.prop";
			}
			is = new FileInputStream(configPath);
			prop.load(is);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadProp(String configurationPath) {
		PropLoader.configPath = configurationPath;
	}
	
	public static PropLoader getPropLoaderInstance() {
		if(propLoader == null) {
			propLoader = new PropLoader();
		}
		return propLoader;
	}
	
	public Properties getAllProperties() {
		return prop;
	}
	
	public String getValue(String propKey) {
		return  prop.getProperty(propKey);
	}

	

}
