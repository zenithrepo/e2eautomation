package com.zenith.automation.utility;

import java.time.LocalDate;
import java.util.Date;
import java.util.Vector;

import javax.swing.plaf.synth.SynthSpinnerUI;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.zenith.automation.batch.BatchCollection;
import com.zenith.automation.batch.TdrStructure;
import com.zenith.automation.framework.Constants;

public class ScpUtility {
	
	public Session getScpSession(String hostname, String username, String password) {
		JSch jsch = new JSch();
		Session session = null;
		try {
        session = jsch.getSession(username, hostname, 22);
        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(password);
        session.connect();
		}catch(JSchException e) {
			System.out.println("Unable to connection to host:"+hostname);
			e.printStackTrace();
			CommonUtility.exit(0, "Unable to connection to host:"+hostname);
		}catch(Exception e) {
			System.out.println("Unhandled exceptions");
			e.printStackTrace();
		}

		return session;
	}
	
	public ChannelSftp getChannel(Session session) {
        Channel channel = null;
		try {
			channel = session.openChannel("sftp");
			channel.connect();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        ChannelSftp sftpChannel = (ChannelSftp) channel;
        return sftpChannel;

	} 
	
	public boolean downloadFiles(Session session, ChannelSftp channel, String sourceDir, String destDir) {
		boolean status = false;
        try {
        	Vector vector = channel.ls(sourceDir + "/*.csv");
        	Date lastDate = null;
        	if(Constants.MZ_HIT_TS != null) {//Request was sent to MZ, So only collect the TDR after that time
        		//As per analysis Server is running behind by 24 sec
        		lastDate =  new Date(Constants.MZ_HIT_TS.getTime() - Constants.SERVER_RUNNIG_BEHING);
        	}else {
        		lastDate = new Date(System.currentTimeMillis() - Constants.GET_OLD_FILES);//By default, 5 minutes old file.
        	}
        	
        	long lastEpocDt = (lastDate.getTime())/1000;
        	for (Object sftpFile : vector) {
        		ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry) sftpFile;
        		
        		SftpATTRS attrs = lsEntry.getAttrs();
        		long fileTimeStamp = attrs.getMTime();
        		if((fileTimeStamp > lastEpocDt) && (attrs.getSize() > 0)) {
        			//System.out.println("Differnce from on MZ date:File TimeStamp:"+fileTimeStamp+" ,MZ TS:"+Constants.MZ_HIT_TS.getTime()/1000+" , differnce from MZ:"+(fileTimeStamp -Constants.MZ_HIT_TS.getTime()/1000)+", local computed ts:"+lastEpocDt+" ,differnce from local timestamp"+(fileTimeStamp - lastEpocDt));
        			System.out.println("Downloaded file "+ sourceDir+ "/"+lsEntry.getFilename()+" ,Size:"+attrs.getSize());
        			channel.get(sourceDir+"/"+lsEntry.getFilename(), destDir);
        			/*
        			BatchCollection bc = new BatchCollection();
        			TDRStructure ts = bc.getEachTDR(channel.getInputStream(), "POC_DATA_201803110700");
        			System.out.println("TDR Structure:"+ts.toString());
        			*/
        			status = true;
        		}
            }
		} catch (SftpException e) {
			System.out.println("No files found..."+sourceDir);
			e.printStackTrace();
		}catch(Exception e){
			System.out.println("Unhandled exception while sftp connections... processing further....");
			e.printStackTrace();
		}finally {
			if(!status) {
				System.out.println("No files downloaded for the server.");
			}
	        channel.exit();
	        session.disconnect();
		}
        return status;
	}

}
