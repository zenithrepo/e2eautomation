package com.zenith.reporting;

public class Constants {

	public static final String SUITE_INSERT_STMT = "insert into TB_REPORT(test_instance,suite_id,test_level,name,env,start_ts,end_ts)values (?,?,?,?,?,?,?)";
	public static final String TESTCASE_INSERT_STMT = "insert into TB_REPORT(test_instance,suite_id,testcase_id,test_level,name,env,start_ts,end_ts)values (?,?,?,?,?,?,?,?)";
	public static final String STEP_INSERT_STMT = "insert into TB_REPORT(test_instance,suite_id,testcase_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String TEST_SUITE_LAST_INSTNACE_ID = "select max(test_instance) from tb_report where suite_id=?";
}
