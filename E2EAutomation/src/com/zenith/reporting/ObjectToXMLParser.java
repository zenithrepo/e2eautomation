package com.zenith.reporting;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;  
import java.util.LinkedList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.zenith.automation.framework.TestAssert;
import com.zenith.automation.framework.TestExecutorHandler;  
  
  
public class ObjectToXMLParser {  
	private LinkedList<ResultDAO> liResult = null;
	
	public synchronized void updateResult(ResultDAO dao) {
		ResultDAO result = new ResultDAO();
		if(liResult == null) {
			liResult = new LinkedList<ResultDAO>();
		}
		liResult.add(dao);	
	}
	
	public void generateXml(String resultXml ) { 
		try {
			
		//Before creating XML, update result in DB.
		ResultToDB resultDb = new ResultToDB();
		int iRows = resultDb.insertInDb(this.liResult);
		System.out.println(iRows + " inserted in DB");
	    JAXBContext contextObj = JAXBContext.newInstance(XmlSkeleton.class);
		
	    Marshaller marshallerObj = contextObj.createMarshaller();  
	    marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    XmlSuite xmlSuite = new XmlSuite();
	    
	    xmlSuite.setTestCase(this.liResult);
	    
	    XmlSkeleton ske=new XmlSkeleton();
	    ske.setTestSuite(xmlSuite);
	    marshallerObj.marshal(ske, new FileOutputStream(resultXml));
	    System.out.println("File generated");
	    XmlToHtml xmlHtml = new XmlToHtml();
	    xmlHtml.generateReport();
		}catch(JAXBException e) {
			e.printStackTrace();
		}  catch( FileNotFoundException e) {
			e.printStackTrace();
		}
	}  
}  