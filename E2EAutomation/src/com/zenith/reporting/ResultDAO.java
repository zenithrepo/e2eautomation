package com.zenith.reporting;

import java.util.ArrayList;
import java.util.Date;


public class ResultDAO {
	private int testSuiteId;
	private int testCaseId;
	private int executionSeqNo;
	private String testSuite;
	private String testCase;
	private String testStep;
	private Object expectedResult;
	private Object actualResult;
	private String summary;
	private String status;
	private Date testStartTime;
	private Date testEndTime;
	private String executionTime;
	private String env;
	


	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public int getExecutionSeqNo() {
		return executionSeqNo;
	}
	public void setExecutionSeqNo(int executionSeqNo) {
		this.executionSeqNo = executionSeqNo;
	}
	public int getTestSuiteId() {
		return testSuiteId;
	}
	public void setTestSuiteId(int testSuiteId) {
		this.testSuiteId = testSuiteId;
	}
	public int getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getExecutionTime() {
		return executionTime;
	}
	public void setExecutionTime(String executionTime) {
		this.executionTime = executionTime;
	}
	public Date getTestStartTime() {
		return testStartTime;
	}
	public void setTestStartTime(Date testStartTime) {
		this.testStartTime = testStartTime;
	}
	public Date getTestEndTime() {
		return testEndTime;
	}
	public void setTestEndTime(Date testEndTime) {
		this.testEndTime = testEndTime;
	}
	public String getTestSuite() {
		return testSuite;
	}
	public void setTestSuite(String testSuite) {
		this.testSuite = testSuite;
	}
	public String getTestCase() {
		return testCase;
	}
	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}
	public String getTestStep() {
		return testStep;
	}
	public void setTestStep(String testStep) {
		this.testStep = testStep;
	}
	public Object getExpectedResult() {
		return expectedResult.toString();
	}
	public void setExpectedResult(Object expectedResult) {
		this.expectedResult = expectedResult;
	}
	public Object getActualResult() {
		return actualResult.toString();
	}
	public void setActualResult(Object actualResult) {
		this.actualResult = actualResult;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
