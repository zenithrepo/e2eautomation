package com.zenith.reporting;

import java.util.Date;


//Field to be populated in tb_report tables
public class ResultTcDAO {
	private int testInstnaceId;
	private int suiteid;
	private int testCaseId;
	
	private String testCaseName;
	private String env;
	
	private Date startTs;
	private Date endTs;
	
	
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public int getTestInstnaceId() {
		return testInstnaceId;
	}
	public void setTestInstnaceId(int testInstnaceId) {
		this.testInstnaceId = testInstnaceId;
	}
	public int getSuiteid() {
		return suiteid;
	}
	public void setSuiteid(int suiteid) {
		this.suiteid = suiteid;
	}
	public int getTestCaseId() {
		return testCaseId;
	}
	public void setTestCaseId(int testCaseId) {
		this.testCaseId = testCaseId;
	}
	public String getTestCaseName() {
		return testCaseName;
	}
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
	public Date getStartTs() {
		return startTs;
	}
	public void setStartTs(Date startTs) {
		this.startTs = startTs;
	}
	public Date getEndTs() {
		return endTs;
	}
	public void setEndTs(Date endTs) {
		this.endTs = endTs;
	}
	@Override
	public String toString() {
		return "ResultTcDAO [testInstnaceId=" + testInstnaceId + ", suiteid=" + suiteid + ", testCaseId=" + testCaseId
				+ ", testCaseName=" + testCaseName + ", startTs=" + startTs + ", endTs=" + endTs + "]";
	}
	
	
	
}
