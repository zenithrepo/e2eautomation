package com.zenith.reporting;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zenith.reporting.Constants;

import com.zenith.automation.utility.MySqlUtility;

public class ResultToDB {
	Connection conn;
	// Suite level . TestCases->TestSteps
	Map<String, ResultTcDAO> testCaseMap;

	public static void main(String[] args) {
		ResultToDB res = new ResultToDB();
	}

	public ResultToDB() {
		this.conn = new MySqlUtility().getDBConnection();
		testCaseMap = new HashMap<String, ResultTcDAO>();

	}


	public void insertSuiteLevel(int instanceId, int suiteId,String name, String env, java.util.Date startTs, java.util.Date endTs) throws SQLException {
		int columnCtr = 1;
		PreparedStatement preparedStatement = conn.prepareStatement(Constants.SUITE_INSERT_STMT);
		preparedStatement.setInt(columnCtr++, instanceId);// test_instance
		preparedStatement.setInt(columnCtr++, suiteId);// suite_id
		preparedStatement.setInt(columnCtr++, 1);// test_level
		preparedStatement.setString(columnCtr++,name);// step_name
		preparedStatement.setString(columnCtr++, env);// env
		preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(startTs));
		preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(endTs));
		
		preparedStatement.addBatch();
		preparedStatement.executeBatch();
		conn.commit();
	}

	
	public int insertInDb(List<ResultDAO> llResult) {
		PreparedStatement preparedStatement = null;
		int totalRecs = -1;
		try {
			preparedStatement = conn.prepareStatement(Constants.STEP_INSERT_STMT);
			conn.setAutoCommit(false);
			int testStepCtr = 0,testCaseCtr = 0, instanceId = 0, suiteId =0;
			boolean isFirstRecord = true;
			java.util.Date startTs = null, endTs = null;
			String suiteName = null, env = null;
			for (ResultDAO resultDao : llResult) {
				if (isFirstRecord) {
					instanceId = getLastInstanceId(resultDao.getTestSuiteId());
					startTs = resultDao.getTestStartTime();
					suiteId = resultDao.getTestSuiteId();
					suiteName = resultDao.getTestSuite();
					env = resultDao.getEnv();
					isFirstRecord = false;
				}
				endTs = resultDao.getTestEndTime(); 
				boolean isNewTc = updateTestCasemap(resultDao.getTestCase(),instanceId, resultDao);
				if(isNewTc) {
					testStepCtr = 0;
					if(resultDao.getTestCaseId() == 0) {
						testCaseCtr++;
					}else {
						testCaseCtr = resultDao.getTestCaseId();
					}
				}
				int columnCtr = 1;
				preparedStatement.setInt(columnCtr++, instanceId);// test_instance
				preparedStatement.setInt(columnCtr++, resultDao.getTestSuiteId());// suite_id
				preparedStatement.setInt(columnCtr++, testCaseCtr);// testcase_id
				preparedStatement.setInt(columnCtr++, 3);// test_level
				preparedStatement.setInt(columnCtr++, ++testStepCtr);// step_number
				preparedStatement.setString(columnCtr++, resultDao.getTestStep());// step_name
				preparedStatement.setString(columnCtr++, resultDao.getStatus());// status
				preparedStatement.setString(columnCtr++, resultDao.getSummary());// result_message
				preparedStatement.setString(columnCtr++, resultDao.getEnv());// env
				preparedStatement.setString(columnCtr++, "");// executed_by
				preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(resultDao.getTestStartTime()));// start_date
				preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(resultDao.getTestEndTime()));// complete_date
				preparedStatement.addBatch();
			}
			insertSuiteLevel(instanceId, suiteId, suiteName, env,startTs,endTs);
			totalRecs = preparedStatement.executeBatch().length;
			conn.commit();
			insertTestCaseLevel();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return totalRecs;
	}

	public boolean updateTestCasemap(String key,int  instanceId,ResultDAO stepDao) {
		boolean newTestCase = false;
		if(!testCaseMap.containsKey(key)) {// if doesnot contains key...
			newTestCase = true;
			ResultTcDAO tcDao =  new ResultTcDAO();
			tcDao.setSuiteid(stepDao.getTestSuiteId());
			tcDao.setTestCaseId(stepDao.getTestCaseId());
			tcDao.setTestInstnaceId(instanceId);
			tcDao.setTestCaseName(stepDao.getTestCase());
			tcDao.setEnv(stepDao.getEnv());
			tcDao.setStartTs(stepDao.getTestStartTime());
			tcDao.setEndTs(stepDao.getTestEndTime());
			testCaseMap.put(key, tcDao);
		}else {//if already present, update the end ts in testcase level
			testCaseMap.get(key).setEndTs(stepDao.getTestEndTime());;
			
		}
		return newTestCase;
	}
	
	public void insertTestCaseLevel() throws SQLException {
		if(this.testCaseMap != null) {
			Set<String> testCases = this.testCaseMap.keySet();
			PreparedStatement preparedStatement =  conn.prepareStatement(Constants.TESTCASE_INSERT_STMT);
			int testCaseCtr =0;
			for(String testCase : testCases) {
				int columnCtr = 1;
				testCaseCtr++;
				ResultTcDAO tcDao = this.testCaseMap.get(testCase);
				preparedStatement.setInt(columnCtr++, tcDao.getTestInstnaceId());
				preparedStatement.setInt(columnCtr++, tcDao.getSuiteid());
				if(tcDao.getTestCaseId() == 0) {
					preparedStatement.setInt(columnCtr++, testCaseCtr);
				}else {
					preparedStatement.setInt(columnCtr++, tcDao.getTestCaseId());
				}
				preparedStatement.setInt(columnCtr++, 2);
				preparedStatement.setString(columnCtr++, tcDao.getTestCaseName());
				preparedStatement.setString(columnCtr++, tcDao.getEnv());
				preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(tcDao.getStartTs()));
				preparedStatement.setTimestamp(columnCtr++, dateToTimestamp(tcDao.getEndTs()));
				preparedStatement.addBatch();
			}
			preparedStatement.executeBatch();
			conn.commit();
		}
		
	}

	public Timestamp dateToTimestamp(java.util.Date utilDate) {
		Timestamp ts = new Timestamp(utilDate.getTime());
		return ts;
	}
	public int getLastInstanceId(int suiteId) throws SQLException {
		int instanceId = -1; 
		PreparedStatement lstId = conn.prepareStatement(Constants.TEST_SUITE_LAST_INSTNACE_ID);
		lstId.setInt(1, suiteId);
		ResultSet rs = lstId.executeQuery();
		if (rs.next()) {
			String tmpId = rs.getString(1);
			if (tmpId != null) {
				instanceId = Integer.parseInt(tmpId) + 1;
			} else {
				instanceId = 1;
			}
		}
		return instanceId;

	}
	

}
