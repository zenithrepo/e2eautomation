
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

 
	<xsl:template match="/">
		<html>
 
			<head>
			
			<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.js"></script>
			<script type="text/javascript">
			
			$(document).ready(function(){
			    $('#dataTable td.y_n').each(function(){
			        if ($(this).text() == 'FALSE') {
			            $(this).css('background-color','#E90C06');
			        }
			        if ($(this).text() == 'TRUE') {
			            $(this).css('background-color','#B3FA62');
			        }
			    });
			});
			
			</script>
				
				
			</head>
 
			<body>
			
				<table class="tfmt" style="width:100%" border="1" id="dataTable">
				
					<tr bgcolor="#FFEE40">
						<th style="width:350px">TestSuite</th>
						<th style="width:350px">TestCase</th>
						<th style="width:350px">TestStep</th>
						<th style="width:250px">ExpectedResult</th>
						<th style="width:250px">ActualResult</th>
						<th style="width:250px">Summary</th>
						<th style="width:250px">ExecutionTime</th>
						<th style="width:250px">Status</th>
					</tr>
 
					<xsl:for-each select="xmlSkeleton/testSuite/testCase">
 						<tr bgcolor="#F1F4B3">
							<td class="colfmt" align="center">
								<xsl:value-of select="testSuite"/>
							</td>
							<td class="colfmt" align="center">
								<xsl:value-of select="testCase"/>
							</td>
							<td class="colfmt" align="center">
								<xsl:value-of select="testStep"/>
							</td>
							
							<td class="colfmt">
								<xsl:value-of select="expectedResult"/>
							</td>
 							<xsl:if  test="actualResult">
							<td class="colfmt">
								<xsl:value-of select="actualResult" />
							</td>
							</xsl:if>
							<td  class="colfmt"  align="center">
								<xsl:value-of select="summary" />
							</td>
							<td  class="y_n" align="center">
								<xsl:value-of select="executionTime" />
							</td>
							<td  class="y_n" align="center">
								<xsl:value-of select="status" />
							</td>

						</tr>
 
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>