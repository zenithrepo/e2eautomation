package com.zenith.reporting;

import java.util.LinkedList;
import java.util.List;

 
import javax.xml.bind.annotation.XmlElement;  
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement  
public class XmlSkeleton {
	private XmlSuite testSuite;
	
	public XmlSkeleton() {}

	public XmlSkeleton(XmlSuite testSuite) {
		super();
		this.testSuite = testSuite;
	}
	
	@XmlElement
	public XmlSuite getTestSuite() {
		return testSuite;
	}

	public void setTestSuite(XmlSuite testSuite) {
		this.testSuite = testSuite;
	}
	
	
}
