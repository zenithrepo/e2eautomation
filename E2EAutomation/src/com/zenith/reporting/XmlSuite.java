package com.zenith.reporting;

import java.util.LinkedList;
import java.util.List;

public class XmlSuite {
	private LinkedList<ResultDAO> testCase;
	
	
	public LinkedList<ResultDAO> getTestCase() {
		return testCase;
	}

	public void setTestCase(LinkedList<ResultDAO> testCase) {
		this.testCase = testCase;
	}
	
}
