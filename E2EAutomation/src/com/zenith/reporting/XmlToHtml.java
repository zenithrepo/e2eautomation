package com.zenith.reporting;
 
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
 
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.zenith.automation.utility.CommonUtility;
import com.zenith.automation.utility.PropLoader;
 
public class XmlToHtml {
	PropLoader propLoader = PropLoader.getPropLoaderInstance();
	String local = propLoader.getValue("TEST_RESULT");

	public static void main(String [] args) {
		XmlToHtml xml = new XmlToHtml();
		xml.generateReport();
	}
	public  void generateReport() {
		Source xml = new StreamSource( propLoader.getValue("TEST_RESULT_XML_OUTPUT"));
		Source xslt = new StreamSource("C:\\Users\\x172875\\Documents\\BitBucket_Repo\\e2eautomation\\E2EAutomation\\src\\com\\zenith\\reporting\\TestReport.xsl");
		
		convertXMLToHTML(xml, xslt);

	}
 
	public void convertXMLToHTML(Source xml, Source xslt) {
		StringWriter sw = new StringWriter();
 
		try {
 
			FileWriter fw = new FileWriter("C:\\Users\\x172875\\Documents\\tmp\\TestResult.html");
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer trasform = tFactory.newTransformer(xslt);
			trasform.transform(xml, new StreamResult(sw));
			fw.write(sw.toString());
			fw.close();
 
			System.out.println("Reports generated successfully at C:\\Users\\x172875\\Documents\\tmp\\template ");
 
		} catch (IOException | TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
 
		
	}
}