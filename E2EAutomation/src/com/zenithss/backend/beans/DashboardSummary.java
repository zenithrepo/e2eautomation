package com.zenithss.backend.beans;

public class DashboardSummary {

	int totalSuites;
	int totalTestcases;
	int successSuites;
	int failedTestCases;
	int successTestCases;
	int skippedTestCases;
	int successSteps;
	int failedSteps;
	
	String date = "";
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTotalTestcases() {
		return totalTestcases;
	}
	public void setTotalTestcases(int totalTestcases) {
		this.totalTestcases = totalTestcases;
	}
	public int getSuccessSteps() {
		return successSteps;
	}
	public void setSuccessSteps(int successSteps) {
		this.successSteps = successSteps;
	}
	public int getFailedSteps() {
		return failedSteps;
	}
	public void setFailedSteps(int failedSteps) {
		this.failedSteps = failedSteps;
	}
	public int getTotalSuites() {
		return totalSuites;
	}
	public void setTotalSuites(int totalSuites) {
		this.totalSuites = totalSuites;
	}
	public int getSuccessSuites() {
		return successSuites;
	}
	public void setSuccessSuites(int successSuites) {
		this.successSuites = successSuites;
	}
	public int getFailedTestCases() {
		return failedTestCases;
	}
	public void setFailedTestCases(int failedTestCases) {
		this.failedTestCases = failedTestCases;
	}
	public int getSuccessTestCases() {
		return successTestCases;
	}
	public void setSuccessTestCases(int successTestCases) {
		this.successTestCases = successTestCases;
	}
	public int getSkippedTestCases() {
		return skippedTestCases;
	}
	public void setSkippedTestCases(int skippedTestCases) {
		this.skippedTestCases = skippedTestCases;
	}
	
	
	
}
