package com.zenithss.backend.beans;

import java.io.Serializable;
import java.util.LinkedList;


public class DashboardSummaryList extends LinkedList<DashboardSummary> implements Serializable{
	private static final long serialVersionUID = 7139955782444811322L;
}
