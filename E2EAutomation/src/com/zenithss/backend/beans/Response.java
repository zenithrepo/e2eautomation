package com.zenithss.backend.beans;

public class Response{

	String status;
	String errorMessage;
	int durationInSec;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public int getDurationInSec() {
		return durationInSec;
	}
	public void setDurationInSec(int durationInSec) {
		this.durationInSec = durationInSec;
	}
	
}
