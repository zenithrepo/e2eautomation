package com.zenithss.backend.beans;

public class StatusStatistics
{
	String statusName;
	int statusTotal;
	int statusPercentage;
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public int getStatusTotal() {
		return statusTotal;
	}
	public void setStatusTotal(int statusTotal) {
		this.statusTotal = statusTotal;
	}
	public int getStatusPercentage() {
		return statusPercentage;
	}
	public void setStatusPercentage(int statusPercentage) {
		this.statusPercentage = statusPercentage;
	}
	
	
}
