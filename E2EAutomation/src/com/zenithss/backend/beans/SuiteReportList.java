package com.zenithss.backend.beans;

import java.io.Serializable;
import java.util.LinkedList;


public class SuiteReportList extends LinkedList<SuiteReport> implements Serializable{
	private static final long serialVersionUID = 7139955782444811322L;
}
