package com.zenithss.backend.beans;

import java.util.ArrayList;

public class SuiteStatistics {


	int total;
    int successTotal;
    int successPercentage;
    
    int failedTotal;
    int failedPercentage;
    
    int ignoredTotal;
    int ignoredPercentage;
    
    int pendingTotal;
    int pendingPercentage;
    
    int notActive = 0;
    
    String[] labelsPerceantageArray = new String[4];
    int[] seriePerceantageArray = new int[4];
	
    
	public String[] getLabelsPerceantageArray() {
		return labelsPerceantageArray;
	}

	
	public int getNotActive() {
		return notActive;
	}


	public void setNotActive(int notActive) {
		this.notActive = notActive;
	}


	public void setLabelsPerceantageArray(String[] labelsPerceantageArray) {
		this.labelsPerceantageArray = labelsPerceantageArray;
	}

	public int[] getSeriePerceantageArray() {
		return seriePerceantageArray;
	}

	public void setSeriePerceantageArray(int[] seriePerceantageArray) {
		this.seriePerceantageArray = seriePerceantageArray;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getSuccessTotal() {
		return successTotal;
	}

	public void setSuccessTotal(int successTotal) {
		this.successTotal = successTotal;
	}

	public int getSuccessPercentage() {
		return successPercentage;
	}

	public void setSuccessPercentage(int successPercentage) {
		this.successPercentage = successPercentage;
	}

	public int getFailedTotal() {
		return failedTotal;
	}

	public void setFailedTotal(int failedTotal) {
		this.failedTotal = failedTotal;
	}

	public int getFailedPercentage() {
		return failedPercentage;
	}

	public void setFailedPercentage(int failedPercentage) {
		this.failedPercentage = failedPercentage;
	}

	public int getIgnoredTotal() {
		return ignoredTotal;
	}

	public void setIgnoredTotal(int ignoredTotal) {
		this.ignoredTotal = ignoredTotal;
	}

	public int getIgnoredPercentage() {
		return ignoredPercentage;
	}

	public void setIgnoredPercentage(int ignoredPercentage) {
		this.ignoredPercentage = ignoredPercentage;
	}

	public int getPendingTotal() {
		return pendingTotal;
	}

	public void setPendingTotal(int pendingTotal) {
		this.pendingTotal = pendingTotal;
	}

	public int getPendingPercentage() {
		return pendingPercentage;
	}

	public void setPendingPercentage(int pendingPercentage) {
		this.pendingPercentage = pendingPercentage;
	}

	
	
	
}
