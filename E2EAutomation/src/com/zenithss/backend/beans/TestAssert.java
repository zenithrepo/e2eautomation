package com.zenithss.backend.beans;

import java.util.ArrayList;

public class TestAssert{
	String pkgClass="com.zenith.automation.framework.TdrImpl";
	String identifier = "POC_DATA_201803160500";
	int waitTime = 0;
	String sessionId = ""; 
	String ban = "";
	String assertType = "CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100";
	
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}

	
	public String getAssertType() {
		return assertType;
	}
	public void setAssertType(String assertType) {
		this.assertType = assertType;
	}
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}


	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	}
