package com.zenithss.backend.beans;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;


public class TestCase {
 
	int testcaseId;
	String  testcaseName="";
	String	testcaseXml;
	String  testenv="";
	String tag="";
	String description="";
	Timestamp    creationDate;
	String hpqcId = "";
	/*ArrayList<TestPrerequisities> testPrerequisities = new ArrayList<TestPrerequisities>();
	ArrayList<TestStep> testStep = new ArrayList<TestStep>();
	ArrayList<TestAssert> testAssert = new ArrayList<TestAssert>();*/
	

	
	public String getTag() {
		return tag;
	}

	public String getHpqcId() {
		return hpqcId;
	}

	public void setHpqcId(String hpqcId) {
		this.hpqcId = hpqcId;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(int testcaseId) {
		this.testcaseId = testcaseId;
	}
	public String getTestcaseName() {
		return testcaseName;
	}
	public void setTestcaseName(String testcaseName) {
		this.testcaseName = testcaseName;
	}
	public String getTestcaseXml() {
		return testcaseXml;
	}
	public void setTestcaseXml(String testcaseXml) {
		this.testcaseXml = testcaseXml;
	}

	public String getTestenv() {
		return testenv;
	}
	public void setTestenv(String testenv) {
		this.testenv = testenv;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	@Override
	public String toString() {
		return "TestCase [testcaseId=" + testcaseId + ", testcaseName=" + testcaseName + ", testcaseXml=" + testcaseXml
				+ ", testenv=" + testenv + ", tag=" + tag + ", description=" + description + ", creationDate="
				+ creationDate + ", hpqcId=" + hpqcId + "]";
	}



	
	
}
