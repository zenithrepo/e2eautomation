package com.zenithss.backend.beans;

import java.util.ArrayList;
import java.util.List;

public class TestCaseReport {

	 int testcaseId;
	 TestRecord testCaseRecord = new TestRecord();
	 List<TestRecord> stepsList = new ArrayList<TestRecord>();
	public int getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(int testcaseId) {
		this.testcaseId = testcaseId;
	}
	public TestRecord getTestCaseRecord() {
		return testCaseRecord;
	}
	public void setTestCaseRecord(TestRecord testCaseRecord) {
		this.testCaseRecord = testCaseRecord;
	}
	public List<TestRecord> getStepsList() {
		return stepsList;
	}
	public void setStepsList(List<TestRecord> stepsList) {
		this.stepsList = stepsList;
	}
	 
	 
}
