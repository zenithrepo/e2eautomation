package com.zenithss.backend.beans;

public class TestPrerequisities{
	String pkgClass = "com.zenith.automation.framework.SapccDbImpl";
	String prerequisitiestype = "TABLE";
	String sub = "S8414167";
	String preCheckField = "METER*";
	int waitTime = 30;
	

	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}

	public String getPrerequisitiestype() {
		return prerequisitiestype;
	}
	public void setPrerequisitiestype(String prerequisitiestype) {
		this.prerequisitiestype = prerequisitiestype;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}
	public String getPreCheckField() {
		return preCheckField;
	}
	public void setPreCheckField(String preCheckField) {
		this.preCheckField = preCheckField;
	}
}
