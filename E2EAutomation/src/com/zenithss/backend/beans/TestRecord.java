package com.zenithss.backend.beans;

import java.util.ArrayList;

public class TestRecord {

	 ArrayList<TestRecordData> stepRecordList = new ArrayList<TestRecordData>();
	 ArrayList<TestRecordData> testRecordList = new ArrayList<TestRecordData>();
	 TestRecordData suiteRecord = new TestRecordData();
	public ArrayList<TestRecordData> getStepRecordList() {
		return stepRecordList;
	}
	public void setStepRecordList(ArrayList<TestRecordData> stepRecordList) {
		this.stepRecordList = stepRecordList;
	}
	public ArrayList<TestRecordData> getTestRecordList() {
		return testRecordList;
	}
	public void setTestRecordList(ArrayList<TestRecordData> testRecordList) {
		this.testRecordList = testRecordList;
	}
	public TestRecordData getSuiteRecord() {
		return suiteRecord;
	}
	public void setSuiteRecord(TestRecordData suiteRecord) {
		this.suiteRecord = suiteRecord;
	}   
}
