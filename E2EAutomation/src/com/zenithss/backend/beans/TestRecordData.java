package com.zenithss.backend.beans;

import java.sql.Timestamp;

public class TestRecordData {

	 long testInstance;
	 String testcaseName;
	 int testcaseId;
	 int suiteId;
	 String suiteName;
	 String stepName;
	 int stepNumber;
	 String status;
	 String resultMessage;
	 String env;
	 String executedBy;//     
	 Timestamp startDate;  // timestamp DEFAULT CURRENT_TIMESTAMP,
	 Timestamp completeDate; //  timestamp,
	 int testLevel =3;
	 
	 		
				
	public int getTestLevel() {
		return testLevel;
	}
	public void setTestLevel(int testLevel) {
		this.testLevel = testLevel;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}
	public int getStepNumber() {
		return stepNumber;
	}
	public void setStepNumber(int stepNumber) {
		this.stepNumber = stepNumber;
	}
	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public Timestamp getCompleteDate() {
		return completeDate;
	}
	public void setCompleteDate(Timestamp completeDate) {
		this.completeDate = completeDate;
	}
	public long getTestInstance() {
		return testInstance;
	}
	public void setTestInstance(long testInstance) {
		this.testInstance = testInstance;
	}
	public String getTestcaseName() {
		return testcaseName;
	}
	public void setTestcaseName(String testcaseName) {
		this.testcaseName = testcaseName;
	}
	public int getTestcaseId() {
		return testcaseId;
	}
	public void setTestcaseId(int testcaseId) {
		this.testcaseId = testcaseId;
	}
	public int getSuiteId() {
		return suiteId;
	}
	public void setSuiteId(int suiteId) {
		this.suiteId = suiteId;
	}
	public String getSuiteName() {
		return suiteName;
	}
	public void setSuiteName(String suiteName) {
		this.suiteName = suiteName;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public String getExecutedBy() {
		return executedBy;
	}
	public void setExecutedBy(String executedBy) {
		this.executedBy = executedBy;
	}
}
