package com.zenithss.backend.beans;

import java.util.HashMap;

public class TestStep {

	String pkgClass="";
	String ban="";
	String subscriberId="";
	String sessionId="";
	int mccMnc;
	String ratingGroup="";
	long usage;
	String unit="";
	int waitTime;
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getWaitTime() {
		return waitTime;
	}
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}
	public String getPkgClass() {
		return pkgClass;
	}
	public void setPkgClass(String pkgClass) {
		this.pkgClass = pkgClass;
	}
	public String getBan() {
		return ban;
	}
	public void setBan(String ban) {
		this.ban = ban;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public long getUsage() {
		return usage;
	}
	public void setUsage(long usage) {
		this.usage = usage;
	}
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public int getMccMnc() {
		return mccMnc;
	}
	public void setMccMnc(int mccMnc) {
		this.mccMnc = mccMnc;
	}
	public String getRatingGroup() {
		return ratingGroup;
	}
	public void setRatingGroup(String ratingGroup) {
		this.ratingGroup = ratingGroup;
	}
	
	
	
	
}
