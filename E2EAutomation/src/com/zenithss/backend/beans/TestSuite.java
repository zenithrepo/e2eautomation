package com.zenithss.backend.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TestSuite {

	int suiteId;
	String name;
	String env;
	int executionCounter = 0;
	Timestamp creationDate;
	List<TestCase> testcases = new ArrayList<>();
	
	public int getExecutionCounter() {
		return executionCounter;
	}
	public void setExecutionCounter(int executionCounter) {
		this.executionCounter = executionCounter;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public int getSuiteId() {
		return suiteId;
	}
	public void setSuiteId(int suiteId) {
		this.suiteId = suiteId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEnv() {
		return env;
	}
	public void setEnv(String env) {
		this.env = env;
	}
	public List<TestCase> getTestcases() {
		return testcases;
	}
	public void setTestcases(List<TestCase> testcases) {
		this.testcases = testcases;
	}
	@Override
	public String toString() {
		return "TestSuite [suiteId=" + suiteId + ", name=" + name + ", env=" + env + ", executionCounter="
				+ executionCounter + ", creationDate=" + creationDate + ", testcases=" + testcases + "]";
	}
	
}
