package com.zenithss.backend.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

public class BaseDao {

	protected DataSource getDataSource() throws NamingException {
		DataSource res = null;
	
		Context ctx = new InitialContext();
		res = (DataSource) ctx.lookup("java:comp/env/jdbc/atdatasource"); 
		
		ctx.close();	    
		return res;
	}

	protected void info(Object msg) {
		Logger.getLogger(getClass()).info(msg);
	}
	
	protected void warn(Object msg) {
		Logger.getLogger(getClass()).warn(msg);
	}

	protected void debug(Object msg) {
		Logger.getLogger(getClass()).debug(msg);
	}
	
	protected void error(Object msg, Throwable err) {
		Logger.getLogger(getClass()).error(msg, err);
	}

	protected Connection getConnection() throws SQLException, NamingException {
		DataSource ds = getDataSource();
		Connection cn = ds.getConnection();
	    return cn;
	}


	
	protected void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			}
			catch (Exception e) {
				// no implementation
			}
		}
	}

	protected boolean isFilled(String str) {
		return str != null && str.trim().length() > 0;
	}

	protected boolean isNull(String param) {
		return param == null || param.equals("null") || param.equals("") || param.equals("yyyymmdd");
	}

	protected void close(ResultSet set) {
		try {
			if(set != null){
				set.close();
			}
		}
		catch(Exception e){
			// no implementation
		}
	}

	protected void close(PreparedStatement statement) {
		try {
			if(statement != null){
				statement.close();
			}
		}
		catch(Exception e){
			// no implementation
		}
	}

}
