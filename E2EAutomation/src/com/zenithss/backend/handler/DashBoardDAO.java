package com.zenithss.backend.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.NamingException;

import com.zenithss.backend.beans.DashboardSummary;
import com.zenithss.backend.beans.DashboardSummaryList;
import com.zenithss.backend.beans.TestRecord;
import com.zenithss.backend.beans.TestSuiteList;

public class DashBoardDAO extends BaseDao {

	public DashboardSummaryList getDashboardSummary() throws SQLException, NamingException {
		DashboardSummaryList dashboardSummaryList = new DashboardSummaryList();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("SELECT (SELECT COUNT(*) FROM tb_suite) as totalSuites, " + 
					"  (SELECT COUNT(*) FROM tb_testcase) as totalTestcases," + 
					"  (SELECT COUNT(*) FROM tb_report WHERE status='success' and record_type=2 ) as successTestcase," + 
					"  (SELECT COUNT(*) FROM tb_report WHERE status='failed' and record_type=2 ) as failedTestcase," + 
					"  (SELECT COUNT(*) FROM tb_report WHERE status='success' and record_type=3 ) as successSteps," + 
					"  (SELECT COUNT(*) FROM tb_report WHERE status='failed' and record_type=3 ) as failedSteps;");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				DashboardSummary dashboardSummary = new DashboardSummary();
				dashboardSummary.setTotalSuites(rs.getInt("totalSuites"));
				dashboardSummary.setTotalTestcases(rs.getInt("totalTestcases"));
				dashboardSummary.setSuccessTestCases(rs.getInt("successTestcase"));
				dashboardSummary.setSuccessSteps(rs.getInt("successSteps"));
				dashboardSummary.setFailedTestCases(rs.getInt("failedTestcase"));
				dashboardSummary.setSuccessTestCases(rs.getInt("successTestcase"));
				
				System.out.println("dashboardSummary = "+dashboardSummary);
				dashboardSummaryList.add(dashboardSummary);
				
			}
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return dashboardSummaryList;
	}
	
	
	
}
