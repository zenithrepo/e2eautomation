package com.zenithss.backend.handler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import com.zenithss.backend.beans.*;
import com.zenithss.backend.util.ProcessXML;


public class TestCaseDAO extends BaseDao {
	
	

	public TestCaseDAO() throws ClassNotFoundException, NamingException {
	}

	
	public TestCase getTestCase(String tid) throws SQLException, NamingException 
	{
		TestCase tc = new TestCase();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select testcase_id , testcase_name,testcase_xml,env,tag,description,creation_date, hpqc_id from tb_testcases where testcase_id=?");
			stmt.setString(1, tid);
			System.out.println("getTestCase :: afterquery ");			
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {				
			
				tc.setTestcaseName(rs.getString("testcase_name"));
				tc.setTestcaseId(rs.getInt("testcase_id"));
				tc.setTestenv(rs.getString("env"));
				tc.setCreationDate(rs.getTimestamp("creation_date"));
				tc.setTag(rs.getString("tag"));
				tc.setDescription(rs.getString("description"));	
				tc.setHpqcId(rs.getString("hpqc_id"));					
				tc.setTestcaseXml(rs.getString("testcase_xml"));
				
			  }				
			}
			catch (SQLException e) {
				error("error", e);
			}
			finally {
				close(con);
			}

		return tc;		
	}
	
	
	public Response deleteTestCase(String tid) throws SQLException, NamingException {
		Response response = new Response();
		int suiteID=0 ; 
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("createTestCase :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("delete from TB_TESTCASES where testcase_id=?");	
			 pstmt.setString(1, tid);			
			 int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {			 
				 response.setStatus("success");
				 response.setErrorMessage("TestCase #" +tid+ " is deleted successfully");
			 }
			System.out.println("createTestCase :: insertResult = "+suiteID);
		}
		catch (Exception e) {
			error("error", e);
			response.setStatus("failed");
			 response.setErrorMessage("TestCase #" +tid+ " is used in some suite, so delete suite first and then delete testcase.");
		}
		finally {
			close(con);	
			}

		return  response;
	}
	
	public Response createTestCase(TestCase testcase) throws SQLException, NamingException {
		Response response = new Response();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("createTestCase :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("insert into TB_TESTCASES (testcase_name,env,tag, testcase_xml,description,hpqc_id) values(?,?,?,?,?,?)");	
			 pstmt.setString(1, testcase.getTestcaseName());
			 pstmt.setString(2, testcase.getTestenv());		
			 pstmt.setString(3, testcase.getTag());	
			 pstmt.setString(4, testcase.getTestcaseXml());	
			 pstmt.setString(5, testcase.getDescription());	
			 pstmt.setString(6, testcase.getHpqcId());	
			 int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {			 
				 response.setStatus("success");
				 response.setErrorMessage("Testcase "+testcase.getTestcaseName()+" added successfully");
			 }
			
		}
		catch (Exception e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  response;
	}
	public Response updateTestCase(TestCase testcase) throws SQLException, NamingException {
		Response response = new Response();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("createTestCase :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("update TB_TESTCASES set testcase_name = ? , tag = ?, testcase_xml = ? ,description = ? ,hpqc_id = ? where testcase_id=?");	
			 pstmt.setString(1, testcase.getTestcaseName());			 	
			 pstmt.setString(2, testcase.getTag());	
			 pstmt.setString(3, testcase.getTestcaseXml());	
			 pstmt.setString(4, testcase.getDescription());	
			 pstmt.setString(5, testcase.getHpqcId());
			 pstmt.setInt(6, testcase.getTestcaseId());
			 int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {			 
				 response.setStatus("success");
				 response.setErrorMessage("Testcase "+testcase.getTestcaseName()+" updated successfully");
			 }			
		}
		catch (Exception e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  response;
	}
	
	
	
	public ObjectList getTestCaseSelectOptions() throws SQLException, NamingException 
	{
		ObjectList tList = new ObjectList();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select testcase_id , testcase_name from tb_testcases");
			System.out.println("afterquery = ");			
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {				
				IOptions tc = new IOptions();
				String tid = rs.getString("testcase_id");
				String tname = rs.getString("testcase_name");
				
				tc.setLabel(tid+"-"+tname);
				tc.setValue(tid);					
				
				tList.add(tc);
			  }				
			}
			catch (SQLException e) {
				error("error", e);
			}
			finally {
				close(con);
			}

		return tList;		
	}
	
	
	
	public TestCaseList getTestCaseList() throws SQLException, NamingException 
	{
		TestCaseList tList = new TestCaseList();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select testcase_id , testcase_name,testcase_xml,env,tag,description,creation_date, hpqc_id from tb_testcases");
			System.out.println("afterquery = ");			
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {				
			TestCase tc = new TestCase();
				tc.setTestcaseName(rs.getString("testcase_name"));
				tc.setTestcaseId(rs.getInt("testcase_id"));
				tc.setTestenv(rs.getString("env"));
				tc.setCreationDate(rs.getTimestamp("creation_date"));
				tc.setTag(rs.getString("tag"));
				tc.setDescription(rs.getString("description"));	
				tc.setHpqcId(rs.getString("hpqc_id"));	
				//ProcessXML processXML = new ProcessXML();				
				//tc = processXML.parseDocument(rs.getString("testcase_xml"), tc);
				tc.setTestcaseXml(rs.getString("testcase_xml"));
				tList.add(tc);
			  }				
			}
			catch (SQLException e) {
				error("error", e);
			}
			finally {
				close(con);
			}

		return tList;		
	}
	
	public TestCaseList getTestCasedata(String testCaseID) throws SQLException, NamingException {
		TestCaseList tclist = new TestCaseList();

		if(testCaseID != null && testCaseID.equalsIgnoreCase("0")) {
			TestCase tc = new TestCase();
			/*tc.getTestPrerequisities().add(new TestPrerequisities());
			tc.getTestStep().add(new TestStep());
			tc.getTestAssert().add(new TestAssert());*/
			
			tclist.add(tc);
		}else
		{
			
			
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select testcase_id,testcase_name,testcase_xml, env,tag,description,hpqc_id from tb_testcases where testcase_id = ?");
		
			stmt.setString(1, testCaseID);					
			
			ResultSet rs = stmt.executeQuery();

			TestCase tc = new TestCase();
			
			while (rs.next()) {			
				
				tc.setTestcaseName(rs.getString("testcase_name"));
				tc.setTestcaseId(rs.getInt("testcase_id"));
				tc.setTestcaseXml(rs.getString("testcase_xml"));
				tc.setTestenv(rs.getString("env"));
				tc.setHpqcId(rs.getString("hpqc_id"));
				ProcessXML processXML = new ProcessXML();				
				tc = processXML.parseDocument(rs.getString("testcase_xml"), tc);
				tclist.add(tc);
				System.out.println("reading testcase data and tname = "+tc.getTestcaseName());
			}
			tclist.add(tc);	
		}			
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);
		}
		}
		return tclist;
	}

	
	
	
	public ArrayList<TestStep> parseXML(String xml)
	{
		ArrayList<TestStep> stepList = new ArrayList<TestStep>();
		
		
		return stepList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
