
package com.zenithss.backend.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import com.zenithss.backend.beans.DashboardSummary;
import com.zenithss.backend.beans.DashboardSummaryList;
import com.zenithss.backend.beans.StatusStatistics;
import com.zenithss.backend.beans.SuiteReport;
import com.zenithss.backend.beans.SuiteReportList;
import com.zenithss.backend.beans.SuiteStatistics;
import com.zenithss.backend.beans.TestCase;
import com.zenithss.backend.beans.TestRecord;
import com.zenithss.backend.beans.TestRecordData;
import com.zenithss.backend.beans.TestSuite;
import com.zenithss.backend.beans.TestSuiteList;


public class TestReportDAO extends BaseDao {
	
	

	public TestReportDAO() throws ClassNotFoundException, NamingException {
	}

	
	

	public List<TestRecordData> getSuiteRecordHistory(String suiteId ) throws SQLException, NamingException {
		List<TestRecordData> suiteRecordHistoryList = new ArrayList<TestRecordData>();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("getSuiteRecordHistory :: con = "+con);
			PreparedStatement stmt = con.prepareStatement("select DISTINCT  (test_instance) as test_instance, testcase_id, suite_id , end_ts , status from tb_report where suite_id= ? and test_level=1 group by test_instance order by test_instance DESC");
			stmt.setString(1, suiteId);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				TestRecordData testRecord = new TestRecordData();
				testRecord.setTestInstance(rs.getInt("test_instance"));
				testRecord.setTestcaseId(rs.getInt("testcase_id"));
				testRecord.setSuiteId(rs.getInt("suite_id"));
				testRecord.setStatus(rs.getString("status"));
				testRecord.setCompleteDate(rs.getTimestamp("end_ts"));
				System.out.println("suiteRecordHistory = "+testRecord);
				suiteRecordHistoryList.add(testRecord);				
			}
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  suiteRecordHistoryList;
	}
	
	public ArrayList<SuiteStatistics> getInstancePercentage(long latestReocdInstanceID , String suiteID) throws SQLException, NamingException {
		ArrayList<SuiteStatistics> suiteStaArray = new ArrayList<SuiteStatistics>();
		
		Connection con = null;
		try {
			con = getConnection();
			
			System.out.println("latestReocdInstanceID = "+latestReocdInstanceID);
			PreparedStatement stmt = con.prepareStatement("select status, COUNT(status) as sum ,((count(status)/(SELECT COUNT(*) from tb_report as t where  t.status IS NOT NULL and test_level = 2 and t.test_instance =? and t.suite_id = ?)) * 100) as percentage "
					+ "from tb_report as r where r.status IS NOT NULL and test_level = 2 and r.test_instance=? and r.suite_id = ? group by r.status");
			stmt.setLong(1, latestReocdInstanceID);
			stmt.setString(2, suiteID);
			stmt.setLong(3, latestReocdInstanceID);
			stmt.setString(4, suiteID);
			ResultSet rs = stmt.executeQuery();

			SuiteStatistics suiteStatistics = new SuiteStatistics();
			 String[] labelsPerceantageArray = new String[4];
			 labelsPerceantageArray[0] = "0%";
			 labelsPerceantageArray[1] = "0%";
			 labelsPerceantageArray[2] = "0%";
			 labelsPerceantageArray[3] = "0%";
			 int[] seriePerceantageArray = new int[4];
			int total = 0;
			while (rs.next()) {
				int thisStatusCount = rs.getInt("sum");
				total = total + thisStatusCount;
				String status = rs.getString("status");
				if(status.equalsIgnoreCase("success"))
				{
					suiteStatistics.setSuccessPercentage(rs.getInt("percentage"));
					suiteStatistics.setSuccessTotal(thisStatusCount);
					labelsPerceantageArray[3] = suiteStatistics.getSuccessPercentage()+"%";
					seriePerceantageArray[3] = suiteStatistics.getSuccessPercentage();
				}else if(status.equalsIgnoreCase("failed"))
				{
					suiteStatistics.setFailedPercentage(rs.getInt("percentage"));
					suiteStatistics.setFailedTotal(thisStatusCount);
					labelsPerceantageArray[2] = suiteStatistics.getFailedPercentage()+"%";
					seriePerceantageArray[2] = suiteStatistics.getFailedPercentage();
				}else if(status.equalsIgnoreCase("ignored"))
				{
					suiteStatistics.setIgnoredPercentage(rs.getInt("percentage"));
					suiteStatistics.setIgnoredTotal(thisStatusCount);
					labelsPerceantageArray[1] = suiteStatistics.getIgnoredPercentage()+"%";
					seriePerceantageArray[1] = suiteStatistics.getIgnoredPercentage();
					
				}else if(status.equalsIgnoreCase("pending"))
				{
					suiteStatistics.setPendingPercentage(rs.getInt("percentage"));
					suiteStatistics.setPendingTotal(thisStatusCount);
					labelsPerceantageArray[0] = suiteStatistics.getPendingPercentage()+"%";
					seriePerceantageArray[0] = suiteStatistics.getPendingPercentage();
				}else
					System.out.println("Please add the status code in GUI");		
				
			}
			suiteStatistics.setLabelsPerceantageArray(labelsPerceantageArray);
			suiteStatistics.setSeriePerceantageArray(seriePerceantageArray);
			suiteStatistics.setTotal(total);
			suiteStaArray.add(suiteStatistics);
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  suiteStaArray;
	}
	


	public SuiteReportList getReportList(String suiteId, String instance) throws SQLException, NamingException {
		SuiteReportList srlist = new SuiteReportList();

		SuiteReport suiteReport = new SuiteReport();

		suiteReport.setSuiteRecordHistoryList(getSuiteRecordHistory(suiteId));
		if(suiteReport.getSuiteRecordHistoryList() != null && suiteReport.getSuiteRecordHistoryList().size()>0)
		{
			long latestReocdInstanceID;
			if(instance == null || instance.equalsIgnoreCase("0"))				
				latestReocdInstanceID = suiteReport.getSuiteRecordHistoryList().get(0).getTestInstance();			
			else
				latestReocdInstanceID = Long.parseLong(instance);
			
			suiteReport.setStatusStatisticsList(getInstancePercentage(latestReocdInstanceID,suiteId));
			Connection con = null;
			try {
				con = getConnection();
				System.out.println("getReportList :: con = "+con);
				/*PreparedStatement stmt = con.prepareStatement(" select r.test_instance as test_instance, r.testcase_id as testcase_id, t.testcase_name as testcase_name , s.suite_name as suite_name,"
						+ "  r.suite_id as suite_id ,r.record_type as record_type ,r.record_name as record_name ,  r.status  as status  ,  r.result_message as result_message ,  t.env as env   , "
						+ " r.total_time as total_time    ,  r.executed_by as executed_by , r.schedule_date as schedule_date , r.complete_date as complete_date from tb_suite as s "
						+ "LEFT JOIN tb_report as r ON r.suite_id = s.suite_id "
						+ "LEFT JOIN tb_testcase as t ON r.testcase_id = t.testcase_id  "
						+ " where r.suite_id = ? and r.test_instance = ? ORDER BY r.complete_date DESC, r.step_sequence ASC");*/
				
				PreparedStatement stmt = con.prepareStatement(" select test_instance , suite_id   ,  testcase_id  ,  test_level  ,  name ,step_number , status "
						+ "  ,  message   ,  env      ,   executed_by   ,  start_ts   ,  end_ts   from  TB_REPORT  where test_instance = ? and suite_id = ?");
				
				stmt.setLong(1, latestReocdInstanceID);
				stmt.setString(2, suiteId);
				
				System.out.println("joint query ");				
				ResultSet rs = stmt.executeQuery();		
				TestRecord testRecord = new TestRecord();
				while (rs.next()) {
					TestRecordData testRecordData = new TestRecordData();
					int testLevel = rs.getInt("test_level");
					testRecordData.setTestLevel(testLevel);
					
					if(testLevel ==1) {								
						testRecordData.setSuiteId(rs.getInt("suite_id"));
						testRecordData.setSuiteName(rs.getString("name"));
						testRecordData.setStatus(rs.getString("status"));
						testRecordData.setCompleteDate(rs.getTimestamp("end_ts"));
						testRecordData.setStartDate(rs.getTimestamp("start_ts"));
						testRecordData.setEnv(rs.getString("env"));
						testRecordData.setExecutedBy(rs.getString("executed_by"));		
						testRecordData.setResultMessage(rs.getString("message"));
						testRecord.setSuiteRecord(testRecordData);
						System.out.println("adding the suite record");
					}else if(testLevel ==2)
					{
						testRecordData.setTestcaseId(rs.getInt("testcase_id"));
						testRecordData.setTestcaseName(rs.getString("name"));
						testRecordData.setStatus(rs.getString("status"));
						testRecordData.setCompleteDate(rs.getTimestamp("end_ts"));
						testRecordData.setStartDate(rs.getTimestamp("start_ts"));
						testRecordData.setEnv(rs.getString("env"));
						testRecordData.setExecutedBy(rs.getString("executed_by"));
						testRecordData.setResultMessage(rs.getString("message"));
						testRecord.getTestRecordList().add(testRecordData);
						System.out.println("adding the test record");
					}else if(testLevel ==3)
					{
						testRecordData.setTestcaseId(rs.getInt("testcase_id"));
						testRecordData.setStepName(rs.getString("name"));
						testRecordData.setStepNumber(rs.getInt("step_number"));
						testRecordData.setStatus(rs.getString("status"));
						testRecordData.setCompleteDate(rs.getTimestamp("end_ts"));
						testRecordData.setStartDate(rs.getTimestamp("start_ts"));
						testRecordData.setEnv(rs.getString("env"));
						testRecordData.setExecutedBy(rs.getString("executed_by"));	
						testRecordData.setResultMessage(rs.getString("message"));
						testRecord.getStepRecordList().add(testRecordData);
					}												    
				}
				suiteReport.setTestRecord(testRecord);				
			}
			catch (SQLException e) {
				error("error", e);
			}
			finally {
				close(con);
			}
			
		}else
		{
			/// No execution history in table.
			System.out.println("No record in execution history");
		}
		srlist.add(suiteReport);
		return srlist;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
