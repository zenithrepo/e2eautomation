package com.zenithss.backend.handler;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import com.zenithss.backend.beans.*;


public class TestSuiteDAO extends BaseDao {
	
	

	public TestSuiteDAO() throws ClassNotFoundException, NamingException {
	}

	public Response deleteSuite(String sid) throws SQLException, NamingException {
		Response response = new Response();
		int suiteID=0 ; 
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("createTestCase :: deleting sid = "+sid);
			PreparedStatement pstmt = con.prepareStatement("delete from TB_SUITES where suite_id=?");	
			pstmt.setString(1, sid);			
			int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {			 
				 response.setStatus("success");
				 response.setErrorMessage("Suite #" +sid+ " is deleted successfully");
			 }
			System.out.println("createTestCase :: insertResult = "+suiteID);
		}
		catch (Exception e) {
			error("error", e);
			response.setStatus("failed");
			response.setErrorMessage("Suite #" +sid+ " is already ran, so you can't delete this suite.");
		}
		finally {
			close(con);	
			}

		return  response;
	}
	
	public void checkSuiteResult(String sid) throws SQLException, NamingException
	{
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("getSuiteRecordHistory :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("select distinct(test_instance), testcase_id from tb_report as r where test_instance in (select max(test_instance) as test_instance from tb_report where suite_id=?)");	
			pstmt.setString(1, sid);
						 int total = 0;
						 int success = 0;
						 String instanceID = "";
						 String testCaseID = "";
			 ResultSet rs = pstmt.executeQuery();
				while (rs.next()) {					
					instanceID = rs.getString("test_instance");
					testCaseID = rs.getString("testcase_id");	
					
					String  status = checkTestCaseResult(sid,instanceID,testCaseID);
					if(status.equalsIgnoreCase("SUCCESS"))
						success = success +1;
					
					total = total +1;					
				}				
				System.out.println("checkSuiteResult :: total = "+total + "success = "+success);
				if(total == success) 
				{
					insertReport(sid, instanceID, "0", "SUCCESS","1");					
				}else
					insertReport(sid, instanceID, "0", "FAILED","1");
							
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}
	}
	
	public String checkTestCaseResult(String sid, String instanceID, String testCaseID) throws SQLException, NamingException
	{
		String returnVal = "FAILED";
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement pstmt = con.prepareStatement("select test_instance, testcase_id, status, test_level from tb_report as r where  suite_id=? and test_instance=? and testcase_id=? and test_level=3");	
			 pstmt.setString(1, sid);
			 pstmt.setString(2, instanceID);
			 pstmt.setString(3, testCaseID);
			 ResultSet rs = pstmt.executeQuery();
			 int total = 0;
			 int success = 0;
				while (rs.next()) {					
					String status = rs.getString("status");
					if(status != null && status.equalsIgnoreCase("success"))
					{
						success = success +1;
					}
					total = total +1;				
				}
				
				System.out.println("checkTestCaseResult :: total = "+total + "success = "+success);
				if(total == success)
				{
					insertReport(sid, instanceID, testCaseID, "SUCCESS","2");
					returnVal = "SUCCESS";
				}else
				{
					insertReport(sid, instanceID, testCaseID, "FAILED","2");
					returnVal = "FAILED";
				}
				
				
				System.out.println("checkTestCaseResult :: returnVal = "+returnVal + " sid= "+sid+ " instanceID= "+instanceID+ " testCaseID="+testCaseID);
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}
		return returnVal;
	}
	
	public void insertReport(String sID , String instanceID, String tID, String status, String testLevel) throws SQLException, NamingException {
		
		
		System.out.println("insertReport :: status = "+status + " sid= "+sID+ " instanceID= "+instanceID+ " testCaseID="+tID + " testLevel = "+testLevel );
		Connection con = null;
		try {
			con = getConnection();
			PreparedStatement pstmt = con.prepareStatement("update TB_REPORT set status = ? where test_instance =? and suite_id = ? and testcase_id = ? and test_level = ?");
			 pstmt.setString(1, status);
			 pstmt.setString(2, instanceID);
			 pstmt.setString(3, sID);
			 pstmt.setString(4, tID);
			 pstmt.setString(5, testLevel);
		    
			 int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {				 
				 System.out.println("insertReport :: Record inserted successfully");
				 
			 }			
			
		}
		catch (SQLException e) {
			System.out.println("error"+e);
		}
		finally {
			close(con);	
			}
	}
	
	public Response createSuite(TestSuite testsuite) throws SQLException, NamingException {
		Response response = new Response();
		int suiteID=0 ; 
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("getSuiteRecordHistory :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("insert into TB_SUITES (suite_name , env ) values(?,?)");	
			 pstmt.setString(1, testsuite.getName());
			 pstmt.setString(2, testsuite.getEnv());			    
			 int insertCount = pstmt.executeUpdate();
			 if(insertCount > 0)
			 {
				 suiteID = getMaxSuiteID(testsuite.getName());
				 insertSuiteTestCaseMapping(suiteID , testsuite);
				 response.setStatus("success");
				 response.setErrorMessage("Suite added successfully, Suite number is :"+suiteID);
			 }			
			System.out.println("createSuite :: insertResult = "+suiteID);
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  response;
	}
	
	public int insertSuiteTestCaseMapping(int suiteID, TestSuite testsuite) throws SQLException, NamingException {
		int testCaseInsertCount=0 ; 
		
		Connection con = null;
		try {
			
			if(testsuite.getTestcases() != null && testsuite.getTestcases().size()>0)
			{
			con = getConnection();
			con.setAutoCommit(false);
			System.out.println("insertSuiteTestCaseMapping :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (?,?,?)");	
			int i=1;
			for(TestCase testCase : testsuite.getTestcases()) {
				pstmt.setInt(1, suiteID);
				 pstmt.setString(2, testCase.getTestcaseName());	
				 pstmt.setInt(3, i);	
				 pstmt.addBatch();
				 i ++;
			}			 
			 
			int[] updateCounts = pstmt.executeBatch();
			
			 if(updateCounts != null && updateCounts.length > 0)
			 {
				 testCaseInsertCount = updateCounts.length;
				 System.out.println("batch insert is done and inserted count is = "+testCaseInsertCount);
			 }else
				 System.out.println("batch insert is failed or zero records inserted in table");
			 con.commit();			
			
			}else
			{
				System.out.println("testsuite.getTestcases()  is null or size is zero so not insertign the mapping entry");
			}
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  testCaseInsertCount;
	}
	
	
	
	
	public int getMaxSuiteID(String suiteName) throws SQLException, NamingException {
		int suiteID=0 ; 
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("getSuiteRecordHistory :: con = "+con);
			PreparedStatement pstmt = con.prepareStatement("select suite_id, suite_name from TB_SUITES where suite_id = (select max(suite_id) from tb_suites)");			
			 ResultSet rs = pstmt.executeQuery();

				while (rs.next()) {			
					
					if(suiteName.equalsIgnoreCase(rs.getString("suite_name")))
					{
						suiteID = rs.getInt("suite_id");
						System.out.println("LAst inserted suite_id = "+suiteID);						
					}
				}
				
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  suiteID;
	}
	

	
	public HashMap<Integer, Integer> getExecutionCounter() throws SQLException, NamingException {
		HashMap<Integer, Integer> countMap = new HashMap();
		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("getSuiteRecordHistory :: con = "+con);
			PreparedStatement stmt = con.prepareStatement("select suite_id, count(distinct test_instance) as count from tb_report group by suite_id");
			
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {				
				countMap.put(rs.getInt("suite_id"), rs.getInt("count"));
			}
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);	
			}

		return  countMap;
	}
	

	public TestSuiteList getSuitesList() throws SQLException, NamingException {
		TestSuiteList tslist = new TestSuiteList();
		//HashMap<Integer, Integer> countMap = getExecutionCounter();		
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select s.suite_id as suite_id , s.suite_name as suite_name, s.creation_date as creation_date, s.env as senv, st.testcase_id as testcase_id , ( select  testcase_name  from   TB_TESTCASES t  where t.testcase_id = st.testcase_id order by st.execution_sq) as testcase_name "
					+ ", ( select  count(distinct test_instance)  from   TB_REPORT as r where r.suite_id = s.suite_id) as count from TB_SUITES as s join TB_SUITES_TESTCASES as st on st.suite_id=s.suite_id order by s.suite_id desc");
			System.out.println("afterquery = ");			
			ResultSet rs = stmt.executeQuery();
			Map<String, TestSuite> hmap = new LinkedHashMap<>();
			while (rs.next()) {
				String suiteId = rs.getString("suite_id");
				if(hmap.containsKey(suiteId))
				{
					TestSuite testSuite = hmap.get(suiteId);
					TestCase testCase = new TestCase();
					testCase.setTestcaseId(rs.getInt("testcase_id"));
					testCase.setTestcaseName(rs.getString("testcase_name"));
					testSuite.getTestcases().add(testCase);
				}else
				{
				
				TestSuite testSuite = new TestSuite();
				testSuite.setEnv(rs.getString("senv"));
				testSuite.setName(rs.getString("suite_name"));
				testSuite.setSuiteId(Integer.parseInt(suiteId));
				testSuite.setCreationDate(rs.getTimestamp("creation_date"));				
				
					testSuite.setExecutionCounter(rs.getInt("count"));
					TestCase testCase = new TestCase();
					testCase.setTestcaseId(rs.getInt("testcase_id"));
					testCase.setTestcaseName(rs.getString("testcase_name"));
					testSuite.getTestcases().add(testCase);
					hmap.put(suiteId, testSuite);
				}
			
			}
				
			for (Map.Entry<String, TestSuite> entry : hmap.entrySet()) {
			    String key = entry.getKey();
			    TestSuite value = entry.getValue();
			    tslist.add(value);
			}
			
		
			
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);
		}

		return tslist;
	}
	
	
	
	public boolean genearteSuiteXMlFile(String suiteID) throws SQLException, NamingException {
		
		boolean fileGenerated = false;		
		String suiteXMlFormat = "<test-suite suite-id=\"$SID\" name=\"$TSNAME\" env=\"$TSENV\"> ";
		Connection con = null;
		try {
			con = getConnection();
			System.out.println("con = "+con);
			PreparedStatement stmt = con.prepareStatement("select s.suite_name as suitename, s.env as suiteenv, t.testcase_id as testcase_id , t.testcase_name as testcase_name, t.env as testcase_env,  t.testcase_xml as testcasexml "
					+ "from tb_suites as s , tb_testcases as t , TB_SUITES_TESTCASES as st where st.suite_id = s.suite_id and st.testcase_id = t.testcase_id and s.suite_id =?");
			stmt.setString(1, suiteID);
			ResultSet rs = stmt.executeQuery();

			
			String suiteName="";
			while (rs.next()) {				
				suiteXMlFormat = suiteXMlFormat.replace("$SID", suiteID);
				suiteName = rs.getString("suitename");
				suiteXMlFormat = suiteXMlFormat.replace("$TSNAME", suiteName);
				suiteXMlFormat = suiteXMlFormat.replace("$TSENV",  rs.getString("suiteenv"));	
				
				String tcaseenv = rs.getString("testcase_env");

				suiteXMlFormat = suiteXMlFormat + rs.getString("testcasexml");
				
			}				
			suiteXMlFormat = suiteXMlFormat+ "</test-suite>";	
			System.out.println("suite xml = "+suiteXMlFormat);
			System.out.println("xml is ready and wrtign to xml file");
			stringToDom(suiteXMlFormat, suiteName);
			
		}
		catch (SQLException e) {
			error("error", e);
		}
		finally {
			close(con);
		}
		return fileGenerated;
	}
	
	
	public boolean stringToDom(String xmlSource, String suiteName) {
		
		java.io.FileWriter fw = null;
		
		try
		{
			System.out.println("Creatign the file for xml :- "+xmlSource);
			fw = new java.io.FileWriter("C:\\Users\\x172875\\Documents\\BitBucket_Repo\\e2eautomation\\E2EAutomation\\src\\test-suite.xml");
			fw.write(xmlSource);
		}catch(Exception e)
		{
			System.out.println("stringToDom :: Exception "+e.getMessage());
		}finally
		{
			try {
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    
	    return true;
	}
	
	
	public ArrayList<TestStep> parseXML(String xml)
	{
		ArrayList<TestStep> stepList = new ArrayList<TestStep>();
	
		
		
		return stepList;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
