
package com.zenithss.backend.servlet;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

import com.zenith.automation.process.Service;
import com.zenithss.backend.beans.DashboardSummaryList;
import com.zenithss.backend.beans.ObjectList;
import com.zenithss.backend.beans.Response;
import com.zenithss.backend.beans.SuiteReportList;
import com.zenithss.backend.beans.TestCase;
import com.zenithss.backend.beans.TestCaseList;
import com.zenithss.backend.beans.TestSuite;
import com.zenithss.backend.beans.TestSuiteList;
import com.zenithss.backend.handler.DashBoardDAO;
import com.zenithss.backend.handler.TestCaseDAO;
import com.zenithss.backend.handler.TestReportDAO;
import com.zenithss.backend.handler.TestSuiteDAO;
import com.zenithss.backend.util.Json;


/**
 * Servlet 
 */
@WebServlet("/GUIController")
public class GUIController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GUIController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Object obj = null;
		
		String sId="";
		// Set refresh, autoload time as 3 seconds
	    //  response.setIntHeader("Refresh", 3);
				
		// Set response content type
		response.setContentType("application/json");
		StringBuffer postBodySB = new StringBuffer();
		
		
		if(request.getMethod().equalsIgnoreCase("POST"))
		{		
		  String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		    	postBodySB.append(line);
		  } catch (Exception e) { /*report an error*/ }

		  System.out.println("postBodySB = "+postBodySB);
		}	  
			   
		String param = request.getParameter("param");
		System.out.println("param value = "+param);
		try {			
			TestCaseDAO testCaseDAO = new TestCaseDAO();
			TestSuiteDAO testSuiteDAO = new TestSuiteDAO();	
			ObjectList objList = new ObjectList();
			Gson gson;
			switch(param)
			{				
			case "deletetest":				
				objList.add(testCaseDAO.deleteTestCase(request.getParameter("tid")));
				obj = objList;				
			break;
			case "deletesuite":				
			objList.add(testSuiteDAO.deleteSuite(request.getParameter("sid")));
			obj = objList;				
		break;
			case "gettest":				
				objList.add(testCaseDAO.getTestCase(request.getParameter("tid")));
				obj = objList;				
			break;
			case "createtest":
				gson = new GsonBuilder().registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
				TestCase testcase = (TestCase)gson.fromJson(postBodySB.toString(), TestCase.class);	
				
				ObjectList obj1 = new ObjectList();
				obj1.add(testCaseDAO.createTestCase(testcase));
				obj = obj1;
				System.out.println("testcase = "+testcase.toString());
			break;
			case "updatetest":
				gson = new GsonBuilder().registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
				TestCase updatetestcase = (TestCase)gson.fromJson(postBodySB.toString(), TestCase.class);
				
				objList.add(testCaseDAO.updateTestCase(updatetestcase));
				obj = objList;
				System.out.println("testcase = "+updatetestcase.toString());
			break;
			case "createsuite":
				gson = new GsonBuilder().registerTypeAdapter(Double.class, new BadDoubleDeserializer()).create();
				TestSuite testsuite = (TestSuite)gson.fromJson(postBodySB.toString(), TestSuite.class);	
				ObjectList object1 = new ObjectList();
				object1.add(testSuiteDAO.createSuite(testsuite));
				obj = object1;
				System.out.println("testsuite = "+testsuite.toString());
			break;
			case "tselect":
				ObjectList objectList1 = new ObjectList();	
				objectList1 = testCaseDAO.getTestCaseSelectOptions();				
				obj = objectList1;
			break;			 
			case "suite":
				TestSuiteList testSuiteList = null;
				
				testSuiteList = testSuiteDAO.getSuitesList();				
				obj = testSuiteList; 
			break;	
			case "testdata":
				String tId = request.getParameter("tid");
				System.out.println("sId = "+tId);
				TestCaseList tcaseList = null;						
				tcaseList = testCaseDAO.getTestCasedata(tId);				
				obj = tcaseList; 
			break;	
			case "testcase":				
				TestCaseList tcList = null;				
				tcList = testCaseDAO.getTestCaseList();				
				obj = tcList; 
			break;
			case "suitedetails":
				SuiteReportList suiteReportList = null;
				sId = request.getParameter("sid");
				String instance = request.getParameter("instance");
				System.out.println("sId = "+sId + " test_instance="+instance);
				TestReportDAO testReportDAO = new TestReportDAO();	
				suiteReportList = testReportDAO.getReportList(sId,instance);				
				obj = suiteReportList; 
			break;
			case "dashboard":
				DashboardSummaryList testCaseList = null;
				//testCaseList = testSuiteDAO.getDashboardSummary();				
				obj = testCaseList; 
			break;
			case "report":
			break;
			case "run":
				ObjectList objectList = new ObjectList();
				Service service = new Service();
				sId = request.getParameter("sid");	
				// call suite run method.					
				testSuiteDAO.genearteSuiteXMlFile(sId);
				//service.trigger();
				String mainParam[] = new String[1];
				mainParam[0] = "C:\\Users\\x172875\\Documents\\BitBucket_Repo\\e2eautomation\\E2EAutomation\\src\\configuration.prop";
				Service.main(mainParam);
				testSuiteDAO.checkSuiteResult(sId);
				Response resp = new Response();
				resp.setStatus("success");
				resp.setErrorMessage("Suite ID : "+ sId+ " executed successfully");
				resp.setDurationInSec(100);
				objectList.add(resp);
				obj = objectList;
				break;
			case "dashboardSummary":
				DashboardSummaryList dashboardSummaryList = new DashboardSummaryList();
				DashBoardDAO dashBoardDAO = new DashBoardDAO();	
				dashboardSummaryList = dashBoardDAO.getDashboardSummary();				
				obj = dashboardSummaryList;
				break;	
			case "getmysql":	
				Json json = new Json();				
				obj = json.readJSONFile();				
				break;	
			}		
		}
		catch (Exception e) {
			System.out.println("error"+ e);			
			obj = e;
		}		
		PrintWriter out = response.getWriter();
		String json = Json.toJson(obj);
		System.out.println("json: " + json);		
		out.println(json);
		close(out);
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	protected void close(PrintWriter out) {
		out.flush();
		out.close();
	}
	
	public static class BadDoubleDeserializer implements JsonDeserializer<Double> {

		@Override
		public Double deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
			try {
				return Double.parseDouble(element.getAsString().replace(',', '.'));
			} catch (NumberFormatException e) {
				throw new JsonParseException(e);
			}
		}

	}
	
}
