package com.zenithss.backend.util;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.sun.xml.internal.bind.api.TypeReference;
import com.zenithss.backend.beans.TestCase;
import com.zenithss.backend.servlet.GUIController.BadDoubleDeserializer;
import com.zenithss.backend.beans.DataBase;
import com.zenithss.backend.beans.DataBaseList;
import com.zenithss.backend.beans.ObjectList;
public class Json {
	public static String toJson(Object obj) {
		String res = "";
		
		if (obj != null) {
			Gson gson = new Gson();
			
			res = gson.toJson(obj);
		}
		
		return res;
	}
	
	
		public static String toJsonMap(HashMap<String,HashMap> outterMap, String FirstKeyName) {
			String res = "";
			
			String returnString = new String();
			for (Entry<String, HashMap> entry : outterMap.entrySet()) {
				
				//System.out.println("Item : " + entry.getKey() + " Count : " + entry.getValue());
				
				String output = toJson(entry.getValue());
				//output = output.replaceAll("\"", "");
				output = output.substring(1,output.length());
				output = "{\""+FirstKeyName+"\": \""+entry.getKey()+"\" ,"+output+",";
				returnString = returnString + output;
								
			}	
			
			if(returnString.endsWith(","))
				returnString = returnString.substring(0,returnString.length()-1);
							
			
			String NewreturnString = "[";
			NewreturnString = NewreturnString + returnString +"]";
			
			
			return NewreturnString;
			
		}
		
		public static String lineChartData(Set<String> set, String chartData , String FirstKeyName) {
		/*	String returnString = "json: [";
			returnString = returnString + chartData +"],keys: {x: '"+ FirstKeyName+"',value: ['";
					
			
			Iterator<String> iter = set.iterator();
			while (iter.hasNext()) {
				returnString = returnString+iter.next() + "',";
			}
			
			if(returnString.endsWith(","))
				returnString = returnString.substring(0,returnString.length()-1)+"]}";
			else
				returnString = returnString+"]}";*/
			
			String returnString = "[";
			returnString = returnString + chartData +"]";
					
			String secondPart = "'";
			Iterator<String> iter = set.iterator();
			while (iter.hasNext()) {
				secondPart = secondPart+iter.next() + "',";
			}
			if(secondPart.endsWith(","))
				secondPart = secondPart.substring(0,secondPart.length()-1);
			
			returnString = returnString +"|"+secondPart;
			
						
			return returnString;
		}
		
		public List<DataBase> readJSONFile()
		{
			List<DataBase> dbList = new LinkedList<>();
			try {
			String jsonArray = new String ( Files.readAllBytes( Paths.get("C:\\Users\\x172875\\Documents\\BitBucket_Repo\\e2eautomation\\E2EAutomation\\src\\mysqldetails.json") ) );

			ObjectMapper  objectMapper = new ObjectMapper();

			DataBase[] dbArray = objectMapper.readValue(jsonArray, DataBase[].class);
			if(dbArray != null)
			{
				System.out.println("Printing array length");
				System.out.println(dbArray.length);
				System.out.println(dbArray);
				dbList = Arrays.asList(dbArray);
				System.out.println("dbList " + dbList );
				//objectList = (DataBaseList) Arrays.asList(dbArray);
				//List<DataBase> dbList1 = Arrays.asList(dbArray);
				
				
			}
			}catch(Exception e)
			{
				System.out.println("Exception e ="+e.getMessage());
			}
			return dbList;
		}
		
		
}
