package com.zenithss.backend.util;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException
;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.zenith.automation.framework.Constants;
import com.zenithss.backend.beans.TestAssert;
import com.zenithss.backend.beans.TestCase;
import com.zenithss.backend.beans.TestPrerequisities;
import com.zenithss.backend.beans.TestStep;


public class ProcessXML extends DefaultHandler{
	String sTestCaseName = null;	
	String tmpValue;
	TestCase testCase ;
	
    public TestCase parseDocument(String xmlStr, TestCase tc) {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
        	testCase = tc;
        	System.out.println("xmlStr = "+xmlStr);
        	if(xmlStr == null || xmlStr.length() < 4)
        	{
        		System.out.println("parseDocument :: xml string is null or blank");
        	}else {
	        	InputStream stream = new ByteArrayInputStream(xmlStr.getBytes(StandardCharsets.UTF_8));
	        	
	            SAXParser parser = factory.newSAXParser();
	            parser.parse(stream, this);
        		}
            //parser.parse("src/test-suite.xml", this);
        } catch (ParserConfigurationException e) {
            System.out.println("ParserConfig error");
        } catch (SAXException e) {
            System.out.println("SAXException : xml not well formed");
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found Exception");
        }catch(IOException e) {
        	System.out.println("IO Exception");
        }catch(Exception e) {
        	e.printStackTrace();
        }
        return tc;
    }
    
    @Override
    public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {
 
    	if (elementName.equalsIgnoreCase(Constants.TEST_CASE)) {
    		sTestCaseName = attributes.getValue(Constants.NAME);
    		testCase.setTestcaseName(sTestCaseName);
    		String sTestCaseEnv = attributes.getValue(Constants.ENV);
    		if(sTestCaseEnv != null) {//Environment from test case level
    			testCase.setTestenv(sTestCaseEnv);    			
    		}    		
    	}
    	if (elementName.equalsIgnoreCase(Constants.TEST_PREREQUITSIES)) {
    		TestPrerequisities testPrerequisties = new TestPrerequisities();
    		testPrerequisties.setPkgClass(attributes.getValue(Constants.CLASS));
    		testPrerequisties.setPrerequisitiestype(Constants.TYPE);
    		testPrerequisties.setPreCheckField(attributes.getValue(Constants.PRECHECK_FIELD));
    		testPrerequisties.setSub(attributes.getValue(Constants.SUBSCRIBER));
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testPrerequisties.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testPrerequisties.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		//testCase.preRequistes.add(testPrerequisties);
    		//testCase.getTestPrerequisities().add(testPrerequisties);
    	}

    	if (elementName.equalsIgnoreCase(Constants.TEST_STEP)) {
    		TestStep testStep = new TestStep();
    		testStep.setPkgClass(attributes.getValue(Constants.CLASS));
    		testStep.setBan(attributes.getValue(Constants.BAN));
    		testStep.setSubscriberId(attributes.getValue(Constants.SUBSCRIBER));
    		testStep.setSessionId(attributes.getValue(Constants.SESSION_ID));
    		String sUsage = attributes.getValue(Constants.USAGE);
    		if(sUsage != null && sUsage != "") {
    			testStep.setUsage(Integer.parseInt(sUsage));
    		}else {
    			testStep.setUsage(Constants.DEFAULT_NUMBER);
    		}
    		String rg = attributes.getValue(Constants.RATING_GROUP);
    		if(rg != null && rg != "") {
    			testStep.setRatingGroup(rg);
    		}else {
    			testStep.setRatingGroup("10101");
    		}
    		testStep.setUnit(attributes.getValue(Constants.UNIT));
    		if(attributes.getValue(Constants.MCC_MNC_XML) != null && attributes.getValue(Constants.MCC_MNC_XML) != "") {
    			int iMccMnc =-1;
    			iMccMnc = Integer.parseInt(attributes.getValue(Constants.MCC_MNC_XML));
    			testStep.setMccMnc(iMccMnc);
    		}else {
    			testStep.setMccMnc(302220);
    		}
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testStep.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testStep.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		//testCase.getTestStep().add(testStep);
    	}
    	if (elementName.equalsIgnoreCase(Constants.TEST_ASSERT)) {
    		TestAssert testAssert =  new TestAssert();
    		testAssert.setPkgClass(attributes.getValue(Constants.CLASS));
    		String sTimer = attributes.getValue(Constants.WAIT_TIMER);
    		if(sTimer != null && sTimer != "") {
    			testAssert.setWaitTime(Integer.parseInt(sTimer));
    		}else {
    			testAssert.setWaitTime(Constants.DEFAULT_NUMBER);
    		}
    		testAssert.setIdentifier(attributes.getValue(Constants.SUBSCRIBER));
    		testAssert.setSessionId(attributes.getValue(Constants.SESSION_ID));
    		testAssert.setBan(attributes.getValue(Constants.BAN));
    		testAssert.setAssertType(attributes.getValue(Constants.ASSERT_TYPE));
    		//testCase.getTestAssert().add(testAssert);
    		//testCase.validationSteps.add(testAssert);
    	}
    	/*if (elementName.equalsIgnoreCase(Constants.TEST_REPORT)) {
    		TestReport testReport = new TestReport();
    		testReport.setFileName(attributes.getValue(Constants.FILE_NAME));
    		testReport.setFileToDir(attributes.getValue(Constants.PATH));
    		testSuite.testReport = testReport;
    	}*/
    }
    
    
    @Override
    public void endElement(String s, String s1, String element) throws SAXException {

    }
    @Override
    public void characters(char[] ac, int i, int j) throws SAXException {
    	tmpValue = new String(ac, i, j);
    }
    

}
